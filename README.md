Gitlab repo: https://gitlab.com/photocontestproject/photocontest

Azure link: https://portal.azure.com/#@telerikacademy.com/resource/subscriptions/36fe589c-d8a1-4e2c-8898-f98b788867c4/resourceGroups/Pictures/providers/Microsoft.Storage/storageAccounts/photocontest/overview

  
  
  

## Table of contents

* [Technologies](#technologies)

* [General info](#general-info)

* [Screenshots](#screenshots)

* [Features](#features)

* [Status](#status)

* [Contact](#contact)

  <br>
  
  

**Technologies**:

**Automapper** (8.1.1)

**X.PagedList.MVC.Core** (7.6.0)

**Microsoft.EntityFrameworkCore.Tools** (3.1.9)

**Microsoft.EntityFrameworkCore.SqlServer** (3.1.9)

**Microsoft.EntityFrameworkCore.Relational** (3.1.9)

**Microsoft.EntityFrameworkCore.Design** (3.1.9)

**Microsoft.EntityFrameworkCore** (3.1.9)

**Microsoft.AspNetCore.Mvc.Razor.RuntimeCompilation** (3.1.13)

**Microsoft.AspNetCore.Mvc.NewtonsoftJson** (3.1.9)

**Microsoft.AspNetCore.Identity.EntityFrameworkCore** (3.1.9)

**Hangfire.SqlServer** (1.7.23)

**Hangfire.AspNetCore** (1.7.23)

**bootstrap** (5.0.1)

**Azure.Storage.Blobs** (12.8.4)

**WindowsAzure.Storage** (9.3.3)

  <br/>
  
<br/>
<br/>

## Screenshots

<br>

## Homepage:

You can go to check our contests or browse through the users.

![](pictures/home_page.png)![](/.png)

<br>

## Contest info:
This is the place where you could check all the information about a certain contest.


![](pictures/contest_inside.png)

<br>

## User info:
You can check all the user's information.

![](pictures/user.png)

<br>

## Photo info:
All the information about a photo.

![](pictures/photo_inside.png)

<br>

## Rate photo:
How to rate a photo.
![](pictures/rate_picture.png)

<br>

## About us:
You can contact us.

![](pictures/contact_us.png)

<br>




## General info

  

## Solution "PhotoContest" includes N projects:

* 1.[Web] > PhotoContest.API > API

* 2.[MVC] > PhotoContest.MVC > MVC

* 3.[Services] > PhotoContest.Services > Services

* 4.[Data] > PhotoContest.Data > Data

* 5.[Tests] > PhotoContest.Tests > Tests

  
  
  

## Web application

  

## 1.[Web] > PhotoContest.API > API

The API functionality is implemented inside the .NET Core 3.1 MVC Web Application.

PhotoContest.API contains all the available services that we offer.

  

To use the API you need to be registered user in the MVC application and you must acquire your API key once you are logged in. After that, you need to authenticate yourself in the API. Once that is done, you can get information about the application throught the API.

  
  
## Swagger:

A screenshot of available methods.

![](pictures/swagger.png)

<br>

## Swagger authentication:

![](pictures/swagger_authentication.png)
  
  
<br>

## Swagger result:

![](pictures/swagger_result.png)

<br>
  
  
  

## 2.[WEB] > PhotoContest.MVC > MVC

  

MVC project includes:

1.MVC Razor Pages

- Controls the flow of the application as well as front end part.

  

2.Logs

- Logs all the events that has occured in the application.

  

3.Mapping

- Data Models -> Service Models

- Service Models <-> View Models

  

4.Middleware

- 404 redirects you to our error page

  
  

## 3.[Services] > PhotoContest.Services

This is a .NET Core 3.1 Class Library project.

  

Includes:

- /Models

-- Includes models needed for application.

  
  
  
  

- /ServiceContracts

-- Includes all the necessary interfaces.

- /Services

-- Includes all the logic for the application.

  

- Admin service

- Category service

- Contest service

- Photo service

- PhotoViewModel Service

- User service

  
  

**Data layer**

  

## 4.[Data] > PhotoContest.Data

This is a .NET Core 3.1 Class Library project.

  

Includes:

  

* Configurations

	*   Contains series of relation instruction to Entity Framework.

  
* Migrations

  * Contains series of migrations used by Entity Framework.

  

* DbContext

  * Contains the database of the project.

  
  

## 3.1[Data] > PhotoContest.Data

This is a .NET Core 3.1 Class Library project.

  

Includes:

  

- Admin.cs

- Category.cs

- Comment.cs

- Contest.cs

- ContestUser.cs

- FollowFollowed.cs

- Label.cs

- LabelPhoto.cs

- Photo.cs

- PhotoScore.cs

- Rank.cs

- Role.cs

- User.cs

- PhotoContestContext.cs

  

## Database / Class Diagrams

## This is how our database relationships look.

![](pictures/database.png)

  

