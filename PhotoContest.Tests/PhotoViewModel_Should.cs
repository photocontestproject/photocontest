﻿using Azure.Storage.Blobs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Tests.Utils;
using PhotoContestServicess.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests
{
    [TestClass]
    public class PhotoViewModel_Should
    {
        private const string connectionString = "DefaultEndpointsProtocol=https;AccountName=photocontest;AccountKey=l69XRYx6ZbrjJdpEciIsq4ebymLOcV3sb/2TBuhk2vmpVb02ZIMyjb0JjaShNRv2fosicf87m4u+MgI4MIoVEg==;EndpointSuffix=core.windows.net";
        [TestMethod]
        public async Task Get_Photos_Should_Return_Right_Number()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using var _context = new PhotoContestContext(options);
            var blobService = new BlobServiceClient(connectionString);
            _context.AddRange(Util.GetPhotos());
            _context.SaveChanges();

            var service = new PhotoService(_context, blobService);

            var photos = await service.NumberOfPhotos();

            Assert.AreEqual(10, photos);

            _context.Database.EnsureDeleted();
        }

        [TestMethod]
        public async Task Get_Photos_Winners_Should_Return_Right_Number()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using var _context = new PhotoContestContext(options);
            var blobService = new BlobServiceClient(connectionString);
            _context.AddRange(Util.GetPhotos());
            _context.AddRange(Util.GetContests());
            _context.AddRange(Util.GetContestPhotos());
            _context.SaveChanges();

            var service = new PhotoService(_context, blobService);

            var photos = await service.GetPhotosWinners();

            Assert.AreEqual(10, photos.Count);

            _context.Database.EnsureDeleted();
        }

        [TestMethod]
        public async Task Get_Photos_User_Should_Return_Right_Number()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                var blobService = new BlobServiceClient(connectionString);
                _context.AddRange(Util.GetPhotos());
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new PhotoService(_context, blobService);

                var photos = await service.GetPhotosUser(2);

                Assert.AreEqual(2, photos.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Photos_User_Should_Return_Empty()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                var blobService = new BlobServiceClient(connectionString);
                _context.AddRange(Util.GetPhotos());
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new PhotoService(_context, blobService);

                var photos = await service.GetPhotosUser(55);

                Assert.AreEqual(0, photos.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Photos_Photoname_Should_Return_Photo()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                var blobService = new BlobServiceClient(connectionString);
                _context.AddRange(Util.GetPhotos());
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new PhotoService(_context, blobService);

                var photos = await service.GetPhotosByName("PictureNameThird");

                Assert.AreEqual("0.5", photos.Aperture);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Photos_Photoname_Should_Return_Empty()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                var blobService = new BlobServiceClient(connectionString);
                _context.AddRange(Util.GetPhotos());
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new PhotoService(_context, blobService);

                var photos = await service.GetPhotosByName("PictureNameThirddsfadsf");

                Assert.AreEqual(null, photos);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Photos_Contest_Should_Return_Contest_Photos()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                var blobService = new BlobServiceClient(connectionString);
                _context.AddRange(Util.GetPhotos());
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new PhotoService(_context, blobService);

                var photos = await service.GetPhotoContest(1);

                Assert.AreEqual(2, photos.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Photos_Contest_Should_Return_Empty()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                var blobService = new BlobServiceClient(connectionString);
                _context.AddRange(Util.GetPhotos());
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new PhotoService(_context, blobService);

                var photos = await service.GetPhotoContest(15);

                Assert.AreEqual(0, photos.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Photo_By_Id_Should_Return_Photo()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                var blobService = new BlobServiceClient(connectionString);
                _context.AddRange(Util.GetPhotos());
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new PhotoService(_context, blobService);

                var photo =  service.GetPhotoById(1);

                Assert.AreEqual("1.5", photo.Aperture);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Photo_By_Id_Should_Return_Empty()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                var blobService = new BlobServiceClient(connectionString);
                _context.AddRange(Util.GetPhotos());
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new PhotoService(_context, blobService);

                var photo =  service.GetPhotoById(19);

                Assert.AreEqual(null, photo);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void Update_Contest_Photo_Should_Not_Throw()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                var blobService = new BlobServiceClient(connectionString);
                _context.AddRange(Util.GetPhotos());
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new PhotoService(_context, blobService);

                service.ContestPhotoUpdate("PictureNameThird", "TestContest", 3);

                Assert.AreEqual(0, 0);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void Get_Photos_Uri_Should_Return_Uri()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                var blobService = new BlobServiceClient(connectionString);
                _context.AddRange(Util.GetPhotos());
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new PhotoViewModelService(blobService);

                var result = service.GetBlobsUri();

                Assert.IsNotNull(result);

                _context.Database.EnsureDeleted();
            }

        }
    }
}


