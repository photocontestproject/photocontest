﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Tests.Utils;
using PhotoContestServicess.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests
{
    [TestClass]
    public class ContestService_Should
    {
        [TestMethod]
        public async Task Get_Contest_Should_Return_Contest()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contest = await service.GetContest("TestContest");

                Assert.IsNotNull(contest);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Contest_Photo_Should_Return_Photos()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var photos = service.GetContestPhotos("TestContest");

                Assert.IsNotNull(photos);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Contests_Should_Return_Number_Of_Contests()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.GetContests(5);

                Assert.AreEqual(5, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_All_Contests_Should_Return_Number_Of_Contests()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.GetAllContests();

                Assert.AreEqual(10, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_All_Users_Contests_Should_Return_Contests()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.GetUserContests(2);

                Assert.IsNotNull(contests);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_All_Users_Contests_Should_Return_Empty()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.GetUserContests(291);

                Assert.AreEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Admin_Contests_Should_Return_Contests()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.GetAdminContests(1);

                Assert.AreNotEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Admin_Contests_Should_Return_Empty()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.GetAdminContests(16);

                Assert.AreEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Contest_Category_Should_Return_Contests()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.ContestCategory(1, 2);

                Assert.AreNotEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Contest_Category_Should_Return_Empty()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.ContestCategory(16, 2);

                Assert.AreEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Contest_Category_Admin_Should_Return_Contests()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.ContestCategory(1, 2);

                Assert.AreNotEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Contest_Category_Admin_Should_Return_Empty()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.ContestCategory(14, 2);

                Assert.AreEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Contest_Category_User_Phase_Should_Return_Contess()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.ContestCatUserPhase(1, 2, 1);

                Assert.AreNotEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Contest_Category_User_Phase_Should_Return_Empty()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.ContestCatUserPhase(21, 2, 1);

                Assert.AreEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Contest_Cat_User_Phase_Should_Return_Contests()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.ContestCatUserPhase(1, 2, new List<int>() { 1, 2 });

                Assert.AreNotEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Contest_Cat_User_Phase_Should_Return_Empty()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service .ContestCatUserPhase(43, 2, new List<int>() { 1, 2 });

                Assert.AreEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_All_Contest_Filter_Should_Return_Contests()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.GetAllContestsFilter(1, new List<int>() { 1, 2 });

                Assert.AreNotEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Contest_Cat_Phase_Should_Return_Contests()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service .ContestCatPhase(1, 1);

                Assert.AreNotEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Contest_Cat_Phase_Should_Return_Empty()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.ContestCatPhase(21, 1);

                Assert.AreEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }


        [TestMethod]
        public async Task Contest_Cat_Phase_Admin_Should_Return_Contests()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.ContestCatUserPhaseAdmin(1, 1, new List<int>() { 1, 2 });

                Assert.AreNotEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Contest_Cat_Phase_Admin_Should_Return_Empty()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contests = await service.ContestCatUserPhaseAdmin(214, 1, new List<int>() { 1, 2 });

                Assert.AreEqual(0, contests.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Change_Contest_To_Expired_Should_Change()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                service.ChangeContestToExpired("TestContest");

                var contest = await service.GetContest("TestContest");

                Assert.AreEqual(3, contest.Phase);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Change_Contest_To_Phase_Two()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                service.ChangeContestToPhaseTwo("TestContest2");

                var contest = await service.GetContest("TestContest2");

                Assert.AreEqual(2, contest.Phase);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Delete_Contest_Should_Delete()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var contestToDelete = await service .GetContest("TestContest");

                service.DeleteContest(contestToDelete);

                Assert.AreEqual(true, contestToDelete.IsDeleted);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_Category_Should_Return_Category()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();

            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetUsers());
                _context.AddRange(Util.GetCategories());
                _context.SaveChanges();

                var userService = new UsersServices(_context);

                var service = new ContestService(_context, userService);

                var category = await service.GetCategory(2);

                Assert.AreEqual("Urban", category.Name);

                _context.Database.EnsureDeleted();
            }
        }
    }
}
