﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Tests.Utils;
using PhotoContestServicess.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests
{
    [TestClass]
    public class CategoryService_Should
    {
        [TestMethod]
        public async Task Get_Category_Id()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.AddRange(Util.GetCategories());
                _context.SaveChanges();

                var service = new CategoryService(_context);

                var categoryId = await service.GetCategoryid("Sky");


                Assert.AreEqual(3, categoryId);
                _context.Database.EnsureDeleted();
            }
        }


        //[TestMethod]
        //public async Task Get_Category_Name()
        //{
        //    var options = Util.GetInMemoryDatabase<PhotoContestContext>();


        //    using (var _context = new PhotoContestContext(options))
        //    {
        //        _context.AddRange(Util.GetUsers());
        //        _context.AddRange(Util.GetCategories());
        //        _context.SaveChanges();

        //        var service = new CategoryService(_context);

        //        var categoryName = await service.GetCategoryname(3);


        //        Assert.AreEqual("Sky", categoryName);
        //        _context.Database.EnsureDeleted();
        //    }
        //}


        //[TestMethod]
        //public async Task Get_All_Categories_Should_Return_Categories()
        //{
        //    var options = Util.GetInMemoryDatabase<PhotoContestContext>();


        //    using (var _context = new PhotoContestContext(options))
        //    {
        //        _context.AddRange(Util.GetUsers());
        //        _context.AddRange(Util.GetCategories());
        //        _context.SaveChanges();

        //        var service = new CategoryService(_context);

        //        var categories = await service.GettAll();

        //        Assert.AreEqual(10, categories.Count);
        //        _context.Database.EnsureDeleted();
        //    }
        //}
    }
}


