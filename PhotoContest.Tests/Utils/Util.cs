﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Tests.Utils
{
    public static class Util
    {
        public static DbContextOptions<T> GetInMemoryDatabase<T>() where T : DbContext
        {
            var options = new DbContextOptionsBuilder<T>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;

            return options;
        }

        public static ICollection<User> GetUsers()
        {
            //var passHasher = new Microsoft.AspNetCore.Identity.PasswordHasher<User>();
            return new List<User>()
            {
                new User()
                {
                    Id = 1,
                    UserName = "Petkan Ivanov",
                    NormalizedUserName = "PETKAN IVANOV",
                    Email = "petkan@abv.bg",
                    NormalizedEmail = "PETKAN@ABV.BG",
                    PasswordHash = "user123hashed",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    RankId = 3,
                    ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-1.jpg"
                },
                new User()
                {
                    Id = 2,
                    UserName = "Ivan Petkov",
                    NormalizedUserName = "IVAN PETKOV",
                    Email = "ivan@abv.bg",
                    NormalizedEmail = "IVAN@ABV.BG",
                    PasswordHash = "user123hashed",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    RankId = 2,
                    ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-2.jpg"
                },
                new User()
                {
                    Id = 3,
                    UserName = "Pesho Borisov",
                    NormalizedUserName = "PESHO BORISOV",
                    Email = "pesho@abv.bg",
                    NormalizedEmail = "PESHO@ABV.BG",
                    PasswordHash = "user123hashed",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    RankId = 2,
                    ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-3.jpg"
                },
                new User()
                {
                    Id = 4,
                    UserName = "Pesho Borisov",
                    NormalizedUserName = "PESHO BORISOV",
                    Email = "pesho@abv.bg",
                    NormalizedEmail = "PESHO@ABV.BG",
                    PasswordHash = "user123hashed",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    RankId = 2,
                    ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-4.jpg"
                },
                new User()
                {
                    Id = 5,
                    UserName = "Boris Peshov",
                    NormalizedUserName = "BORIS PESHOV",
                    Email = "boris@abv.bg",
                    NormalizedEmail = "BORIS@ABV.BG",
                    PasswordHash = "user123hashed",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    RankId = 3,
                    ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-5.jpg"
                },
                new User()
                {
                    Id = 6,
                    UserName = "Teodor Orehov",
                    NormalizedUserName = "TEODOR OREHOV",
                    Email = "teodor@abv.bg",
                    NormalizedEmail = "TEODOR@ABV.BG",
                    PasswordHash = "user123hashed",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    RankId = 2,
                    ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-6.jpg"
                },
                new User()
                {
                    Id = 7,
                    UserName = "Oreh Teodorov",
                    NormalizedUserName = "TEODOR OREHOV",
                    Email = "teodor@abv.bg",
                    NormalizedEmail = "TEODOR@ABV.BG",
                    PasswordHash = "user123hashed",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    RankId = 2,
                    ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-7.jpg"
                },
                new User()
                {
                    Id = 8,
                    UserName = "Hristina Vachkova",
                    NormalizedUserName = "HRISTINA VACHKOVA",
                    Email = "hristina@abv.bg",
                    NormalizedEmail = "HRISTINA@ABV.BG",
                    PasswordHash = "user123hashed",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    RankId = 3,
                    ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-8.jpg"
                },
                new User()
                {
                    Id = 9,
                    UserName = "Stanislav Georgiev",
                    NormalizedUserName = "STANISLAV GEORGIEV",
                    Email = "stanislav@abv.bg",
                    NormalizedEmail = "STANISLAV@ABV.BG",
                    PasswordHash = "user123hashed",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    RankId = 2,
                    ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-9.jpg"
                },
                new User()
                {
                    Id = 10,
                    UserName = "Georgi Stankov",
                    NormalizedUserName = "GEORGI STANKOV",
                    Email = "georgi@abv.bg",
                    NormalizedEmail = "GEORGI@ABV.BG",
                    PasswordHash = "user123hashed",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    RankId = 2,
                    ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-10.jpg"
                },

            };
        }

        public static ICollection<Photo> GetPhotos()
        {
            return new List<Photo>()
            {
                new Photo()
                {
                    Aperture = "1.5",
                    Camera = "nikon",
                    Description = "this is a test description",
                    FocalLength = "1.9",
                    Id = 1,
                    ISO = "this is ISO",
                    Name = "PictureNameFirst",
                    PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/nature-1.jpg",
                    ShutterSpeed = "3",
                    UserId = 2
                },
                new Photo()
                {
                    Aperture = "2.4",
                    Camera = "nikon 2",
                    Description = "this is a test description2",
                    FocalLength = "2.0",
                    Id = 2,
                    ISO = "this is ISO 2",
                    Name = "PictureNameSecond",
                    PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/nature-2.jpg",
                    ShutterSpeed = "1",
                    UserId = 2
                },
                new Photo()
                {
                    Aperture = "0.5",
                    Camera = "nikon 3",
                    Description = "this is a test description 3",
                    FocalLength = "1.9",
                    Id = 3,
                    ISO = "this is ISO",
                    Name = "PictureNameThird",
                    PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/nature-3.jpg",
                    ShutterSpeed = "3",
                    UserId = 3
                },
                new Photo()
                {
                    Aperture = "2.4",
                    Camera = "nikon 2",
                    Description = "this is a test description4",
                    FocalLength = "2.0",
                    Id = 4,
                    ISO = "this is ISO 2",
                    Name = "PictureNameFourth",
                    PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/nature-4.jpg",
                    ShutterSpeed = "1",
                    UserId = 3
                },
                new Photo()
                {
                    Aperture = "1.5",
                    Camera = "nikon 5",
                    Description = "this is a test description 5",
                    FocalLength = "1.9",
                    Id = 5,
                    ISO = "this is ISO 5",
                    Name = "PictureNameFifth",
                    PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/nature-5.jpg",
                    ShutterSpeed = "3",
                    UserId = 4
                },
                new Photo()
                {
                    Aperture = "2.4",
                    Camera = "nikon 2",
                    Description = "this is a test description6",
                    FocalLength = "2.0",
                    Id = 6,
                    ISO = "this is ISO 6",
                    Name = "PictureNameSixth",
                    PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/nature-6.jpg",
                    ShutterSpeed = "1",
                    UserId = 4
                },
                new Photo()
                {
                    Aperture = "4.5",
                    Camera = "nikon 3",
                    Description = "this is a test description 3",
                    FocalLength = "1.9",
                    Id = 7,
                    ISO = "this is ISO",
                    Name = "PictureNameFirst",
                    PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/nature-7.jpg",
                    ShutterSpeed = "3",
                    UserId = 4
                },
                new Photo()
                {
                    Aperture = "2.4",
                    Camera = "nikon 2",
                    Description = "this is a test description8",
                    FocalLength = "2.0",
                    Id = 8,
                    ISO = "this is ISO 2",
                    Name = "PictureNameSecond",
                    PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/nature-4.jpg",
                    ShutterSpeed = "1",
                    UserId = 5
                },
                new Photo()
                {
                    Aperture = "4.5",
                    Camera = "nikon 9",
                    Description = "this is a test description 9",
                    FocalLength = "1.9",
                    Id = 9,
                    ISO = "this is ISO",
                    Name = "PictureNameFirst",
                    PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/nature-10.jpg",
                    ShutterSpeed = "3",
                    UserId = 7
                },
                new Photo()
                {
                    Aperture = "2.4",
                    Camera = "nikon 2",
                    Description = "this is a test description2",
                    FocalLength = "2.0",
                    Id = 10,
                    ISO = "this is ISO 2",
                    Name = "PictureNameSecond",
                    PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/nature-10.jpg",
                    ShutterSpeed = "1",
                    UserId = 7
                },
            };
        }

        public static ICollection<Comment> GetComments()
        {
            return new List<Comment>()
            {
                new Comment()
                {
                    Id = 1,
                    CommentTime = DateTime.Now.AddDays(2),
                    Description = "beautiful!!",
                    PhotoId = 1,
                    UserId = 2,
                },
                new Comment()
                {
                    Id = 2,
                    CommentTime = DateTime.Now.AddDays(5),
                    Description = "gorgeous",
                    PhotoId = 1,
                    UserId = 2
                },
                new Comment()
                {
                    Id = 3,
                    CommentTime = DateTime.Now.AddDays(2),
                    Description = "very beautiful photo",
                    PhotoId = 2,
                    UserId = 2,
                },
                new Comment()
                {
                    Id = 4,
                    CommentTime = DateTime.Now.AddDays(2),
                    Description = "nice shot",
                    PhotoId = 2,
                    UserId = 2
                },
                new Comment()
                {
                    Id = 5,
                    CommentTime = DateTime.Now.AddDays(2),
                    Description = "nice shot",
                    PhotoId = 2,
                    UserId = 2
                },
                new Comment()
                {
                    Id = 6,
                    CommentTime = DateTime.Now.AddDays(2),
                    Description = "very nice texture btw",
                    PhotoId = 10,
                    UserId = 10
                },
                new Comment()
                {
                    Id = 7,
                    CommentTime = DateTime.Now.AddDays(12),
                    Description = "i'd like this photo to be on my desktop",
                    PhotoId = 9,
                    UserId = 7
                },
                new Comment()
                {
                    Id = 8,
                    CommentTime = DateTime.Now.AddDays(82),
                    Description = "i'd like this photo to be on my screen",
                    PhotoId = 6,
                    UserId = 3
                },
                new Comment()
                {
                    Id = 9,
                    CommentTime = DateTime.Now.AddDays(82),
                    Description = "i'd like this photo to be on my screen",
                    PhotoId = 8,
                    UserId = 6
                },
                new Comment()
                {
                    Id = 10,
                    CommentTime = DateTime.Now.AddDays(82),
                    Description = "it looks like i'm there",
                    PhotoId = 5,
                    UserId = 3
                },
                new Comment()
                {
                    Id = 11,
                    CommentTime = DateTime.Now.AddDays(8),
                    Description = "i can feel the photo",
                    PhotoId = 5,
                    UserId = 9
                },
                new Comment()
                {
                    Id = 12,
                    CommentTime = DateTime.Now.AddDays(8),
                    Description = "such a pretty photo(y)",
                    PhotoId = 8,
                    UserId = 8
                },
                new Comment()
                {
                    Id = 13,
                    CommentTime = DateTime.Now.AddDays(82),
                    Description = "it looks like i'm there",
                    PhotoId = 8,
                    UserId = 2
                },
                new Comment()
                {
                    Id = 14,
                    CommentTime = DateTime.Now.AddDays(2),
                    Description = "i can feel the photo",
                    PhotoId = 5,
                    UserId = 6
                },
                new Comment()
                {
                    Id = 15,
                    CommentTime = DateTime.Now.AddDays(8),
                    Description = "such a pretty photo(y)",
                    PhotoId = 5,
                    UserId = 7
                }
            };
        }
        public static ICollection<IdentityUserRole<int>> IdentityUserRoles()
        {
            return new List<IdentityUserRole<int>>()
            {
                new IdentityUserRole<int>()
                {
                    UserId = 2,
                    RoleId = 2
                },
                new IdentityUserRole<int>()
                {
                    UserId = 3,
                    RoleId = 2
                },
                new IdentityUserRole<int>()
                {
                    UserId = 3,
                    RoleId = 2
                },
                new IdentityUserRole<int>()
                {
                    UserId = 4,
                    RoleId = 2
                },
                new IdentityUserRole<int>()
                {
                    UserId = 5,
                    RoleId = 3
                },
                new IdentityUserRole<int>()
                {
                    UserId = 6,
                    RoleId = 2
                },
                new IdentityUserRole<int>()
                {
                    UserId = 7,
                    RoleId = 2
                },
                new IdentityUserRole<int>()
                {
                    UserId = 8,
                    RoleId = 2
                },
                new IdentityUserRole<int>()
                {
                    UserId = 9,
                    RoleId = 2
                },
                new IdentityUserRole<int>()
                {
                    UserId = 10,
                    RoleId = 3
                },
            };
        }

        public static ICollection<Contest> GetContests()
        {
            return new List<Contest>()
            {
                new Contest()
                {
                    Id = 1,
                    Name = "TestContest",
                    IsDeleted = false,
                    ParticipantLimit = 50,
                    Profile = "https://photocontest.blob.core.windows.net/youtubedemo/ocean-2.jpg",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(1),
                    Description = "this is a test contest",
                    WinnerPhoto = 1,
                    CategoryId = 1,
                    Phase = 1,
                    AdminId = 1
                },
                new Contest()
                {
                    Id = 2,
                    Name = "TestContest2",
                    IsDeleted = false,
                    ParticipantLimit = 500,
                    Profile = "https://photocontest.blob.core.windows.net/youtubedemo/ocean-3.jpg",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(1),
                    Description = "this is a test contest2",
                    WinnerPhoto = 1,
                    CategoryId = 3,
                    Phase = 1,
                    AdminId = 1
                },
                new Contest()
                {
                    Id = 3,
                    Name = "TestContest3",
                    IsDeleted = false,
                    ParticipantLimit = 10,
                    Profile = "https://photocontest.blob.core.windows.net/youtubedemo/ocean-4.jpg",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(2),
                    Description = "this is a test contest3",
                    WinnerPhoto = 1,
                    CategoryId = 4,
                    Phase = 1,
                    AdminId = 1
                },
                new Contest()
                {
                    Id = 4,
                    Name = "TestContest4",
                    IsDeleted = false,
                    ParticipantLimit = 50,
                    Profile = "https://photocontest.blob.core.windows.net/youtubedemo/ocean-5.jpg",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(3),
                    Description = "this is a test contest4",
                    WinnerPhoto = 1,
                    CategoryId = 3,
                    Phase = 1,
                    AdminId = 1
                },
                new Contest()
                {
                    Id = 5,
                    Name = "TestContest5",
                    IsDeleted = false,
                    ParticipantLimit = 24,
                    Profile = "https://photocontest.blob.core.windows.net/youtubedemo/ocean-6.jpg",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(10),
                    Description = "this is a test contest",
                    WinnerPhoto = 1,
                    CategoryId = 4,
                    Phase = 1,
                    AdminId = 1
                },
                new Contest()
                {
                    Id = 6,
                    Name = "TestContest6",
                    IsDeleted = false,
                    ParticipantLimit = 50,
                    Profile = "https://photocontest.blob.core.windows.net/youtubedemo/ocean-7.jpg",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(1),
                    Description = "this is a test contest",
                    WinnerPhoto = 1,
                    CategoryId = 4,
                    Phase = 1,
                    AdminId = 1
                },
                new Contest()
                {
                    Id = 7,
                    Name = "TestContest7",
                    IsDeleted = false,
                    ParticipantLimit = 50,
                    Profile = "https://photocontest.blob.core.windows.net/youtubedemo/ocean-8.jpg",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(12),
                    Description = "this is a test contest",
                    WinnerPhoto = 1,
                    CategoryId = 4,
                    Phase = 1,
                    AdminId = 1
                },
                new Contest()
                {
                    Id = 8,
                    Name = "TestContest8",
                    IsDeleted = false,
                    ParticipantLimit = 50,
                    Profile = "https://photocontest.blob.core.windows.net/youtubedemo/ocean-9.jpg",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(1),
                    Description = "this is a test contest",
                    WinnerPhoto = 1,
                    CategoryId = 4,
                    Phase = 1,
                    AdminId = 1
                },
                new Contest()
                {
                    Id = 9,
                    Name = "TestContest9",
                    IsDeleted = false,
                    ParticipantLimit = 560,
                    Profile = "https://photocontest.blob.core.windows.net/youtubedemo/ocean-10.jpg",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(1),
                    Description = "this is a test contest",
                    WinnerPhoto = 1,
                    CategoryId = 1,
                    Phase = 1,
                    AdminId = 1
                },
                new Contest()
                {
                    Id = 10,
                    Name = "TestContest10",
                    IsDeleted = false,
                    ParticipantLimit = 50,
                    Profile = "https://photocontest.blob.core.windows.net/youtubedemo/nature-4.jpg",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(1),
                    Description = "this is a test contest",
                    WinnerPhoto = 1,
                    CategoryId = 2,
                    Phase = 1,
                    AdminId = 1
                },
            };
        }

        public static ICollection<Category> GetCategories()
        {
            return new List<Category>()
            {
                 new Category()
                 {
                     Id = 1,
                     Name = "Mountains"
                 },
                 new Category()
                 {
                    Id = 2,
                    Name = "Urban"
                 },
                 new Category()
                 {
                    Id = 3,
                    Name = "Sky"
                 },
                 new Category()
                 {
                     Id = 4,
                     Name = "Architecture"
                 },
                 new Category()
                 {
                     Id = 5,
                     Name = "Birds"
                 },
                 new Category()
                 {
                     Id = 6,
                     Name = "Landscape"
                 },
                 new Category()
                 {
                     Id = 7,
                     Name = "Sci-Fi"
                 },
                 new Category()
                 {
                     Id = 8,
                     Name = "People"
                 },
                 new Category()
                 {
                     Id = 9,
                     Name = "City"
                 },
                 new Category()
                 {
                     Id = 10,
                     Name = "Tech"
                 }
            };
        }

        public static ICollection<PhotoScoreContest> GetContestPhotos()
        {
            return new List<PhotoScoreContest>()
            {
                new PhotoScoreContest()
                {
                    PhotoId = 1, ContestId = 1
                },
                new PhotoScoreContest()
                {
                    PhotoId = 2, ContestId = 1
                },
                new PhotoScoreContest()
                {
                    PhotoId = 2, ContestId = 2
                },
                new PhotoScoreContest()
                {
                    PhotoId = 1, ContestId = 2
                },
                new PhotoScoreContest()
                {
                    PhotoId = 3, ContestId = 3
                },
                new PhotoScoreContest()
                {
                    PhotoId = 4, ContestId = 4
                },
                new PhotoScoreContest()
                {
                    PhotoId = 5, ContestId = 5
                },
                new PhotoScoreContest()
                {
                    PhotoId = 6, ContestId = 5
                },
                new PhotoScoreContest()
                {
                    PhotoId = 1, ContestId = 6
                },
                new PhotoScoreContest()
                {
                    PhotoId = 10, ContestId = 6
                },
                new PhotoScoreContest()
                {
                    PhotoId = 7, ContestId = 7
                },
                new PhotoScoreContest()
                {
                    PhotoId = 2, ContestId = 7
                },
                new PhotoScoreContest()
                {
                    PhotoId = 3, ContestId = 8
                },
                new PhotoScoreContest()
                {
                    PhotoId = 4, ContestId = 8
                },
                new PhotoScoreContest()
                {
                    PhotoId = 10, ContestId = 8
                },
                new PhotoScoreContest()
                {
                    PhotoId = 2, ContestId = 9
                },
                new PhotoScoreContest()
                {
                    PhotoId = 1, ContestId = 9
                },
                new PhotoScoreContest()
                {
                    PhotoId = 8, ContestId = 9
                },
                new PhotoScoreContest()
                {
                    PhotoId = 5, ContestId = 10
                },
                new PhotoScoreContest()
                {
                    PhotoId = 4, ContestId = 10
                },
                new PhotoScoreContest()
                {
                    PhotoId = 3, ContestId = 10
                },
                new PhotoScoreContest()
                {
                    PhotoId = 2, ContestId = 10
                },
                new PhotoScoreContest()
                {
                    PhotoId = 1, ContestId = 10
                },
            };
        }

        public static ICollection<ContestUser> GetContestUsers()
        {
            return new List<ContestUser>()
            {
                new ContestUser()
                {
                     ContestId = 1, UserId = 2
                },
                new ContestUser()
                {
                    ContestId = 2, UserId = 2
                },
                new ContestUser()
                {
                    ContestId = 3, UserId = 2
                },
                new ContestUser()
                {
                    ContestId = 4, UserId = 2
                },
                new ContestUser()
                {
                    ContestId = 5, UserId = 2
                },
                new ContestUser()
                {
                    ContestId = 6, UserId = 2
                },
                new ContestUser()
                {
                    ContestId = 1, UserId = 4, Rated = true
                },
                new ContestUser()
                {
                    ContestId = 2, UserId = 6, Rated = true
                },
                new ContestUser()
                {
                    ContestId = 7, UserId = 7
                },
                new ContestUser()
                {
                    ContestId = 8, UserId = 8
                },
                new ContestUser()
                {
                    ContestId = 9, UserId = 9
                },
                new ContestUser()
                {
                    ContestId = 10, UserId = 10
                },
            };
        }

        public static ICollection<Rank> GetRanks()
        {
            return new List<Rank>()
            {
                new Rank()
                {
                    Id = 1,
                    RankName = "Junkie"
                },
                new Rank()
                {
                    Id = 2,
                    RankName = "Enthusiast"
                },
                new Rank()
                {
                    Id = 3,
                    RankName = "Master"
                },
                new Rank()
                {
                    Id = 4,
                    RankName = "Dictator"
                }
            };
        }

        public static ICollection<PhotoScoreContest> GetPhotoScoreContests()
        {
            return new List<PhotoScoreContest>()
            {
               new PhotoScoreContest()
                {
                   ContestId = 1,
                   PhotoId = 1,
                },
                new PhotoScoreContest()
                {
                   ContestId = 2,
                   PhotoId = 2,
                },
                new PhotoScoreContest()
                {
                  ContestId = 1,
                  PhotoId = 3,
                },
                new PhotoScoreContest()
                {
                    ContestId = 3,
                    PhotoId = 3,
                },
                new PhotoScoreContest()
                {
                    ContestId = 4,
                    PhotoId = 4,
                },
                new PhotoScoreContest()
                {
                    ContestId = 5,
                    PhotoId = 5,
                },
                new PhotoScoreContest()
                {
                    ContestId = 5,
                    PhotoId = 6,
                },
                new PhotoScoreContest()
                {
                    ContestId = 6,
                    PhotoId = 14,
                },
                new PhotoScoreContest()
                {
                    ContestId = 6,
                    PhotoId = 10,
                },
                new PhotoScoreContest()
                {
                    ContestId = 7,
                    PhotoId = 7,
                },
                new PhotoScoreContest()
                {
                    ContestId = 7,
                    PhotoId = 2,
                },
                new PhotoScoreContest()
                {
                    ContestId = 7,
                    PhotoId = 5,
                },
                new PhotoScoreContest()
                {
                    ContestId = 8,
                    PhotoId = 3,
                },
                new PhotoScoreContest()
                {
                    ContestId = 8,
                    PhotoId = 4,
                },
                new PhotoScoreContest()
                {
                    ContestId = 8,
                    PhotoId = 10,
                },
                new PhotoScoreContest()
                {
                    ContestId = 8,
                    PhotoId = 1,
                },
            };
        }

        public static ICollection<Role> GetRoles()
        {
            return new List<Role>()
            {
                new Role()
                {
                    Id = 1,
                    Name = "admin"
                },
                new Role()
                {
                    Id = 2,
                    Name = "user"
                }
            };
        }

        public static ICollection<IdentityUserRole<int>> GetUserRoles()
        {
            var userRole1 = new IdentityUserRole<int>()
            {
                RoleId = 1,
                UserId = 1
            };
            var userRole2 = new IdentityUserRole<int>()
            {
                RoleId = 2,
                UserId = 2
            };
            var userRole3 = new IdentityUserRole<int>()
            {
                RoleId = 2,
                UserId = 3
            };
            var userRole4 = new IdentityUserRole<int>()
            {
                RoleId = 2,
                UserId = 4
            };
            var userRole5 = new IdentityUserRole<int>()
            {
                RoleId = 2,
                UserId = 5
            };
            var userRole6 = new IdentityUserRole<int>()
            {
                RoleId = 2,
                UserId = 6
            };
            var userRole7 = new IdentityUserRole<int>()
            {
                RoleId = 2,
                UserId = 7
            };
            var userRole8 = new IdentityUserRole<int>()
            {
                RoleId = 2,
                UserId = 8
            };
            var userRole9 = new IdentityUserRole<int>()
            {
                RoleId = 2,
                UserId = 9
            };
            var userRole10 = new IdentityUserRole<int>()
            {
                RoleId = 2,
                UserId = 10
            };
            var list = new List<IdentityUserRole<int>>()
            {
                userRole1,userRole2,userRole3,userRole4,userRole5,
                userRole6,userRole7,userRole8,userRole9,userRole10
            };
            return list;
        }
    }
}

