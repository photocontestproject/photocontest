﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoContest.Data;
using PhotoContest.Tests.Utils;
using PhotoContestServicess.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests
{
    [TestClass]
    public class UserService_Should
    {
        [TestMethod]
        public async Task Get_First_User_Should()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var users = await service.GetAllUsers();

                //Assert.AreEqual(2, service.GetEmployee("Joreto1", "j.petrov1@gmail.com").RoleId);
                Assert.AreEqual("Petkan", users[0].UserName.Split(" ")[0]);
                Assert.AreEqual(10, users.Count());
                _context.Database.EnsureDeleted();
            }
        }
        [TestMethod]
        public async Task Get_User_By_Id_Should()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var users =  await service.GetUser(1);

                //Assert.AreEqual(2, service.GetEmployee("Joreto1", "j.petrov1@gmail.com").RoleId);
                Assert.AreEqual("Petkan", users.UserName.Split(" ")[0]);
                _context.Database.EnsureDeleted();
            }
        }
        [TestMethod]
        public async Task Get_User_By_Id_Should_Return()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var user = service.GetUser(15);


                Assert.AreEqual(null, user);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_User_By_Email_Should_Return_User()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var user = await service.GetUserByEmail("georgi@abv.bg");

                Assert.AreEqual("Georgi", user.UserName.Split(" ")[0]);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_User_By_Email_Should_Return_Null()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var user = await service.GetUserByEmail("geordfagi@abv.bg");

                Assert.AreEqual(null, user);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_User_By_Name_Should_Return_User()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var user = await service.GetUserByName("Georgi Stankov");

                Assert.AreEqual("Stankov", user.UserName.Split(" ")[1]);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_User_By_Name_Should_Return_Null()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var user = await service.GetUserByName("Georggwesgi");

                Assert.AreEqual(null, user);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_User_Name_By_Email_Should_Return_Name()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var user = await service.GetUserName("stanislav@abv.bg");

                Assert.AreEqual("Stanislav Georgiev", user);

                _context.Database.EnsureDeleted();
            }
        }


        [TestMethod]
        public async Task Get_List_Of_Users_With_Rank()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.AddRange(Util.GetRanks());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var users = await service.GetUserRankFilter(new List<string>() { "Enthusiast" });

                Assert.AreEqual(7, users.Count());

                _context.Database.EnsureDeleted();
            }
        }


        [TestMethod]
        public async Task Get_User_If_Joined_Contest_Returns_True()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.AddRange(Util.GetContestUsers());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var ifJoined = await service.CheckIfUserJoinedContest(2, 1);

                Assert.AreEqual(true, ifJoined);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_User_If_Joined_Contest_ReturnsFalse()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.AddRange(Util.GetContestUsers());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var ifJoined = await service.CheckIfUserJoinedContest(4, 421);

                Assert.AreEqual(false, ifJoined);

                _context.Database.EnsureDeleted();
            }
        }

        
        [TestMethod]
        public async Task Check_If_User_Rated_Contest()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.AddRange(Util.GetContestUsers());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var ifRated = await service.CheckIfUserRatedContest(1, 1);

                Assert.AreEqual(false, ifRated);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_User_By_Id_Returns_User()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.AddRange(Util.GetContestUsers());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var user =  service.GetUserById(2);

                Assert.IsNotNull(user);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_User_By_Id_Returns_Empty()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.AddRange(Util.GetContestUsers());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var user =  service.GetUserById(266);

                Assert.IsNull(user);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_User_And_Roles_Should_Return_Users()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetRanks());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var users = await service.GetUserAndRoles();

                Assert.AreNotEqual(0, users.Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void Contest_User_Update_Should_Not_Throw()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetRanks());
                _context.AddRange(Util.GetContests());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                service.ContestUserUpdate("TestContest", 3);

                Assert.AreEqual(4, Util.GetRanks().Count);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Update_User_Should_Update()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.AddRange(Util.GetContestUsers());
                _context.AddRange(Util.GetRanks());
                _context.AddRange(Util.GetContests());
                _context.AddRange(Util.GetContestPhotos());
                _context.AddRange(Util.GetPhotos());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                service.UpdateUser(2);

                var user =  service.GetUserById(2);

                Assert.AreEqual(2, user.RankId);

                _context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Get_User_Rank_Should_Return_Rank()
        {
            var options = Util.GetInMemoryDatabase<PhotoContestContext>();


            using (var _context = new PhotoContestContext(options))
            {
                _context.AddRange(Util.GetUsers());
                _context.AddRange(Util.GetCategories());
                _context.AddRange(Util.GetRanks());
                _context.SaveChanges();

                var service = new UsersServices(_context);

                var rank = await service.GetUserRank(2);

                Assert.AreEqual("Enthusiast", rank);
                _context.Database.EnsureDeleted();
            }
        }
    }
}


