﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;

namespace PhotoContest.Data.Models
{
    public class Rank
    {
        public int? Id { get; set; }

        public string RankName { get; set; }

        public ICollection<User> Users { get; set; } = new List<User>();
    }
}
