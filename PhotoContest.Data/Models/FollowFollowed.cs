﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Models
{
    public class FollowFollowed
    {
        public int FollowedId { get; set; }
        public User Followed { get; set; }

        public int FollowerId { get; set; }

        public User Follower { get; set; }

    }
}
