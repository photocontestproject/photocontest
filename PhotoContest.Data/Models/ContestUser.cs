﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Models
{
    public class ContestUser
    {
        public int ContestId { get; set; }
        public Contest Contest { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public bool Rated{ get; set; }
    }
}
