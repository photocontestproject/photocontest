﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }

        ICollection<Contest> Contests = new List<Contest>();
    }
}
