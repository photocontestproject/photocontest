﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Models
{
    public class Label
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsDeleted { get; set; }

        public ICollection<LabelPhoto> LabelPhotos { get; set; } = new List<LabelPhoto>();
    }
}
