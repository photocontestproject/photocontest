﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Models.Contracts
{
    public interface IAdmin
    {
        public ICollection<Contest> ContestsCreated { get; set; }
    }
}
