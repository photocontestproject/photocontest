﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Models
{
    public class LabelPhoto
    {
        public int LabelId { get; set; }
        public Label Label { get; set; }

        public int PhotoId { get; set; }
        public Photo Photo { get; set; }
    }
}
