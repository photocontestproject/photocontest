﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoContest.Data.Models
{
    public class Contest
    {
        public int Id { get; set; }
        [Required]
        [StringLength(maximumLength: 100, MinimumLength = 3)]
        public string Name { get; set; }

        public string Profile{ get; set; }

        public int WinnerPhoto { get; set; }

        public int? CategoryId { get; set; }

        public int? AdminId { get; set; }

       
        public int Phase { get; set; }

        [Required]
        [StringLength(maximumLength: 200, MinimumLength = 3)]
        public string Description { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        public bool IsDeleted { get; set; }

        public int ParticipantLimit { get; set; }

        public ICollection<ContestUser> Users { get; set; } = new List<ContestUser>();
         
        public Admin Admin { get; set; }
        public Category Category { get; set; }
        public ICollection<PhotoScoreContest> PhotoScoreContests { get; set; } = new List<PhotoScoreContest>();


    }
}
