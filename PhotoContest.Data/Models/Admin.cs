﻿using PhotoContest.Data.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Models
{
    public class Admin : User,IAdmin
    {
        public ICollection<Contest> ContestsCreated { get; set; }
    }
}
