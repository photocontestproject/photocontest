﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Models
{
    public class PhotoScoreContest
    {
        public int Score { get; set; }
        
        public int PhotoId { get; set; }
        public Photo Photo { get; set; }

        public int ContestId { get; set; }
        public Contest Contest { get; set; }
    }
}
