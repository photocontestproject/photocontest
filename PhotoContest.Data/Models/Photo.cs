﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoContest.Data.Models
{
    public class Photo
    {
        public int Id { get; set; }

        [StringLength(maximumLength: 100, MinimumLength = 3)]
        public string Name { get; set; }

        [StringLength(maximumLength: 200)]
        public string Description { get; set; }

        public string PhotoLink { get; set; }

        public int UserId { get; set; }

        public string Camera { get; set; }

        public string ShutterSpeed { get; set; }

        public string Aperture { get; set; }

        public string FocalLength { get; set; }

        public string ISO { get; set; }

        public int ContestScore { get; set; }

        public bool IsDeleted { get; set; }

        public User User { get; set; }
        public ICollection<LabelPhoto> LabelPhotos { get; set; } = new List<LabelPhoto>();
        public ICollection<PhotoScoreContest> PhotoScoreContests { get; set; } = new List<PhotoScoreContest>();
        public ICollection<Comment> Comments { get; set; } = new List<Comment>();
    }
}
