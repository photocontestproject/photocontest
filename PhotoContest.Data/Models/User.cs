﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;


namespace PhotoContest.Data.Models
{
    public class User : IdentityUser<int>
    {

        public string ProfileLink { get; set; }
        public int? RankId { get; set; }

        public Rank Rank { get; set; }

        public string Description { get; set; }

        public bool IsBanned { get; set; }

        public bool IsDeleted { get; set; }

        public string ApiKey { get; set; }

        public ICollection<FollowFollowed> Followers { get; set; } = new List<FollowFollowed>();
        public ICollection<FollowFollowed> Followed { get; set; } = new List<FollowFollowed>();
        public ICollection<ContestUser> Contests { get; set; } = new List<ContestUser>();
        public ICollection<Photo> Photos { get; set; } = new List<Photo>();
    }
}
