﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Models
{
    public class Comment
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public DateTime CommentTime { get; set; }

        //date mod
        public int UserId { get; set; }

        public int PhotoId { get; set; }

        public bool IsDeleted { get; set; }

        public User User { get; set; }

        public Photo Photo { get; set; }
    }
}
