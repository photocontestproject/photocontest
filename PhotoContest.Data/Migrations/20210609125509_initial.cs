﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PhotoContest.Data.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Labels",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Labels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ranks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RankName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ranks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    ProfileLink = table.Column<string>(nullable: true),
                    RankId = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    IsBanned = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    ApiKey = table.Column<string>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Ranks_RankId",
                        column: x => x.RankId,
                        principalTable: "Ranks",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Contests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Profile = table.Column<string>(nullable: true),
                    WinnerPhoto = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: true),
                    AdminId = table.Column<int>(nullable: true),
                    Phase = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 200, nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    ParticipantLimit = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contests_AspNetUsers_AdminId",
                        column: x => x.AdminId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Contests_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "FollowFollowed",
                columns: table => new
                {
                    FollowedId = table.Column<int>(nullable: false),
                    FollowerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FollowFollowed", x => new { x.FollowedId, x.FollowerId });
                    table.ForeignKey(
                        name: "FK_FollowFollowed_AspNetUsers_FollowedId",
                        column: x => x.FollowedId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FollowFollowed_AspNetUsers_FollowerId",
                        column: x => x.FollowerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Photos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    Description = table.Column<string>(maxLength: 200, nullable: true),
                    PhotoLink = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    Camera = table.Column<string>(nullable: true),
                    ShutterSpeed = table.Column<string>(nullable: true),
                    Aperture = table.Column<string>(nullable: true),
                    FocalLength = table.Column<string>(nullable: true),
                    ISO = table.Column<string>(nullable: true),
                    ContestScore = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Photos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Photos_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ContestUsers",
                columns: table => new
                {
                    ContestId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    Rated = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContestUsers", x => new { x.UserId, x.ContestId });
                    table.ForeignKey(
                        name: "FK_ContestUsers_Contests_ContestId",
                        column: x => x.ContestId,
                        principalTable: "Contests",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ContestUsers_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(nullable: true),
                    CommentTime = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    PhotoId = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Photos_PhotoId",
                        column: x => x.PhotoId,
                        principalTable: "Photos",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Comments_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "LabelPhotos",
                columns: table => new
                {
                    LabelId = table.Column<int>(nullable: false),
                    PhotoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LabelPhotos", x => new { x.PhotoId, x.LabelId });
                    table.ForeignKey(
                        name: "FK_LabelPhotos_Labels_LabelId",
                        column: x => x.LabelId,
                        principalTable: "Labels",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_LabelPhotos_Photos_PhotoId",
                        column: x => x.PhotoId,
                        principalTable: "Photos",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "PhotoScoreContests",
                columns: table => new
                {
                    PhotoId = table.Column<int>(nullable: false),
                    ContestId = table.Column<int>(nullable: false),
                    Score = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhotoScoreContests", x => new { x.PhotoId, x.ContestId });
                    table.ForeignKey(
                        name: "FK_PhotoScoreContests_Contests_ContestId",
                        column: x => x.ContestId,
                        principalTable: "Contests",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_PhotoScoreContests_Photos_PhotoId",
                        column: x => x.PhotoId,
                        principalTable: "Photos",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 2, "a063ba51-b206-43e5-8689-b3a8a8ccde4e", "User", "USER" },
                    { 1, "2891c497-8ac3-4578-aef2-c1579bff5291", "Admin", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ApiKey", "ConcurrencyStamp", "Description", "Discriminator", "Email", "EmailConfirmed", "IsBanned", "IsDeleted", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "ProfileLink", "RankId", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { 1, 0, "", "79f92179-736a-46d5-abaf-291cf7c4a483", null, "Admin", "admin@admin.com", false, false, false, false, null, "ADMIN@ADMIN.COM", "ADMIN@ADMIN.COM", "AQAAAAEAACcQAAAAEB/A99oMGJBXxqOXRH5XNqGDTbUngdY8FpaEmA5AcJ/6pEt2LbYwSAs4SR4v2CUtXA==", null, false, "https://photocontest.blob.core.windows.net/youtubedemo/profile-1.jpg", null, "2472c253-8ae9-4098-9443-96ac0a624a5a", false, "admin@admin.com" });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 3, "City" },
                    { 4, "Forest" },
                    { 5, "Landscape" },
                    { 6, "Mountains" },
                    { 7, "Sci-fi" },
                    { 1, "Architecture" },
                    { 9, "Tech" },
                    { 10, "Urban" },
                    { 2, "Birds" },
                    { 8, "Sky" }
                });

            migrationBuilder.InsertData(
                table: "Ranks",
                columns: new[] { "Id", "RankName" },
                values: new object[,]
                {
                    { 1, "Junkie" },
                    { 2, "Enthusiast" },
                    { 3, "Master" },
                    { 4, "Dictator" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ApiKey", "ConcurrencyStamp", "Description", "Discriminator", "Email", "EmailConfirmed", "IsBanned", "IsDeleted", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "ProfileLink", "RankId", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { 6, 0, "", "976d0874-1467-4872-84a6-f49cfdf1dfc8", null, "User", "teodora@abv.bg", false, false, false, false, null, "TEODORA@ABV.BG", "TEODORA GEORGIEVA", "AQAAAAEAACcQAAAAECK3IoKlwNRmdQSJUsSdANnpj2yHUKspetHgZ7TIACRX6xapLRZQQ7rGYd7VF9XUmw==", null, false, "https://photocontest.blob.core.windows.net/youtubedemo/profile-7.jpg", 3, "fa0b49d3-f2f2-4957-99e5-d6a50710063b", false, "Teodora Georgieva" },
                    { 4, 0, "", "8ba1eac8-7d8b-4ae4-8926-490cc8bd2321", null, "User", "tanya@abv.bg", false, false, false, false, null, "TANYA@ABV.BG", " TANYA IVANOVA", "AQAAAAEAACcQAAAAEOuR6lMvuypizSQCgMTTKaHaS3IMQ6Vb294kpTqntPk1CnRJ+48MAtdHq0QM4u80Dg==", null, false, "https://photocontest.blob.core.windows.net/youtubedemo/profile-5.jpg", 3, "7cfbf14a-940f-4a13-8a5b-7ef3098b70ae", false, "Tanya Ivanova" },
                    { 3, 0, "", "156461ac-9ac8-4f51-af93-1229ab884a06", null, "User", "ivan@abv.bg", false, false, false, false, null, "IVAN@ABV.BG", "IVAN PETKANOV", "AQAAAAEAACcQAAAAECMZYUbKDFYqi2fnH9B4GoDwieEvCTSsNAF6Zfo+6d2TsbraLyZeyz4PME9FGp6Ikw==", null, false, "https://photocontest.blob.core.windows.net/youtubedemo/profile-4.jpg", 3, "72456f6a-ea14-4096-bd5a-0f57bd6a7d8b", false, "Ivan Petkanov" },
                    { 2, 0, "", "5cf02feb-8191-419a-99b5-0963737537c9", null, "User", "user@user.com", false, false, false, false, null, "USER@USER.COM", "USER@USER.COM", "AQAAAAEAACcQAAAAECYjXTmyP16SXJP8zzfwxnLYl6nqciZZYdG0lp4SotzHFgCddYEl96v4GwrAjq/c1g==", null, false, "https://photocontest.blob.core.windows.net/youtubedemo/profile-2.jpg", 3, "9efd0693-1623-4ea5-b3ef-ec3946d8eeff", false, "user@user.com" },
                    { 8, 0, "", "ff1f3cd7-5d3e-4646-913d-9a0b312c3fcd", null, "User", "petar@abv.bg", false, false, false, false, null, "PETAR@ABV.BG", "PETAR PESHOV", "AQAAAAEAACcQAAAAELA4nRIIF+PdZTJvpUMjni2naPhGAibKGn6CN6xt3BfE53i3C5OMp3RzdyQ3/CLlAA==", null, false, "https://photocontest.blob.core.windows.net/youtubedemo/profile-9.jpg", 2, "ec909e78-1929-413e-bba0-b754d845bf8d", false, "Petar Peshov" },
                    { 7, 0, "", "8f58be0e-b009-48e2-9c54-3a6f59756ff2", null, "User", "petya@abv.bg", false, false, false, false, null, "PETYA@ABV.BG", "PETYA VASILEVA", "AQAAAAEAACcQAAAAEIkkmCa/b0kaNVDvL/XasxE+mJfX7ZXNMns8BB45Ive14kjklDFhRi3JOMFn902qzw==", null, false, "https://photocontest.blob.core.windows.net/youtubedemo/profile-8.jpg", 2, "57ca0ff4-715f-4d33-ab4b-2cb4457d1b5f", false, "Petya Vasileva" },
                    { 5, 0, "", "ce241257-f9d9-4fa0-9846-e51c5347f727", null, "User", "vasil@abv.bg", false, false, false, false, null, "VASIL@ABV.BG", "VASIL ILIEV", "AQAAAAEAACcQAAAAEDDdqGu+ReUpZtvvz8kQCc7CdxTWKyq1Iz904yHAfckYX/sayFLMbxCsToXtVSAAsg==", null, false, "https://photocontest.blob.core.windows.net/youtubedemo/profile-6.jpg", 1, "4a4855a2-2a5b-4943-a531-bf1d55a266a1", false, "Vasil Iliev" },
                    { 9, 0, "", "5ffedb10-b9b5-4e1f-8544-1d6dee95c807", null, "User", "anatoli@abv.bg", false, false, false, false, null, "ANATOLI@ABV.BG", "ANATOLI VASILEV", "AQAAAAEAACcQAAAAEBuT8wXjsxQ0MTUD3zQ/LDkPfxPbfrFuecmuCjZaB8FjFvtzz3lWNkF6oX6gw59hKQ==", null, false, "https://photocontest.blob.core.windows.net/youtubedemo/profile-10.jpg", 3, "d0c290ad-e84e-48cd-b3b2-705bc84d0c4c", false, "Anatoli Vasilev" }
                });

            migrationBuilder.InsertData(
                table: "Contests",
                columns: new[] { "Id", "AdminId", "CategoryId", "Description", "EndDate", "IsDeleted", "Name", "ParticipantLimit", "Phase", "Profile", "StartDate", "WinnerPhoto" },
                values: new object[,]
                {
                    { 10, 1, 10, "A picture in the city should be uploaded.", new DateTime(2021, 6, 9, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3661), false, "City through the a lense.", 155, 2, "https://photocontest.blob.core.windows.net/youtubedemo/urban-12.jpg", new DateTime(2021, 6, 9, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3657), 6 },
                    { 8, 1, 8, "Look up and shot and upload the picture.", new DateTime(2021, 6, 9, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3643), false, "Sky is the limit", 165, 3, "https://photocontest.blob.core.windows.net/youtubedemo/sky-11.jpg", new DateTime(2021, 6, 9, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3639), 6 },
                    { 7, 1, 7, "Deep look into the future of forms of life.", new DateTime(2021, 6, 9, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3635), false, "A look into the future", 645, 3, "https://photocontest.blob.core.windows.net/youtubedemo/sci-12.jpg", new DateTime(2021, 6, 9, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3631), 6 },
                    { 6, 1, 6, "A collection of stunning mountains shot.", new DateTime(2021, 6, 9, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3624), false, "Beautiful mountains", 65, 3, "https://photocontest.blob.core.windows.net/youtubedemo/mountains-12.jpg", new DateTime(2021, 6, 9, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3621), 6 },
                    { 5, 1, 5, "A collection beautiful landscapes shots.", new DateTime(2021, 6, 9, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3616), false, "Breathtaking landscapes", 55, 3, "https://photocontest.blob.core.windows.net/youtubedemo/landscape-12.jpg", new DateTime(2021, 6, 9, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3612), 5 },
                    { 4, 1, 4, "The contest for all your forest photos.", new DateTime(2021, 6, 10, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3606), false, "A look into the nature.", 100, 2, "https://photocontest.blob.core.windows.net/youtubedemo/forest-11.jpg", new DateTime(2021, 6, 9, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3602), 4 },
                    { 3, 1, 3, "This is the contest for photos of cities.", new DateTime(2021, 6, 9, 16, 2, 9, 58, DateTimeKind.Local).AddTicks(3573), false, "Metropolitan areas", 30, 2, "https://photocontest.blob.core.windows.net/youtubedemo/city-11.jpg", new DateTime(2021, 6, 9, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3569), 3 },
                    { 2, 1, 2, "Contest for your birds photos.", new DateTime(2021, 6, 10, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3507), false, "Local wildlife", 100, 1, "https://photocontest.blob.core.windows.net/youtubedemo/birds-11.jpg", new DateTime(2021, 6, 9, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3478), 2 },
                    { 9, 1, 9, "A collection of random tech.", new DateTime(2021, 6, 9, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3652), false, "Random technology", 165, 2, "https://photocontest.blob.core.windows.net/youtubedemo/tech-12.jpg", new DateTime(2021, 6, 9, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(3648), 6 },
                    { 1, 1, 1, "A contest for newcomers.", new DateTime(2021, 6, 10, 15, 55, 9, 58, DateTimeKind.Local).AddTicks(1151), false, "City Architecture", 50, 1, "https://photocontest.blob.core.windows.net/youtubedemo/architecture-12.jpg", new DateTime(2021, 6, 9, 15, 55, 9, 55, DateTimeKind.Local).AddTicks(5030), 1 }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { 5, 2 },
                    { 4, 2 },
                    { 6, 2 },
                    { 7, 2 },
                    { 3, 2 },
                    { 9, 2 },
                    { 2, 2 },
                    { 8, 2 }
                });

            migrationBuilder.InsertData(
                table: "ContestUsers",
                columns: new[] { "UserId", "ContestId", "Rated" },
                values: new object[,]
                {
                    { 2, 2, false },
                    { 2, 4, false },
                    { 2, 5, false },
                    { 9, 9, false },
                    { 8, 8, false },
                    { 2, 1, false },
                    { 7, 7, false },
                    { 7, 2, false },
                    { 2, 6, false },
                    { 6, 2, true },
                    { 4, 1, true },
                    { 2, 3, false }
                });

            migrationBuilder.InsertData(
                table: "Photos",
                columns: new[] { "Id", "Aperture", "Camera", "ContestScore", "Description", "FocalLength", "ISO", "IsDeleted", "Name", "PhotoLink", "ShutterSpeed", "UserId" },
                values: new object[,]
                {
                    { 100, "f/4.1", "Iphone 10 Pro Max", 0, "Two trains in New York.", "30 mm", "1400", false, "UrbanTen", "https://photocontest.blob.core.windows.net/youtubedemo/urban-10.jpg", "1/210", 3 },
                    { 97, "f/4.1", "Iphone 10 Pro", 0, "Couple of doors.", "30 mm", "1400", false, "UrbanSeven", "https://photocontest.blob.core.windows.net/youtubedemo/urban-7.jpg", "1/210", 3 },
                    { 98, "f/4.1", "Iphone 10 Pro", 0, "Graffitti in Seattle.", "30 mm", "1400", false, "UrbanEight", "https://photocontest.blob.core.windows.net/youtubedemo/urban-8.jpg", "1/210", 3 },
                    { 5, "f/1.2", "NIKON D5600", 0, "Photo is taken at library of oslo", "18mm", "2000", false, "ArchitectureFive", "https://photocontest.blob.core.windows.net/youtubedemo/architecture-5.jpg", "2.11", 4 },
                    { 96, "f/4.1", "Iphone 10 Pro", 0, "Underground.", "30 mm", "1400", false, "UrbanSix", "https://photocontest.blob.core.windows.net/youtubedemo/urban-6.jpg", "1/210", 3 },
                    { 95, "f/4.1", "Iphone 10 Pro Max", 0, "Japan's city view.", "30 mm", "1200", false, "UrbanFive", "https://photocontest.blob.core.windows.net/youtubedemo/urban-5.jpg", "1/210", 3 },
                    { 94, "f/4.1", "Iphone 10 Pro", 0, "City view.", "30 mm", "1200", false, "UrbanFour", "https://photocontest.blob.core.windows.net/youtubedemo/urban-4.jpg", "1/210", 3 },
                    { 93, "f/4.1", "Iphone 10 Pro", 0, "Graffitti shot in New York.", "30 mm", "1200", false, "UrbanThree", "https://photocontest.blob.core.windows.net/youtubedemo/urban-3.jpg", "1/210", 3 },
                    { 92, "f/4.1", "Iphone 10 Pro", 0, "Beautiful urban shot.", "40 mm", "1200", false, "UrbanTwo", "https://photocontest.blob.core.windows.net/youtubedemo/urban-2.jpg", "1/210", 3 },
                    { 91, "f/4.1", "Lenovo P15 Pro", 0, "A beautiful looking lane road.", "40 mm", "1200", false, "UrbanOne", "https://photocontest.blob.core.windows.net/youtubedemo/urban-1.jpg", "1/210", 3 },
                    { 90, "f/4.1", "Lenovo P15 Pro", 0, "Nice setup.", "40 mm", "1200", false, "TechTen", "https://photocontest.blob.core.windows.net/youtubedemo/tech-10.jpg", "1/210", 3 },
                    { 89, "f/4.1", "Lenovo P15 Pro", 0, "A code representation of chaos.", "40 mm", "2000", false, "TechNine", "https://photocontest.blob.core.windows.net/youtubedemo/tech-9.jpg", "1/210", 3 },
                    { 88, "f/4.1", "Lenovo P15 Pro", 0, "A software engineer doing her job.", "40 mm", "2000", false, "TechEight", "https://photocontest.blob.core.windows.net/youtubedemo/tech-8.jpg", "1/210", 3 },
                    { 87, "f/3.8", "Lenovo P15", 0, "Couple of cameras used by god knows who.", "40 mm", "2000", false, "TechSeven", "https://photocontest.blob.core.windows.net/youtubedemo/tech-7.jpg", "1/210", 3 },
                    { 86, "f/3.8", "Lenovo P15", 0, "It all starts with a keyboard.", "40 mm", "2000", false, "TechSix", "https://photocontest.blob.core.windows.net/youtubedemo/tech-6.jpg", "1/210", 3 },
                    { 99, "f/4.1", "Iphone 10 Pro Max", 0, "Skyscreapers in the clouds.", "30 mm", "1400", false, "UrbanNine", "https://photocontest.blob.core.windows.net/youtubedemo/urban-9.jpg", "1/210", 3 },
                    { 6, "f/22", "Canon EOS 5D", 0, "Incredible architecture.", "18 mm", "1400", false, "ArchitectureSix", "https://photocontest.blob.core.windows.net/youtubedemo/architecture-6.jpg", "1", 4 },
                    { 54, "f/4.2", "Xiomi Redmi", 0, "Mountain with a river.", "20 mm", "1500", false, "MountainsFour", "https://photocontest.blob.core.windows.net/youtubedemo/mountains-4.jpg", "1/340", 4 },
                    { 35, "f/7.1", "Samsung Galaxy S21 Ultra", 0, "A forest path captured in the state of Washington.", "25 mm", "2000", false, "ForestFive", "https://photocontest.blob.core.windows.net/youtubedemo/forest-5.jpg", "1/140", 4 },
                    { 78, "f/3.1", "LG S15 Max Pro", 0, "A beautiful pink sky.", "15 mm", "1500", false, "SkyEight", "https://photocontest.blob.core.windows.net/youtubedemo/sky-8.jpg", "1/220", 9 },
                    { 45, "f/3.5", "Samsung Galaxy A71", 0, "Landscape that I took in Sweden.", "34 mm", "1500", false, "LandscapeFive", "https://photocontest.blob.core.windows.net/youtubedemo/landscape-5.jpg", "1/380", 9 },
                    { 44, "f/3.5", "Samsung Galaxy A71", 0, "A picturesque landscape that I took in the Netherlands.", "34 mm", "1500", false, "LandscapeFour", "https://photocontest.blob.core.windows.net/youtubedemo/landscape-4.jpg", "1/350", 9 },
                    { 43, "f/6.5", "Samsung Galaxy S20 Ultra", 0, "Beautiful landscape with a tree.", "30 mm", "1500", false, "LandscapeThree", "https://photocontest.blob.core.windows.net/youtubedemo/landscape-3.jpg", "1/350", 9 },
                    { 42, "f/6.5", "Samsung Galaxy S20 Ultra", 0, "A bridge in Slovenia.", "30 mm", "1500", false, "LandscapeTwo", "https://photocontest.blob.core.windows.net/youtubedemo/landscape-2.jpg", "1/350", 9 },
                    { 24, "f/5", "Apple iPhone 11", 0, "Monochromic view of New York.", "50 mm", "1300", false, "CityFour", "https://photocontest.blob.core.windows.net/youtubedemo/city-4.jpg", "1/2973", 9 },
                    { 18, "f/2.2", "Xiaomi Redmi Note 5 Pro", 0, "A duckling posing.", "182 mm", "2500", false, "BirdsEight", "https://photocontest.blob.core.windows.net/youtubedemo/birds-8.jpg", "1/640", 9 },
                    { 15, "f/1.9", "Samsung SM-A705FN", 0, "Two birds that standed still while i was making this picture in my garden.", "3.92 mm", "2500", false, "BirdsFive", "https://photocontest.blob.core.windows.net/youtubedemo/birds-5.jpg", "1/200", 9 },
                    { 67, "f/5.23", "Huawei P10", 0, "Formation of galaxies are presented on this photo.", "15 mm", "1500", false, "SciSeven", "https://photocontest.blob.core.windows.net/youtubedemo/sci-7.jpg", "1/250", 6 },
                    { 66, "f/5.23", "Huawei P10", 0, "Milky way in the background.", "15 mm", "1500", false, "SciSix", "https://photocontest.blob.core.windows.net/youtubedemo/sci-6.jpg", "1/250", 6 },
                    { 65, "f/5.23", "Huawei P10", 0, "Some other planet beings and Saturn in the back.", "25 mm", "1500", false, "SciFive", "https://photocontest.blob.core.windows.net/youtubedemo/sci-5.jpg", "1/250", 6 },
                    { 64, "f/5.23", "Huawei P10", 0, "Abstract image of a mind.", "25 mm", "1000", false, "SciFour", "https://photocontest.blob.core.windows.net/youtubedemo/sci-4.jpg", "1/240", 6 },
                    { 38, "f/3.54", "FUJIFILM 5200", 0, "Winter trees at Rhodopa mountain in Bulgaria.", "45 mm", "2000", false, "ForestEight", "https://photocontest.blob.core.windows.net/youtubedemo/forest-8.jpg", "1/540", 6 },
                    { 37, "f/4.554", "FUJIFILM 5000", 0, "Beautiful forest sunset at Rila park in Bulgaria.", "25 mm", "2000", false, "ForestSeven", "https://photocontest.blob.core.windows.net/youtubedemo/forest-7.jpg", "1/540", 6 },
                    { 36, "f/4.5", "FUJIFILM 5533", 0, "Beautiful forest sunset at Rila park in Bulgaria.", "15 mm", "2000", false, "ForestSix", "https://photocontest.blob.core.windows.net/youtubedemo/forest-6.jpg", "1/140", 6 },
                    { 10, "f/28", "Canon EOS R", 0, "Nice italian cathedral.", "20.2 mm", "400", false, "ArchitectureTen", "https://photocontest.blob.core.windows.net/youtubedemo/architecture-10.jpg", "1/250", 6 },
                    { 9, "f/20", "FUJIFILM X-T3", 0, "A cozy house to stay in.", "24 mm", "120", false, "ArchitectureNine", "https://photocontest.blob.core.windows.net/youtubedemo/architecture-9.jpg", "1/5", 6 },
                    { 85, "f/3.8", "Lenovo P15", 0, "Minimalistis setup.", "35 mm", "2000", false, "TechFive", "https://photocontest.blob.core.windows.net/youtubedemo/tech-5.jpg", "1/210", 4 },
                    { 84, "f/3.8", "Lenovo P15", 0, "Minimalistis setup for programming.", "35 mm", "2000", false, "TechFour", "https://photocontest.blob.core.windows.net/youtubedemo/tech-4.jpg", "1/260", 4 },
                    { 83, "f/3.8", "Lenovo P15", 0, "A nice setup for gaming.", "25 mm", "2000", false, "TechThree", "https://photocontest.blob.core.windows.net/youtubedemo/tech-3.jpg", "1/260", 4 },
                    { 82, "f/3.8", "Lenovo P15", 0, "Windmills located near the border with Mexico.", "15 mm", "1500", false, "TechTwo", "https://photocontest.blob.core.windows.net/youtubedemo/tech-2.jpg", "1/260", 4 },
                    { 81, "f/3.8", "Lenovo P15", 0, "A computer and my favorite thing in the world: coffee.", "15 mm", "1500", false, "TechOne", "https://photocontest.blob.core.windows.net/youtubedemo/tech-1.jpg", "1/260", 4 },
                    { 51, "f/3.54", "Samsung Galaxy Fold", 0, "A beautiful mountain shot at Switzerland.", "40 mm", "1500", false, "MountainsOne", "https://photocontest.blob.core.windows.net/youtubedemo/mountains-1.jpg", "1/340", 3 },
                    { 53, "f/4.2", "Xiomi Redmi", 0, "A pretty green meadow with mountain.", "20 mm", "1500", false, "MountainsThree", "https://photocontest.blob.core.windows.net/youtubedemo/mountains-3.jpg", "1/340", 4 },
                    { 52, "f/5.2", "Xiomi Redmi", 0, "Timeless mountains.", "20 mm", "1500", false, "MountainsTwo", "https://photocontest.blob.core.windows.net/youtubedemo/mountains-2.jpg", "1/340", 4 },
                    { 34, "f/7.4", "Samsung Galaxy S21", 0, "A whole bunch of trees located south of Ukraine's capital.", "30 mm", "2000", false, "ForestFour", "https://photocontest.blob.core.windows.net/youtubedemo/forest-4.jpg", "1/150", 4 },
                    { 50, "f/3.54", "Samsung Galaxy Fold", 0, "A beautiful path.", "40 mm", "1500", false, "LandscapeTen", "https://photocontest.blob.core.windows.net/youtubedemo/landscape-10.jpg", "1/340", 3 },
                    { 3, "f/1", "FUJIFILM X-T20", 0, "Architectural masterpiece located in Seattle.", "12 mm", "2000", false, "ArchitectureThree", "https://photocontest.blob.core.windows.net/youtubedemo/architecture-3.jpg", "2", 3 },
                    { 30, "f/5", "Nikon EOS 5D", 0, "Bunch of skyscreapers in Manila.", "4.5 mm", "1400", false, "CityTen", "https://photocontest.blob.core.windows.net/youtubedemo/city-10.jpg", "1/15", 3 },
                    { 39, "f/3.54", "FUJIFILM 5200", 0, "The path to one of the most beautiful views in Germany.", "45 mm", "2000", false, "ForestNine", "https://photocontest.blob.core.windows.net/youtubedemo/forest-9.jpg", "1/540", 8 },
                    { 26, "f/15", "Apple iPhone 12", 0, "Arena Armeec.", "4.11 mm", "2000", false, "CitySix", "https://photocontest.blob.core.windows.net/youtubedemo/city-6.jpg", "1/1500", 8 },
                    { 25, "f/19", "Apple iPhone 11", 0, "San Francisco at night.", "4.11 mm", "200", false, "CityFive", "https://photocontest.blob.core.windows.net/youtubedemo/city-5.jpg", "1/1500", 8 },
                    { 19, "f/2", "Canon EOS 5D Mark III", 0, "Two parrots caught fighting in the Philippines.", "172 mm", "1000", false, "BirdsNine", "https://photocontest.blob.core.windows.net/youtubedemo/birds-9.jpg", "1/600", 8 },
                    { 14, "f/2", "Canon EOS R", 0, "A beautiful parrot, Brazil", "3.22 mm", "2000", false, "BirdsFour", "https://photocontest.blob.core.windows.net/youtubedemo/birds-4.jpg", "1/300", 8 },
                    { 13, "f/8", "Apple iPhone XS", 0, "Two pigeons shot at Osaka.", "4 mm", "1500", false, "BirdsThree", "https://photocontest.blob.core.windows.net/youtubedemo/birds-3.jpg", "1/300", 8 },
                    { 58, "f/3.23", "Huawei P20", 0, "A breath-taking view.", "45 mm", "1500", false, "MountainsEight", "https://photocontest.blob.core.windows.net/youtubedemo/mountains-8.jpg", "1/340", 7 },
                    { 57, "f/3.23", "Xiomi Redmi Note", 0, "A snowy mountain in Bulgaria.", "35 mm", "1500", false, "MountainsSeven", "https://photocontest.blob.core.windows.net/youtubedemo/mountains-7.jpg", "1/340", 7 },
                    { 56, "f/3.23", "Xiomi Redmi Note", 0, "A beautiful shot near Austria's capital.", "15 mm", "1500", false, "MountainsSix", "https://photocontest.blob.core.windows.net/youtubedemo/mountains-6.jpg", "1/340", 7 },
                    { 55, "f/3.2", "Xiomi Redmi", 0, "Mountain with a sky.", "15 mm", "1500", false, "MountainsFive", "https://photocontest.blob.core.windows.net/youtubedemo/mountains-5.jpg", "1/340", 7 },
                    { 22, "f/5", "Canon EOS 5D", 0, "New York at sundown.", "70 mm", "1400", false, "CityTwo", "https://photocontest.blob.core.windows.net/youtubedemo/city-2.jpg", "1/400", 7 },
                    { 12, "f/12", "Apple iPhone XS", 0, "A beautiful sunset with birds over balck sea.", "4.25 mm", "1000", false, "BirdsTwo", "https://photocontest.blob.core.windows.net/youtubedemo/birds-2.jpg", "1/230", 7 },
                    { 11, "f/13", "Canon EOS R", 0, "Birds shot at Ohio state", "20 mm ", "this is ISO number 11", false, "BirdsOne", "https://photocontest.blob.core.windows.net/youtubedemo/birds-1.jpg", "1/200", 7 },
                    { 73, "f/2.4", "LG S15 Pro", 0, "Location of this picture is Slovenia.", "30 mm", "2500", false, "SkyThree", "https://photocontest.blob.core.windows.net/youtubedemo/sky-3.jpg", "1/250", 5 },
                    { 72, "f/2.4", "LG S15 Pro", 0, "This beautiful sky is located south of Dhaka, Bangladesh.", "30 mm", "2500", false, "SkyTwo", "https://photocontest.blob.core.windows.net/youtubedemo/sky-2.jpg", "1/250", 5 },
                    { 71, "f/2.4", "LG S15 Pro", 0, "This picture is taken near La Paz, Bolivia.", "30 mm", "1500", false, "SkyOne", "https://photocontest.blob.core.windows.net/youtubedemo/sky-1.jpg", "1/250", 5 },
                    { 48, "f/3.5", "Samsung Galaxy A72", 0, "I was able to make this shot in the Philippines.", "40 mm", "1500", false, "LandscapeEight", "https://photocontest.blob.core.windows.net/youtubedemo/landscape-8.jpg", "1/480", 5 },
                    { 47, "f/3.5", "Samsung Galaxy A72", 0, "Windows like photo", "40 mm", "2000", false, "LandscapeSeven", "https://photocontest.blob.core.windows.net/youtubedemo/landscape-7.jpg", "1/380", 5 },
                    { 46, "f/3.5", "Samsung Galaxy A72", 0, "A photo taken in sunny Spain", "39 mm", "2000", false, "LandscapeSix", "https://photocontest.blob.core.windows.net/youtubedemo/landscape-6.jpg", "1/380", 5 },
                    { 28, "f/4", "Apple iPhone 10", 0, "Washington's skyscreaper.", "2.5 mm", "2000", false, "CityEight", "https://photocontest.blob.core.windows.net/youtubedemo/city-8.jpg", "1/150", 5 },
                    { 27, "f/9", "Apple iPhone 12 PRO", 0, "New York at night.", "3.11 mm", "200", false, "CitySeven", "https://photocontest.blob.core.windows.net/youtubedemo/city-7.jpg", "1/150", 5 },
                    { 8, "f/21", "Canon EOS 5D", 0, "Nice place to visit while in Norway", "20mm", "100", false, "ArchitectureEight", "https://photocontest.blob.core.windows.net/youtubedemo/architecture-8.jpg", "1", 5 },
                    { 7, "f/21", "NIKON D5500", 0, "Bunch of skyscreapers.", "22 mm", "1500", false, "ArchitectureSeven", "https://photocontest.blob.core.windows.net/youtubedemo/architecture-7.jpg", "1/60", 5 },
                    { 40, "f/6.5", "Samsung Galaxy S20", 0, "The gorgeous forest located in northern Switzerland.", "50 mm", "2000", false, "ForestTen", "https://photocontest.blob.core.windows.net/youtubedemo/forest-10.jpg", "1/540", 8 },
                    { 49, "f/3.5", "Samsung Galaxy A72", 0, "A beautiful river.", "35 mm", "1500", false, "LandscapeNine", "https://photocontest.blob.core.windows.net/youtubedemo/landscape-9.jpg", "1/340", 3 },
                    { 41, "f/6.5", "Samsung Galaxy S20", 0, "This picture was shot in Spain.", "40 mm", "1500", false, "LandscapeOne", "https://photocontest.blob.core.windows.net/youtubedemo/landscape-1.jpg", "1/520", 8 },
                    { 60, "f/3.23", "Huawei P20", 0, "A town with lights and mountain.", "45 mm", "2500", false, "MountainsTen", "https://photocontest.blob.core.windows.net/youtubedemo/mountains-10.jpg", "1/240", 8 },
                    { 29, "f/2", "Apple iPhone 10 PRO", 0, "A sidewalk located in Palm's Springs.", "2.5 mm", "2000", false, "CityNine", "https://photocontest.blob.core.windows.net/youtubedemo/city-9.jpg", "1/15", 3 },
                    { 20, "f/2", "Canon EOS 5D Mark III", 0, "Pellicans doing pellican things.", "3.81 mm", "1300", false, "BirdsTen", "https://photocontest.blob.core.windows.net/youtubedemo/birds-10.jpg", "1/600", 3 },
                    { 16, "f/8", "Samsung SM-A705FN", 0, "Two magical parrots posing for the shot", "3.95 mm", "2000", false, "BirdsSix", "https://photocontest.blob.core.windows.net/youtubedemo/birds-6.jpg", "1/150", 3 },
                    { 4, "f/1.2", "OnePlus ONE E1003", 0, "this photo is located in oslo, norway", "3.79 mm", "100", false, "ArchitectureFour", "https://photocontest.blob.core.windows.net/youtubedemo/architecture-4.jpg", "1/140", 3 },
                    { 79, "f/3.8", "LG S15 Max Pro", 0, "50 kilometers north of Varna", "15 mm", "1500", false, "SkyNine", "https://photocontest.blob.core.windows.net/youtubedemo/sky-9.jpg", "1/260", 9 },
                    { 70, "f/1.9", "LG S15 Pro", 0, "A stream of light coming from the brightest star in the galaxy.", "15 mm", "1500", false, "SciTen", "https://photocontest.blob.core.windows.net/youtubedemo/sci-10.jpg", "1/250", 2 },
                    { 69, "f/6.9", "Huawei P10", 0, "An owl staring at your soul.", "25 mm", "1500", false, "SciNine", "https://photocontest.blob.core.windows.net/youtubedemo/sci-9.jpg", "1/250", 2 },
                    { 68, "f/5.23", "Huawei P10", 0, "Otherwordly.", "25 mm", "1500", false, "SciEight", "https://photocontest.blob.core.windows.net/youtubedemo/sci-8.jpg", "1/250", 2 },
                    { 33, "f/3", "Samsung Galaxy A5", 0, "A milenium old tree in Montana, USA.", "30 mm", "1500", false, "ForestThree", "https://photocontest.blob.core.windows.net/youtubedemo/forest-3.jpg", "1/176", 2 },
                    { 32, "f/3", "Nikon EOS 5D PRO", 0, "Misty mountains in Serbia.", "50 mm", "1500", false, "ForestTwo", "https://photocontest.blob.core.windows.net/youtubedemo/forest-2.jpg", "1/176", 2 },
                    { 31, "f/5", "Nikon EOS 5D", 0, "This particular photo is shot near Sofia, Bulgaria.", "4.5 mm", "1400", false, "ForestOne", "https://photocontest.blob.core.windows.net/youtubedemo/forest-1.jpg", "1/166", 2 },
                    { 23, "f/4", "Canon EOS 5D", 0, "Coffeeshop in seattle.", "50 mm", "1200", false, "CityThree", "https://photocontest.blob.core.windows.net/youtubedemo/city-3.jpg", "1/200", 2 },
                    { 21, "f/9", "Canon EOS 5D", 0, "Night at New York.", "255 mm", "500", false, "CityOne", "https://photocontest.blob.core.windows.net/youtubedemo/city-1.jpg", "1/555", 2 },
                    { 17, "f/6", "Xiaomi Redmi Note 5 Pro", 0, "Couple of seagulls over the sea.", "3.61 mm", "1000", false, "BirdsSeven", "https://photocontest.blob.core.windows.net/youtubedemo/birds-7.jpg", "1/640", 2 },
                    { 2, "f/6.0", "SONY ILCE-9", 0, "Nice view of a house.", "580mm", "1600", false, "ArchitectureTwo", "https://photocontest.blob.core.windows.net/youtubedemo/architecture-2.jpg", "1/66", 2 },
                    { 1, "f/6.1", "SONY ILCE-9", 0, "An artist constructing their next project.", "576mm", "1500", false, "ArchitectureOne", "https://photocontest.blob.core.windows.net/youtubedemo/architecture-1.jpg", "1/205", 2 },
                    { 77, "f/3.1", "LG S15 Max", 0, "Around 30 kilometers from Cape Town.", "15 mm", "1500", false, "SkySeven", "https://photocontest.blob.core.windows.net/youtubedemo/sky-7.jpg", "1/220", 8 },
                    { 76, "f/3.1", "LG S15 Max", 0, "15 kilometers south of Milan.", "15 mm", "1500", false, "SkySix", "https://photocontest.blob.core.windows.net/youtubedemo/sky-6.jpg", "1/220", 8 },
                    { 75, "f/3.1", "LG S15 Max", 0, "This photo is taken 50 kilometers west of Sarajevo.", "15 mm", "2500", false, "SkyFive", "https://photocontest.blob.core.windows.net/youtubedemo/sky-5.jpg", "1/220", 8 },
                    { 74, "f/3.1", "LG S15 Max", 0, "Located in Argentina, in particular north of Buenos Aires.", "30 mm", "2500", false, "SkyFour", "https://photocontest.blob.core.windows.net/youtubedemo/sky-4.jpg", "1/220", 8 },
                    { 63, "f/5.23", "Huawei P20 Pro", 0, "A UFO near mountains.", "25 mm", "1000", false, "SciThree", "https://photocontest.blob.core.windows.net/youtubedemo/sci-3.jpg", "1/240", 8 },
                    { 62, "f/3.23", "Huawei P20 Pro", 0, "A outworld hand.", "25 mm", "1500", false, "SciTwo", "https://photocontest.blob.core.windows.net/youtubedemo/sci-2.jpg", "1/240", 8 },
                    { 61, "f/3.23", "Huawei P20 Pro", 0, "A man holding a laser sword.", "45 mm", "1500", false, "SciOne", "https://photocontest.blob.core.windows.net/youtubedemo/sci-1.jpg", "1/240", 8 },
                    { 59, "f/3.23", "Huawei P20", 0, "Really nice lake with mountain.", "45 mm", "2500", false, "MountainsNine", "https://photocontest.blob.core.windows.net/youtubedemo/mountains-9.jpg", "1/240", 8 },
                    { 80, "f/3.8", "LG S15 Max Pro", 0, "50 kilometers north of Burgas", "15 mm", "1500", false, "SkyTen", "https://photocontest.blob.core.windows.net/youtubedemo/sky-10.jpg", "1/260", 9 }
                });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "CommentTime", "Description", "IsDeleted", "PhotoId", "UserId" },
                values: new object[,]
                {
                    { 9, new DateTime(2021, 6, 11, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9218), "I'd like this photo to be on my screen", false, 7, 2 },
                    { 5, new DateTime(2021, 6, 11, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9201), "The reflections added a whole new eye-catching dimension to the photo!", false, 4, 7 },
                    { 4, new DateTime(2021, 6, 11, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9197), "The perfect foreground created a totally enigmatic image!", false, 3, 5 },
                    { 36, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9333), "Oh! Very beautiful.", false, 33, 7 },
                    { 35, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9329), "Oh! Very beautiful.", false, 33, 5 },
                    { 34, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9325), "Oh! Very beautiful, I'm falling in love with this image.", false, 31, 5 },
                    { 25, new DateTime(2021, 8, 30, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9288), "Smart capture of the skyline – no part was wasted.", false, 23, 2 },
                    { 6, new DateTime(2021, 6, 11, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9205), "Great capture of the architecture’s intricate details!", false, 4, 5 },
                    { 23, new DateTime(2021, 6, 11, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9280), "The photo makes the viewer wish that he/she was there.", false, 21, 2 },
                    { 2, new DateTime(2021, 6, 14, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9141), "Perfectly framed architecture!", false, 2, 2 },
                    { 1, new DateTime(2021, 6, 10, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(8034), "Perfect contrast achieved through finding the best direction of the light!", false, 1, 4 },
                    { 42, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9362), "Beautiful colors.", false, 41, 6 },
                    { 41, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9358), "Beautiful colors.", false, 41, 5 },
                    { 40, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9351), "Looking gorgeous.", false, 41, 5 },
                    { 39, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9347), "Lighting is incredible.", false, 39, 5 },
                    { 19, new DateTime(2021, 6, 11, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9264), "The contrast is amazing.", false, 17, 2 },
                    { 28, new DateTime(2021, 9, 5, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9300), "The clean lines saved the photo from being a chaotic jumble of elements!", false, 26, 2 },
                    { 18, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9260), "I have the same camera but cannot shoot a photo like this.", false, 16, 2 },
                    { 33, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9321), "Amazing.", false, 29, 5 },
                    { 43, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9366), "This picture is eloquent like words.", false, 45, 6 },
                    { 26, new DateTime(2021, 6, 11, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9292), "Crisp and sharp image of the city!", false, 24, 3 },
                    { 20, new DateTime(2021, 6, 11, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9267), "I really like the light in the photo", false, 18, 2 },
                    { 17, new DateTime(2021, 6, 11, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9252), "Really good.", false, 15, 2 },
                    { 38, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9344), "Impressive picture.", false, 37, 5 },
                    { 12, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9231), "Smart incorporation of motions into the picture!", false, 10, 2 },
                    { 22, new DateTime(2021, 8, 30, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9276), "Really good.", false, 20, 2 },
                    { 11, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9227), "I can feel the photo", false, 9, 2 },
                    { 48, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9386), "Nice colors.", false, 53, 3 },
                    { 47, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9382), "Vibrant colors.", false, 52, 3 },
                    { 37, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9340), "Impressive picture.", false, 35, 7 },
                    { 8, new DateTime(2021, 6, 11, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9214), "A brilliant mix of exceptional architecture and everyday objects – the people, the trees, and the busy streets!", false, 6, 2 },
                    { 7, new DateTime(2021, 6, 21, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9210), "An interesting play of perspective!", false, 5, 3 },
                    { 46, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9378), "Vibrant colors.", false, 50, 3 },
                    { 49, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9390), "Impressive.", false, 53, 3 },
                    { 27, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9296), "The leading lines made for an intriguing feel!", false, 25, 2 },
                    { 3, new DateTime(2021, 6, 11, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9191), "Beautiful architecture silhouette – perfectly timed photo resulting to the perfect colors and perfect effects!", false, 3, 2 },
                    { 44, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9371), "Impressive.", false, 48, 6 },
                    { 21, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9272), "I want it to a wallpaper", false, 19, 3 },
                    { 24, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9284), "The colourful contrast between the dark sky and city lights is definitely captivating!", false, 22, 2 },
                    { 32, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9317), "Amazing, I have never seen a photo like this.", false, 28, 5 },
                    { 31, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9313), "Compelling photo of the city! ", false, 28, 2 },
                    { 16, new DateTime(2021, 8, 30, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9249), "I really like the angle.", false, 14, 4 },
                    { 50, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9394), "Impressive.", false, 55, 3 },
                    { 15, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9245), "Such a pretty photo.", false, 13, 2 },
                    { 13, new DateTime(2021, 8, 30, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9236), "Amazing!", false, 11, 5 },
                    { 30, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9308), "The patterns in the photo created a lovely harmony!", false, 28, 2 },
                    { 10, new DateTime(2021, 6, 11, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9223), "It looks like I'm there", false, 8, 2 },
                    { 14, new DateTime(2021, 6, 11, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9240), "Gorgeous!", false, 12, 6 },
                    { 45, new DateTime(2021, 6, 17, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9375), "Such a scenic view.", false, 48, 3 },
                    { 29, new DateTime(2021, 6, 11, 15, 55, 9, 59, DateTimeKind.Local).AddTicks(9304), "The clean lines saved the photo from being a chaotic jumble of elements!", false, 27, 2 }
                });

            migrationBuilder.InsertData(
                table: "PhotoScoreContests",
                columns: new[] { "PhotoId", "ContestId", "Score" },
                values: new object[,]
                {
                    { 46, 5, 0 },
                    { 28, 3, 0 },
                    { 53, 6, 0 },
                    { 54, 6, 0 },
                    { 52, 6, 0 },
                    { 35, 4, 0 },
                    { 25, 3, 0 },
                    { 34, 4, 0 },
                    { 6, 1, 50 },
                    { 5, 1, 50 },
                    { 100, 10, 0 },
                    { 99, 10, 0 },
                    { 98, 10, 0 },
                    { 97, 10, 0 },
                    { 96, 10, 0 },
                    { 95, 10, 0 },
                    { 94, 10, 0 },
                    { 93, 10, 0 },
                    { 47, 5, 0 },
                    { 81, 9, 0 },
                    { 85, 9, 0 },
                    { 83, 9, 0 },
                    { 78, 8, 0 },
                    { 45, 5, 0 },
                    { 7, 1, 51 },
                    { 44, 5, 0 },
                    { 43, 5, 0 },
                    { 42, 5, 0 },
                    { 24, 3, 0 },
                    { 18, 2, 0 },
                    { 8, 1, 26 },
                    { 15, 2, 0 },
                    { 82, 9, 0 },
                    { 67, 7, 0 },
                    { 65, 7, 0 },
                    { 64, 7, 0 },
                    { 38, 4, 0 },
                    { 37, 4, 0 },
                    { 27, 3, 0 },
                    { 36, 4, 0 },
                    { 10, 1, 50 },
                    { 9, 1, 15 },
                    { 92, 10, 0 },
                    { 84, 9, 0 },
                    { 66, 7, 0 },
                    { 91, 10, 0 },
                    { 87, 9, 0 },
                    { 89, 9, 0 },
                    { 21, 3, 0 },
                    { 55, 6, 0 },
                    { 17, 2, 0 },
                    { 56, 6, 0 },
                    { 2, 1, 50 },
                    { 57, 6, 0 },
                    { 1, 1, 60 },
                    { 58, 6, 0 },
                    { 77, 8, 0 },
                    { 76, 8, 0 },
                    { 75, 8, 0 },
                    { 74, 8, 0 },
                    { 63, 7, 0 },
                    { 62, 7, 0 },
                    { 61, 7, 0 },
                    { 60, 6, 0 },
                    { 59, 6, 0 },
                    { 41, 5, 0 },
                    { 13, 2, 0 },
                    { 40, 4, 0 },
                    { 39, 4, 0 },
                    { 14, 2, 0 },
                    { 26, 3, 0 },
                    { 23, 3, 0 },
                    { 90, 9, 0 },
                    { 22, 3, 0 },
                    { 32, 4, 0 },
                    { 88, 9, 0 },
                    { 19, 2, 0 },
                    { 86, 9, 0 },
                    { 51, 6, 0 },
                    { 50, 5, 0 },
                    { 48, 5, 0 },
                    { 49, 5, 0 },
                    { 30, 3, 0 },
                    { 29, 3, 0 },
                    { 71, 8, 0 },
                    { 20, 2, 0 },
                    { 72, 8, 0 },
                    { 16, 2, 0 },
                    { 73, 8, 0 },
                    { 4, 1, 50 },
                    { 11, 2, 0 },
                    { 3, 1, 50 },
                    { 79, 8, 0 },
                    { 70, 7, 0 },
                    { 69, 7, 0 },
                    { 68, 7, 0 },
                    { 33, 4, 0 },
                    { 12, 2, 0 },
                    { 31, 4, 0 },
                    { 80, 8, 0 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_RankId",
                table: "AspNetUsers",
                column: "RankId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_PhotoId",
                table: "Comments",
                column: "PhotoId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_UserId",
                table: "Comments",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Contests_AdminId",
                table: "Contests",
                column: "AdminId");

            migrationBuilder.CreateIndex(
                name: "IX_Contests_CategoryId",
                table: "Contests",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ContestUsers_ContestId",
                table: "ContestUsers",
                column: "ContestId");

            migrationBuilder.CreateIndex(
                name: "IX_FollowFollowed_FollowerId",
                table: "FollowFollowed",
                column: "FollowerId");

            migrationBuilder.CreateIndex(
                name: "IX_LabelPhotos_LabelId",
                table: "LabelPhotos",
                column: "LabelId");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_UserId",
                table: "Photos",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PhotoScoreContests_ContestId",
                table: "PhotoScoreContests",
                column: "ContestId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "ContestUsers");

            migrationBuilder.DropTable(
                name: "FollowFollowed");

            migrationBuilder.DropTable(
                name: "LabelPhotos");

            migrationBuilder.DropTable(
                name: "PhotoScoreContests");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Labels");

            migrationBuilder.DropTable(
                name: "Contests");

            migrationBuilder.DropTable(
                name: "Photos");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Ranks");
        }
    }
}
