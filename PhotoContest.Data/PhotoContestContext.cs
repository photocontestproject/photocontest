﻿using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNet.Identity.EntityFramework;

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data.Configurations;
using PhotoContest.Data.Models;
using PhotoContest.Data.Seed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PhotoContest.Data
{

    public class PhotoContestContext : IdentityDbContext<User, Role, int>
    {
        public PhotoContestContext(DbContextOptions<PhotoContestContext> options)
            : base(options)
        {
        }

        public DbSet<Admin> Admins { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Contest> Contests { get; set; }

        public DbSet<ContestUser> ContestUsers { get; set; }

        public DbSet<Label> Labels { get; set; }

        public DbSet<LabelPhoto> LabelPhotos { get; set; }

        public DbSet<Photo> Photos { get; set; }

        public DbSet<PhotoScoreContest> PhotoScoreContests { get; set; }

        public DbSet<Rank> Ranks { get; set; }

        public DbSet<Category> Categories { get; set; }




        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var foreignKey in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                foreignKey.DeleteBehavior = DeleteBehavior.NoAction;
            }

            modelBuilder.ApplyConfiguration(new LabelPhotoConfig());
            modelBuilder.ApplyConfiguration(new PhotoScoresContestConfig());
            modelBuilder.ApplyConfiguration(new UserConfig());
            modelBuilder.ApplyConfiguration(new AdminConfig());
            modelBuilder.ApplyConfiguration(new ContestUserConfig());
            modelBuilder.ApplyConfiguration(new FolloweFollowedConfig());

            modelBuilder.Seed();

            base.OnModelCreating(modelBuilder);
        }
    }
}
