﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Configurations
{
    public class FolloweFollowedConfig : IEntityTypeConfiguration<FollowFollowed>
    {
        public void Configure(EntityTypeBuilder<FollowFollowed> entity)
        {
            entity.HasKey(x => new { x.FollowedId, x.FollowerId });

            entity.HasOne(followedFollowing => followedFollowing.Follower)
                .WithMany(followed => followed.Followed)
                .HasForeignKey(followedFollowing => followedFollowing.FollowerId).OnDelete(DeleteBehavior.Restrict);

            entity.HasOne(followedFollowing => followedFollowing.Followed)
                .WithMany(follower => follower.Followers)
                .HasForeignKey(followedFollowing => followedFollowing.FollowedId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
