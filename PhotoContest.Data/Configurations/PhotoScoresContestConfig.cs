﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Configurations
{
    public class PhotoScoresContestConfig : IEntityTypeConfiguration<PhotoScoreContest>
    {
        public void Configure(EntityTypeBuilder<PhotoScoreContest> entity)
        {
            entity.HasKey(psc => new { psc.PhotoId, psc.ContestId });

            entity.HasOne(psc => psc.Photo)
                .WithMany(ph => ph.PhotoScoreContests)
                .HasForeignKey(psc => psc.PhotoId);

            entity.HasOne(psc => psc.Contest)
            .WithMany(ph => ph.PhotoScoreContests)
            .HasForeignKey(psc => psc.ContestId);
        }
    }
}
