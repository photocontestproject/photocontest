﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Configurations
{
    public class LabelPhotoConfig : IEntityTypeConfiguration<LabelPhoto>
    {
        public void Configure(EntityTypeBuilder<LabelPhoto> entity)
        {
            entity.HasKey(label => new { label.PhotoId, label.LabelId });

            entity.HasOne(labelPh => labelPh.Label)
                .WithMany(label => label.LabelPhotos)
                .HasForeignKey(labelPh => labelPh.LabelId);

            entity.HasOne(labelPh => labelPh.Photo)
                .WithMany(photo => photo.LabelPhotos)
                .HasForeignKey(labelPh => labelPh.PhotoId);
        }
    }
}
