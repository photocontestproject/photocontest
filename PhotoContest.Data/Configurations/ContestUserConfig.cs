﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Configurations
{
    public class ContestUserConfig : IEntityTypeConfiguration<ContestUser>
    {
        public void Configure(EntityTypeBuilder<ContestUser> entity)
        {
            entity.HasKey(contest => new { contest.UserId, contest.ContestId });

            entity.HasOne(contUser => contUser.Contest)
                .WithMany(contest => contest.Users)
                .HasForeignKey(contUser => contUser.ContestId);

            entity.HasOne(contUser => contUser.User)
                .WithMany(user => user.Contests)
                .HasForeignKey(contUser => contUser.UserId);
        }
    }
}
