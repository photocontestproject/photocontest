﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContest.Data.Seed
{
    public static class ModelBuilderExtension
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            // Roles
            var role1 = new Role();
            role1.Id = 1;
            role1.Name = "Admin";
            role1.NormalizedName = "ADMIN";

            var role2 = new Role();
            role2.Id = 2;
            role2.Name = "User";
            role2.NormalizedName = "USER";

            var roles = new List<Role>() { role1, role2 };

            modelBuilder.Entity<Role>().HasData(roles);



            // Password hasher
            var passHasher = new Microsoft.AspNetCore.Identity.PasswordHasher<User>();

            // Admin
            var adminUser = new Admin();
            adminUser.Id = 1;
            adminUser.UserName = "admin@admin.com";
            adminUser.NormalizedUserName = "ADMIN@ADMIN.COM";
            adminUser.Email = "admin@admin.com";
            adminUser.NormalizedEmail = "ADMIN@ADMIN.COM";
            adminUser.PasswordHash = passHasher.HashPassword(adminUser, "admin123");
            adminUser.SecurityStamp = Guid.NewGuid().ToString();
            adminUser.ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-1.jpg";
            adminUser.ApiKey = "";
            modelBuilder.Entity<Admin>().HasData(adminUser);

            // Link Role & User (for Admin)
            var adminUserRole = new IdentityUserRole<int>();
            adminUserRole.RoleId = 1;
            adminUserRole.UserId = adminUser.Id;
            modelBuilder.Entity<IdentityUserRole<int>>().HasData(adminUserRole);

            // Regular User
            var regularUser = new User();
            regularUser.Id = 2;
            regularUser.UserName = "user@user.com";
            regularUser.NormalizedUserName = "USER@USER.COM";
            regularUser.Email = "user@user.com";
            regularUser.NormalizedEmail = "USER@USER.COM";
            regularUser.PasswordHash = passHasher.HashPassword(regularUser, "user123");
            regularUser.SecurityStamp = Guid.NewGuid().ToString();
            regularUser.RankId = 3;
            regularUser.ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-2.jpg";
            regularUser.ApiKey = "";
            modelBuilder.Entity<User>().HasData(regularUser);

            // Link Role & User (for Regular User)
            var regularUserRole = new IdentityUserRole<int>();
            regularUserRole.RoleId = 2;
            regularUserRole.UserId = regularUser.Id;
            modelBuilder.Entity<IdentityUserRole<int>>().HasData(regularUserRole);


            var regularUser3 = new User();
            regularUser3.Id = 3;
            regularUser3.UserName = "Ivan Petkanov";
            regularUser3.NormalizedUserName = "IVAN PETKANOV";
            regularUser3.Email = "ivan@abv.bg";
            regularUser3.NormalizedEmail = "IVAN@ABV.BG";
            regularUser3.PasswordHash = passHasher.HashPassword(regularUser3, "user123");
            regularUser3.SecurityStamp = Guid.NewGuid().ToString();
            regularUser3.RankId = 3;
            regularUser3.ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-4.jpg";
            regularUser3.ApiKey = "";
            modelBuilder.Entity<User>().HasData(regularUser3);

            // Link Role & User (for Regular User)
            var regularUserRole3 = new IdentityUserRole<int>();
            regularUserRole3.RoleId = 2;
            regularUserRole3.UserId = regularUser3.Id;
            modelBuilder.Entity<IdentityUserRole<int>>().HasData(regularUserRole3);


            var regularUser4 = new User();
            regularUser4.Id = 4;
            regularUser4.UserName = "Tanya Ivanova";
            regularUser4.NormalizedUserName = " TANYA IVANOVA";
            regularUser4.Email = "tanya@abv.bg";
            regularUser4.NormalizedEmail = "TANYA@ABV.BG";
            regularUser4.PasswordHash = passHasher.HashPassword(regularUser4, "user123");
            regularUser4.SecurityStamp = Guid.NewGuid().ToString();
            regularUser4.RankId = 3;
            regularUser4.ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-5.jpg";
            regularUser4.ApiKey = "";
            modelBuilder.Entity<User>().HasData(regularUser4);

            // Link Role & User (for Regular User)
            var regularUserRole4 = new IdentityUserRole<int>();
            regularUserRole4.RoleId = 2;
            regularUserRole4.UserId = regularUser4.Id;
            modelBuilder.Entity<IdentityUserRole<int>>().HasData(regularUserRole4);


            var regularUser5 = new User();
            regularUser5.Id = 5;
            regularUser5.UserName = "Vasil Iliev";
            regularUser5.NormalizedUserName = "VASIL ILIEV";
            regularUser5.Email = "vasil@abv.bg";
            regularUser5.NormalizedEmail = "VASIL@ABV.BG";
            regularUser5.PasswordHash = passHasher.HashPassword(regularUser4, "user123");
            regularUser5.SecurityStamp = Guid.NewGuid().ToString();
            regularUser5.RankId = 1;
            regularUser5.ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-6.jpg";
            regularUser5.ApiKey = "";
            modelBuilder.Entity<User>().HasData(regularUser5);

            // Link Role & User (for Regular User)
            var regularUserRole5 = new IdentityUserRole<int>();
            regularUserRole5.RoleId = 2;
            regularUserRole5.UserId = regularUser5.Id;
            modelBuilder.Entity<IdentityUserRole<int>>().HasData(regularUserRole5);


            var regularUser6 = new User();
            regularUser6.Id = 6;
            regularUser6.UserName = "Teodora Georgieva";
            regularUser6.NormalizedUserName = "TEODORA GEORGIEVA";
            regularUser6.Email = "teodora@abv.bg";
            regularUser6.NormalizedEmail = "TEODORA@ABV.BG";
            regularUser6.PasswordHash = passHasher.HashPassword(regularUser4, "user123");
            regularUser6.SecurityStamp = Guid.NewGuid().ToString();
            regularUser6.RankId = 3;
            regularUser6.ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-7.jpg";
            regularUser6.ApiKey = "";
            modelBuilder.Entity<User>().HasData(regularUser6);

            // Link Role & User (for Regular User)
            var regularUserRole6 = new IdentityUserRole<int>();
            regularUserRole6.RoleId = 2;
            regularUserRole6.UserId = regularUser6.Id;
            modelBuilder.Entity<IdentityUserRole<int>>().HasData(regularUserRole6);


            var regularUser7 = new User();
            regularUser7.Id = 7;
            regularUser7.UserName = "Petya Vasileva";
            regularUser7.NormalizedUserName = "PETYA VASILEVA";
            regularUser7.Email = "petya@abv.bg";
            regularUser7.NormalizedEmail = "PETYA@ABV.BG";
            regularUser7.PasswordHash = passHasher.HashPassword(regularUser4, "user123");
            regularUser7.SecurityStamp = Guid.NewGuid().ToString();
            regularUser7.RankId = 2;
            regularUser7.ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-8.jpg";
            regularUser7.ApiKey = "";
            modelBuilder.Entity<User>().HasData(regularUser7);

            // Link Role & User (for Regular User)
            var regularUserRole7 = new IdentityUserRole<int>();
            regularUserRole7.RoleId = 2;
            regularUserRole7.UserId = regularUser7.Id;
            modelBuilder.Entity<IdentityUserRole<int>>().HasData(regularUserRole7);


            var regularUser8 = new User();
            regularUser8.Id = 8;
            regularUser8.UserName = "Petar Peshov";
            regularUser8.NormalizedUserName = "PETAR PESHOV";
            regularUser8.Email = "petar@abv.bg";
            regularUser8.NormalizedEmail = "PETAR@ABV.BG";
            regularUser8.PasswordHash = passHasher.HashPassword(regularUser4, "user123");
            regularUser8.SecurityStamp = Guid.NewGuid().ToString();
            regularUser8.RankId = 2;
            regularUser8.ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-9.jpg";
            regularUser8.ApiKey = "";
            modelBuilder.Entity<User>().HasData(regularUser8);

            // Link Role & User (for Regular User)
            var regularUserRole8 = new IdentityUserRole<int>();
            regularUserRole8.RoleId = 2;
            regularUserRole8.UserId = regularUser8.Id;
            modelBuilder.Entity<IdentityUserRole<int>>().HasData(regularUserRole8);


            var regularUser9 = new User();
            regularUser9.Id = 9;
            regularUser9.UserName = "Anatoli Vasilev";
            regularUser9.NormalizedUserName = "ANATOLI VASILEV";
            regularUser9.Email = "anatoli@abv.bg";
            regularUser9.NormalizedEmail = "ANATOLI@ABV.BG";
            regularUser9.PasswordHash = passHasher.HashPassword(regularUser4, "user123");
            regularUser9.SecurityStamp = Guid.NewGuid().ToString();
            regularUser9.RankId = 3;
            regularUser9.ProfileLink = "https://photocontest.blob.core.windows.net/youtubedemo/profile-10.jpg";
            regularUser9.ApiKey = "";
            modelBuilder.Entity<User>().HasData(regularUser9);

            // Link Role & User (for Regular User)
            var regularUserRole9 = new IdentityUserRole<int>();
            regularUserRole9.RoleId = 2;
            regularUserRole9.UserId = regularUser9.Id;
            modelBuilder.Entity<IdentityUserRole<int>>().HasData(regularUserRole9);


            var contest = new Contest();
            contest.Id = 1;
            contest.Name = "City Architecture";
            contest.IsDeleted = false;
            contest.ParticipantLimit = 50;
            contest.Profile = "https://photocontest.blob.core.windows.net/youtubedemo/architecture-12.jpg";
            contest.StartDate = DateTime.Now;
            contest.EndDate = DateTime.Now.AddDays(1);
            contest.Description = "A contest for newcomers.";
            contest.WinnerPhoto = 1;
            contest.CategoryId = 1;
            contest.Phase = 1;
            contest.AdminId = 1;



            var contest2 = new Contest();
            contest2.Id = 2;
            contest2.Name = "Local wildlife";
            contest2.IsDeleted = false;
            contest2.ParticipantLimit = 100;
            contest2.Profile = "https://photocontest.blob.core.windows.net/youtubedemo/birds-11.jpg";
            contest2.StartDate = DateTime.Now;
            contest2.EndDate = DateTime.Now.AddDays(1);
            contest2.Description = "Contest for your birds photos.";
            contest2.WinnerPhoto = 2;
            contest2.CategoryId = 2;
            contest2.Phase = 1;
            contest2.AdminId = 1;

            var contest3 = new Contest();
            contest3.Id = 3;
            contest3.Name = "Metropolitan areas";
            contest3.IsDeleted = false;
            contest3.ParticipantLimit = 30;
            contest3.Profile = "https://photocontest.blob.core.windows.net/youtubedemo/city-11.jpg";
            contest3.StartDate = DateTime.Now;
            contest3.EndDate = DateTime.Now.AddMinutes(7);
            contest3.Description = "This is the contest for photos of cities.";
            contest3.WinnerPhoto = 3;
            contest3.CategoryId = 3;
            contest3.Phase = 2;
            contest3.AdminId = 1;

            var contest4 = new Contest();
            contest4.Id = 4;
            contest4.Name = "A look into the nature.";
            contest4.IsDeleted = false;
            contest4.ParticipantLimit = 100;
            contest4.Profile = "https://photocontest.blob.core.windows.net/youtubedemo/forest-11.jpg";
            contest4.StartDate = DateTime.Now;
            contest4.EndDate = DateTime.Now.AddDays(1);
            contest4.Description = "The contest for all your forest photos.";
            contest4.WinnerPhoto = 4;
            contest4.CategoryId = 4;
            contest4.Phase = 2;
            contest4.AdminId = 1;

            var contest5 = new Contest();
            contest5.Id = 5;
            contest5.Name = "Breathtaking landscapes";
            contest5.IsDeleted = false;
            contest5.ParticipantLimit = 55;
            contest5.Profile = "https://photocontest.blob.core.windows.net/youtubedemo/landscape-12.jpg";
            contest5.StartDate = DateTime.Now;
            contest5.EndDate = DateTime.Now;
            contest5.Description = "A collection beautiful landscapes shots.";
            contest5.WinnerPhoto = 5;
            contest5.CategoryId = 5;
            contest5.Phase = 3;
            contest5.AdminId = 1;

            var contest6 = new Contest();
            contest6.Id = 6;
            contest6.Name = "Beautiful mountains";
            contest6.IsDeleted = false;
            contest6.ParticipantLimit = 65;
            contest6.Profile = "https://photocontest.blob.core.windows.net/youtubedemo/mountains-12.jpg";
            contest6.StartDate = DateTime.Now;
            contest6.EndDate = DateTime.Now;
            contest6.Description = "A collection of stunning mountains shot.";
            contest6.WinnerPhoto = 6;
            contest6.CategoryId = 6;
            contest6.Phase = 3;
            contest6.AdminId = 1;

            var contest7 = new Contest();
            contest7.Id = 7;
            contest7.Name = "A look into the future";
            contest7.IsDeleted = false;
            contest7.ParticipantLimit = 645;
            contest7.Profile = "https://photocontest.blob.core.windows.net/youtubedemo/sci-12.jpg";
            contest7.StartDate = DateTime.Now;
            contest7.EndDate = DateTime.Now;
            contest7.Description = "Deep look into the future of forms of life.";
            contest7.WinnerPhoto = 6;
            contest7.CategoryId = 7;
            contest7.Phase = 3;
            contest7.AdminId = 1;

            var contest8 = new Contest();
            contest8.Id = 8;
            contest8.Name = "Sky is the limit";
            contest8.IsDeleted = false;
            contest8.ParticipantLimit = 165;
            contest8.Profile = "https://photocontest.blob.core.windows.net/youtubedemo/sky-11.jpg";
            contest8.StartDate = DateTime.Now;
            contest8.EndDate = DateTime.Now;
            contest8.Description = "Look up and shot and upload the picture.";
            contest8.WinnerPhoto = 6;
            contest8.CategoryId = 8;
            contest8.Phase = 3;
            contest8.AdminId = 1;

            var contest9 = new Contest();
            contest9.Id = 9;
            contest9.Name = "Random technology";
            contest9.IsDeleted = false;
            contest9.ParticipantLimit = 165;
            contest9.Profile = "https://photocontest.blob.core.windows.net/youtubedemo/tech-12.jpg";
            contest9.StartDate = DateTime.Now;
            contest9.EndDate = DateTime.Now;
            contest9.Description = "A collection of random tech.";
            contest9.WinnerPhoto = 6;
            contest9.CategoryId = 9;
            contest9.Phase = 2;
            contest9.AdminId = 1;

            var contest10 = new Contest();
            contest10.Id = 10;
            contest10.Name = "City through the a lense.";
            contest10.IsDeleted = false;
            contest10.ParticipantLimit = 155;
            contest10.Profile = "https://photocontest.blob.core.windows.net/youtubedemo/urban-12.jpg";
            contest10.StartDate = DateTime.Now;
            contest10.EndDate = DateTime.Now;
            contest10.Description = "A picture in the city should be uploaded.";
            contest10.WinnerPhoto = 6;
            contest10.CategoryId = 10;
            contest10.Phase = 2;
            contest10.AdminId = 1;





            var contests = new List<Contest>()
            {
                contest,contest2,contest3,contest4,
                contest5,contest6,contest7,contest8,
                contest9,contest10
            };

            modelBuilder.Entity<Contest>().HasData(contests);

            var categories = new List<Category>()
            {
                new Category()
                {
                    Id = 1,
                    Name = "Architecture"
                },
                 new Category()
                 {
                    Id = 2,
                    Name = "Birds"
                 },
                 new Category()
                 {
                    Id = 3,
                    Name = "City"
                 },
                 new Category()
                 {
                     Id = 4,
                     Name = "Forest"
                 },
                 new Category()
                 {
                     Id = 5,
                     Name = "Landscape"
                 },
                 new Category()
                 {
                     Id = 6,
                     Name = "Mountains"
                 },
                 new Category()
                 {
                     Id = 7,
                     Name = "Sci-fi"
                 },
                 new Category()
                 {
                     Id = 8,
                     Name = "Sky"
                 },
                 new Category()
                 {
                     Id = 9,
                     Name = "Tech"
                 },
                 new Category()
                 {
                     Id = 10,
                     Name = "Urban"
                 }

            };
            modelBuilder.Entity<Category>().HasData(categories);


            var photo = new Photo();
            photo.Aperture = "f/6.1";
            photo.Camera = "SONY ILCE-9";
            photo.Description = "An artist constructing their next project.";
            photo.FocalLength = "576mm";
            photo.Id = 1;
            photo.ISO = "1500";
            photo.Name = "ArchitectureOne";
            photo.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/architecture-1.jpg";
            photo.ShutterSpeed = "1/205";
            photo.UserId = 2;

            var photo2 = new Photo();
            photo2.Aperture = "f/6.0";
            photo2.Camera = "SONY ILCE-9";
            photo2.Description = "Nice view of a house.";
            photo2.FocalLength = "580mm";
            photo2.Id = 2;
            photo2.ISO = "1600";
            photo2.Name = "ArchitectureTwo";
            photo2.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/architecture-2.jpg";
            photo2.ShutterSpeed = "1/66";
            photo2.UserId = 2;

            var photo3 = new Photo();
            photo3.Aperture = "f/1";
            photo3.Camera = "FUJIFILM X-T20";
            photo3.Description = "Architectural masterpiece located in Seattle.";
            photo3.FocalLength = "12 mm";
            photo3.Id = 3;
            photo3.ISO = "2000";
            photo3.Name = "ArchitectureThree";
            photo3.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/architecture-3.jpg";
            photo3.ShutterSpeed = "2";
            photo3.UserId = 3;

            var photo4 = new Photo();
            photo4.Aperture = "f/1.2";
            photo4.Camera = "OnePlus ONE E1003";
            photo4.Description = "this photo is located in oslo, norway";
            photo4.FocalLength = "3.79 mm";
            photo4.Id = 4;
            photo4.ISO = "100";
            photo4.Name = "ArchitectureFour";
            photo4.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/architecture-4.jpg";
            photo4.ShutterSpeed = "1/140";
            photo4.UserId = 3;

            var photo5 = new Photo();
            photo5.Aperture = "f/1.2";
            photo5.Camera = "NIKON D5600";
            photo5.Description = "Photo is taken at library of oslo";
            photo5.FocalLength = "18mm";
            photo5.Id = 5;
            photo5.ISO = "2000";
            photo5.Name = "ArchitectureFive";
            photo5.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/architecture-5.jpg";
            photo5.ShutterSpeed = "2.11";
            photo5.UserId = 4;

            var photo6 = new Photo();
            photo6.Aperture = "f/22";
            photo6.Camera = "Canon EOS 5D";
            photo6.Description = "Incredible architecture.";
            photo6.FocalLength = "18 mm";
            photo6.Id = 6;
            photo6.ISO = "1400";
            photo6.Name = "ArchitectureSix";
            photo6.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/architecture-6.jpg";
            photo6.ShutterSpeed = "1";
            photo6.UserId = 4;

            var photo7 = new Photo();
            photo7.Aperture = "f/21";
            photo7.Camera = "NIKON D5500";
            photo7.Description = "Bunch of skyscreapers.";
            photo7.FocalLength = "22 mm";
            photo7.Id = 7;
            photo7.ISO = "1500";
            photo7.Name = "ArchitectureSeven";
            photo7.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/architecture-7.jpg";
            photo7.ShutterSpeed = "1/60";
            photo7.UserId = 5;

            var photo8 = new Photo();
            photo8.Aperture = "f/21";
            photo8.Camera = "Canon EOS 5D";
            photo8.Description = "Nice place to visit while in Norway";
            photo8.FocalLength = "20mm";
            photo8.Id = 8;
            photo8.ISO = "100";
            photo8.Name = "ArchitectureEight";
            photo8.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/architecture-8.jpg";
            photo8.ShutterSpeed = "1";
            photo8.UserId = 5;

            var photo9 = new Photo();
            photo9.Aperture = "f/20";
            photo9.Camera = "FUJIFILM X-T3";
            photo9.Description = "A cozy house to stay in.";
            photo9.FocalLength = "24 mm";
            photo9.Id = 9;
            photo9.ISO = "120";
            photo9.Name = "ArchitectureNine";
            photo9.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/architecture-9.jpg";
            photo9.ShutterSpeed = "1/5";
            photo9.UserId = 6;

            var photo10 = new Photo();
            photo10.Aperture = "f/28";
            photo10.Camera = "Canon EOS R";
            photo10.Description = "Nice italian cathedral.";
            photo10.FocalLength = "20.2 mm";
            photo10.Id = 10;
            photo10.ISO = "400";
            photo10.Name = "ArchitectureTen";
            photo10.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/architecture-10.jpg";
            photo10.ShutterSpeed = "1/250";
            photo10.UserId = 6;

            var photo11 = new Photo();
            photo11.Aperture = "f/13";
            photo11.Camera = "Canon EOS R";
            photo11.Description = "Birds shot at Ohio state";
            photo11.FocalLength = "20 mm ";
            photo11.Id = 11;
            photo11.ISO = "this is ISO number 11";
            photo11.Name = "BirdsOne";
            photo11.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/birds-1.jpg";
            photo11.ShutterSpeed = "1/200";
            photo11.UserId = 7;

            var photo12 = new Photo();
            photo12.Aperture = "f/12";
            photo12.Camera = "Apple iPhone XS";
            photo12.Description = "A beautiful sunset with birds over balck sea.";
            photo12.FocalLength = "4.25 mm";
            photo12.Id = 12;
            photo12.ISO = "1000";
            photo12.Name = "BirdsTwo";
            photo12.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/birds-2.jpg";
            photo12.ShutterSpeed = "1/230";
            photo12.UserId = 7;

            var photo13 = new Photo();
            photo13.Aperture = "f/8";
            photo13.Camera = "Apple iPhone XS";
            photo13.Description = "Two pigeons shot at Osaka.";
            photo13.FocalLength = "4 mm";
            photo13.Id = 13;
            photo13.ISO = "1500";
            photo13.Name = "BirdsThree";
            photo13.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/birds-3.jpg";
            photo13.ShutterSpeed = "1/300";
            photo13.UserId = 8;

            var photo14 = new Photo();
            photo14.Aperture = "f/2";
            photo14.Camera = "Canon EOS R";
            photo14.Description = "A beautiful parrot, Brazil";
            photo14.FocalLength = "3.22 mm";
            photo14.Id = 14;
            photo14.ISO = "2000";
            photo14.Name = "BirdsFour";
            photo14.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/birds-4.jpg";
            photo14.ShutterSpeed = "1/300";
            photo14.UserId = 8;

            var photo15 = new Photo();
            photo15.Aperture = "f/1.9";
            photo15.Camera = "Samsung SM-A705FN";
            photo15.Description = "Two birds that standed still while i was making this picture in my garden.";
            photo15.FocalLength = "3.92 mm";
            photo15.Id = 15;
            photo15.ISO = "2500";
            photo15.Name = "BirdsFive";
            photo15.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/birds-5.jpg";
            photo15.ShutterSpeed = "1/200";
            photo15.UserId = 9;

            var photo16 = new Photo();
            photo16.Aperture = "f/8";
            photo16.Camera = "Samsung SM-A705FN";
            photo16.Description = "Two magical parrots posing for the shot";
            photo16.FocalLength = "3.95 mm";
            photo16.Id = 16;
            photo16.ISO = "2000";
            photo16.Name = "BirdsSix";
            photo16.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/birds-6.jpg";
            photo16.ShutterSpeed = "1/150";
            photo16.UserId = 3;

            var photo17 = new Photo();
            photo17.Aperture = "f/6";
            photo17.Camera = "Xiaomi Redmi Note 5 Pro";
            photo17.Description = "Couple of seagulls over the sea.";
            photo17.FocalLength = "3.61 mm";
            photo17.Id = 17;
            photo17.ISO = "1000";
            photo17.Name = "BirdsSeven";
            photo17.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/birds-7.jpg";
            photo17.ShutterSpeed = "1/640";
            photo17.UserId = 2;

            var photo18 = new Photo();
            photo18.Aperture = "f/2.2";
            photo18.Camera = "Xiaomi Redmi Note 5 Pro";
            photo18.Description = "A duckling posing.";
            photo18.FocalLength = "182 mm";
            photo18.Id = 18;
            photo18.ISO = "2500";
            photo18.Name = "BirdsEight";
            photo18.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/birds-8.jpg";
            photo18.ShutterSpeed = "1/640";
            photo18.UserId = 9;

            var photo19 = new Photo();
            photo19.Aperture = "f/2";
            photo19.Camera = "Canon EOS 5D Mark III";
            photo19.Description = "Two parrots caught fighting in the Philippines.";
            photo19.FocalLength = "172 mm";
            photo19.Id = 19;
            photo19.ISO = "1000";
            photo19.Name = "BirdsNine";
            photo19.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/birds-9.jpg";
            photo19.ShutterSpeed = "1/600";
            photo19.UserId = 8;

            var photo20 = new Photo();
            photo20.Aperture = "f/2";
            photo20.Camera = "Canon EOS 5D Mark III";
            photo20.Description = "Pellicans doing pellican things.";
            photo20.FocalLength = "3.81 mm";
            photo20.Id = 20;
            photo20.ISO = "1300";
            photo20.Name = "BirdsTen";
            photo20.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/birds-10.jpg";
            photo20.ShutterSpeed = "1/600";
            photo20.UserId = 3;

            var photo21 = new Photo();
            photo21.Aperture = "f/9";
            photo21.Camera = "Canon EOS 5D";
            photo21.Description = "Night at New York.";
            photo21.FocalLength = "255 mm";
            photo21.Id = 21;
            photo21.ISO = "500";
            photo21.Name = "CityOne";
            photo21.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/city-1.jpg";
            photo21.ShutterSpeed = "1/555";
            photo21.UserId = 2;

            var photo22 = new Photo();
            photo22.Aperture = "f/5";
            photo22.Camera = "Canon EOS 5D";
            photo22.Description = "New York at sundown.";
            photo22.FocalLength = "70 mm";
            photo22.Id = 22;
            photo22.ISO = "1400";
            photo22.Name = "CityTwo";
            photo22.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/city-2.jpg";
            photo22.ShutterSpeed = "1/400";
            photo22.UserId = 7;

            var photo23 = new Photo();
            photo23.Aperture = "f/4";
            photo23.Camera = "Canon EOS 5D";
            photo23.Description = "Coffeeshop in seattle.";
            photo23.FocalLength = "50 mm";
            photo23.Id = 23;
            photo23.ISO = "1200";
            photo23.Name = "CityThree";
            photo23.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/city-3.jpg";
            photo23.ShutterSpeed = "1/200";
            photo23.UserId = 2;

            var photo24 = new Photo();
            photo24.Aperture = "f/5";
            photo24.Camera = "Apple iPhone 11";
            photo24.Description = "Monochromic view of New York.";
            photo24.FocalLength = "50 mm";
            photo24.Id = 24;
            photo24.ISO = "1300";
            photo24.Name = "CityFour";
            photo24.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/city-4.jpg";
            photo24.ShutterSpeed = "1/2973";
            photo24.UserId = 9;

            var photo25 = new Photo();
            photo25.Aperture = "f/19";
            photo25.Camera = "Apple iPhone 11";
            photo25.Description = "San Francisco at night.";
            photo25.FocalLength = "4.11 mm";
            photo25.Id = 25;
            photo25.ISO = "200";
            photo25.Name = "CityFive";
            photo25.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/city-5.jpg";
            photo25.ShutterSpeed = "1/1500";
            photo25.UserId = 8;

            var photo26 = new Photo();
            photo26.Aperture = "f/15";
            photo26.Camera = "Apple iPhone 12";
            photo26.Description = "Arena Armeec.";
            photo26.FocalLength = "4.11 mm";
            photo26.Id = 26;
            photo26.ISO = "2000";
            photo26.Name = "CitySix";
            photo26.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/city-6.jpg";
            photo26.ShutterSpeed = "1/1500";
            photo26.UserId = 8;

            var photo27 = new Photo();
            photo27.Aperture = "f/9";
            photo27.Camera = "Apple iPhone 12 PRO";
            photo27.Description = "New York at night.";
            photo27.FocalLength = "3.11 mm";
            photo27.Id = 27;
            photo27.ISO = "200";
            photo27.Name = "CitySeven";
            photo27.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/city-7.jpg";
            photo27.ShutterSpeed = "1/150";
            photo27.UserId = 5;

            var photo28 = new Photo();
            photo28.Aperture = "f/4";
            photo28.Camera = "Apple iPhone 10";
            photo28.Description = "Washington's skyscreaper.";
            photo28.FocalLength = "2.5 mm";
            photo28.Id = 28;
            photo28.ISO = "2000";
            photo28.Name = "CityEight";
            photo28.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/city-8.jpg";
            photo28.ShutterSpeed = "1/150";
            photo28.UserId = 5;

            var photo29 = new Photo();
            photo29.Aperture = "f/2";
            photo29.Camera = "Apple iPhone 10 PRO";
            photo29.Description = "A sidewalk located in Palm's Springs.";
            photo29.FocalLength = "2.5 mm";
            photo29.Id = 29;
            photo29.ISO = "2000";
            photo29.Name = "CityNine";
            photo29.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/city-9.jpg";
            photo29.ShutterSpeed = "1/15";
            photo29.UserId = 3;

            var photo30 = new Photo();
            photo30.Aperture = "f/5";
            photo30.Camera = "Nikon EOS 5D";
            photo30.Description = "Bunch of skyscreapers in Manila.";
            photo30.FocalLength = "4.5 mm";
            photo30.Id = 30;
            photo30.ISO = "1400";
            photo30.Name = "CityTen";
            photo30.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/city-10.jpg";
            photo30.ShutterSpeed = "1/15";
            photo30.UserId = 3;

            var photo31 = new Photo();
            photo31.Aperture = "f/5";
            photo31.Camera = "Nikon EOS 5D";
            photo31.Description = "This particular photo is shot near Sofia, Bulgaria.";
            photo31.FocalLength = "4.5 mm";
            photo31.Id = 31;
            photo31.ISO = "1400";
            photo31.Name = "ForestOne";
            photo31.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/forest-1.jpg";
            photo31.ShutterSpeed = "1/166";
            photo31.UserId = 2;

            var photo32 = new Photo();
            photo32.Aperture = "f/3";
            photo32.Camera = "Nikon EOS 5D PRO";
            photo32.Description = "Misty mountains in Serbia.";
            photo32.FocalLength = "50 mm";
            photo32.Id = 32;
            photo32.ISO = "1500";
            photo32.Name = "ForestTwo";
            photo32.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/forest-2.jpg";
            photo32.ShutterSpeed = "1/176";
            photo32.UserId = 2;

            var photo33 = new Photo();
            photo33.Aperture = "f/3";
            photo33.Camera = "Samsung Galaxy A5";
            photo33.Description = "A milenium old tree in Montana, USA.";
            photo33.FocalLength = "30 mm";
            photo33.Id = 33;
            photo33.ISO = "1500";
            photo33.Name = "ForestThree";
            photo33.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/forest-3.jpg";
            photo33.ShutterSpeed = "1/176";
            photo33.UserId = 2;

            var photo34 = new Photo();
            photo34.Aperture = "f/7.4";
            photo34.Camera = "Samsung Galaxy S21";
            photo34.Description = "A whole bunch of trees located south of Ukraine's capital.";
            photo34.FocalLength = "30 mm";
            photo34.Id = 34;
            photo34.ISO = "2000";
            photo34.Name = "ForestFour";
            photo34.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/forest-4.jpg";
            photo34.ShutterSpeed = "1/150";
            photo34.UserId = 4;

            var photo35 = new Photo();
            photo35.Aperture = "f/7.1";
            photo35.Camera = "Samsung Galaxy S21 Ultra";
            photo35.Description = "A forest path captured in the state of Washington.";
            photo35.FocalLength = "25 mm";
            photo35.Id = 35;
            photo35.ISO = "2000";
            photo35.Name = "ForestFive";
            photo35.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/forest-5.jpg";
            photo35.ShutterSpeed = "1/140";
            photo35.UserId = 4;

            var photo36 = new Photo();
            photo36.Aperture = "f/4.5";
            photo36.Camera = "FUJIFILM 5533";
            photo36.Description = "Beautiful forest sunset at Rila park in Bulgaria.";
            photo36.FocalLength = "15 mm";
            photo36.Id = 36;
            photo36.ISO = "2000";
            photo36.Name = "ForestSix";
            photo36.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/forest-6.jpg";
            photo36.ShutterSpeed = "1/140";
            photo36.UserId = 6;

            var photo37 = new Photo();
            photo37.Aperture = "f/4.554";
            photo37.Camera = "FUJIFILM 5000";
            photo37.Description = "Beautiful forest sunset at Rila park in Bulgaria.";
            photo37.FocalLength = "25 mm";
            photo37.Id = 37;
            photo37.ISO = "2000";
            photo37.Name = "ForestSeven";
            photo37.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/forest-7.jpg";
            photo37.ShutterSpeed = "1/540";
            photo37.UserId = 6;

            var photo38 = new Photo();
            photo38.Aperture = "f/3.54";
            photo38.Camera = "FUJIFILM 5200";
            photo38.Description = "Winter trees at Rhodopa mountain in Bulgaria.";
            photo38.FocalLength = "45 mm";
            photo38.Id = 38;
            photo38.ISO = "2000";
            photo38.Name = "ForestEight";
            photo38.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/forest-8.jpg";
            photo38.ShutterSpeed = "1/540";
            photo38.UserId = 6;

            var photo39 = new Photo();
            photo39.Aperture = "f/3.54";
            photo39.Camera = "FUJIFILM 5200";
            photo39.Description = "The path to one of the most beautiful views in Germany.";
            photo39.FocalLength = "45 mm";
            photo39.Id = 39;
            photo39.ISO = "2000";
            photo39.Name = "ForestNine";
            photo39.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/forest-9.jpg";
            photo39.ShutterSpeed = "1/540";
            photo39.UserId = 8;

            var photo40 = new Photo();
            photo40.Aperture = "f/6.5";
            photo40.Camera = "Samsung Galaxy S20";
            photo40.Description = "The gorgeous forest located in northern Switzerland.";
            photo40.FocalLength = "50 mm";
            photo40.Id = 40;
            photo40.ISO = "2000";
            photo40.Name = "ForestTen";
            photo40.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/forest-10.jpg";
            photo40.ShutterSpeed = "1/540";
            photo40.UserId = 8;

            var photo41 = new Photo();
            photo41.Aperture = "f/6.5";
            photo41.Camera = "Samsung Galaxy S20";
            photo41.Description = "This picture was shot in Spain.";
            photo41.FocalLength = "40 mm";
            photo41.Id = 41;
            photo41.ISO = "1500";
            photo41.Name = "LandscapeOne";
            photo41.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/landscape-1.jpg";
            photo41.ShutterSpeed = "1/520";
            photo41.UserId = 8;

            var photo42 = new Photo();
            photo42.Aperture = "f/6.5";
            photo42.Camera = "Samsung Galaxy S20 Ultra";
            photo42.Description = "A bridge in Slovenia.";
            photo42.FocalLength = "30 mm";
            photo42.Id = 42;
            photo42.ISO = "1500";
            photo42.Name = "LandscapeTwo";
            photo42.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/landscape-2.jpg";
            photo42.ShutterSpeed = "1/350";
            photo42.UserId = 9;

            var photo43 = new Photo();
            photo43.Aperture = "f/6.5";
            photo43.Camera = "Samsung Galaxy S20 Ultra";
            photo43.Description = "Beautiful landscape with a tree.";
            photo43.FocalLength = "30 mm";
            photo43.Id = 43;
            photo43.ISO = "1500";
            photo43.Name = "LandscapeThree";
            photo43.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/landscape-3.jpg";
            photo43.ShutterSpeed = "1/350";
            photo43.UserId = 9;

            var photo44 = new Photo();
            photo44.Aperture = "f/3.5";
            photo44.Camera = "Samsung Galaxy A71";
            photo44.Description = "A picturesque landscape that I took in the Netherlands.";
            photo44.FocalLength = "34 mm";
            photo44.Id = 44;
            photo44.ISO = "1500";
            photo44.Name = "LandscapeFour";
            photo44.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/landscape-4.jpg";
            photo44.ShutterSpeed = "1/350";
            photo44.UserId = 9;

            var photo45 = new Photo();
            photo45.Aperture = "f/3.5";
            photo45.Camera = "Samsung Galaxy A71";
            photo45.Description = "Landscape that I took in Sweden.";
            photo45.FocalLength = "34 mm";
            photo45.Id = 45;
            photo45.ISO = "1500";
            photo45.Name = "LandscapeFive";
            photo45.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/landscape-5.jpg";
            photo45.ShutterSpeed = "1/380";
            photo45.UserId = 9;

            var photo46 = new Photo();
            photo46.Aperture = "f/3.5";
            photo46.Camera = "Samsung Galaxy A72";
            photo46.Description = "A photo taken in sunny Spain";
            photo46.FocalLength = "39 mm";
            photo46.Id = 46;
            photo46.ISO = "2000";
            photo46.Name = "LandscapeSix";
            photo46.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/landscape-6.jpg";
            photo46.ShutterSpeed = "1/380";
            photo46.UserId = 5;

            var photo47 = new Photo();
            photo47.Aperture = "f/3.5";
            photo47.Camera = "Samsung Galaxy A72";
            photo47.Description = "Windows like photo";
            photo47.FocalLength = "40 mm";
            photo47.Id = 47;
            photo47.ISO = "2000";
            photo47.Name = "LandscapeSeven";
            photo47.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/landscape-7.jpg";
            photo47.ShutterSpeed = "1/380";
            photo47.UserId = 5;

            var photo48 = new Photo();
            photo48.Aperture = "f/3.5";
            photo48.Camera = "Samsung Galaxy A72";
            photo48.Description = "I was able to make this shot in the Philippines.";
            photo48.FocalLength = "40 mm";
            photo48.Id = 48;
            photo48.ISO = "1500";
            photo48.Name = "LandscapeEight";
            photo48.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/landscape-8.jpg";
            photo48.ShutterSpeed = "1/480";
            photo48.UserId = 5;

            var photo49 = new Photo();
            photo49.Aperture = "f/3.5";
            photo49.Camera = "Samsung Galaxy A72";
            photo49.Description = "A beautiful river.";
            photo49.FocalLength = "35 mm";
            photo49.Id = 49;
            photo49.ISO = "1500";
            photo49.Name = "LandscapeNine";
            photo49.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/landscape-9.jpg";
            photo49.ShutterSpeed = "1/340";
            photo49.UserId = 3;

            var photo50 = new Photo();
            photo50.Aperture = "f/3.54";
            photo50.Camera = "Samsung Galaxy Fold";
            photo50.Description = "A beautiful path.";
            photo50.FocalLength = "40 mm";
            photo50.Id = 50;
            photo50.ISO = "1500";
            photo50.Name = "LandscapeTen";
            photo50.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/landscape-10.jpg";
            photo50.ShutterSpeed = "1/340";
            photo50.UserId = 3;

            var photo51 = new Photo();
            photo51.Aperture = "f/3.54";
            photo51.Camera = "Samsung Galaxy Fold";
            photo51.Description = "A beautiful mountain shot at Switzerland.";
            photo51.FocalLength = "40 mm";
            photo51.Id = 51;
            photo51.ISO = "1500";
            photo51.Name = "MountainsOne";
            photo51.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/mountains-1.jpg";
            photo51.ShutterSpeed = "1/340";
            photo51.UserId = 3;

            var photo52 = new Photo();
            photo52.Aperture = "f/5.2";
            photo52.Camera = "Xiomi Redmi";
            photo52.Description = "Timeless mountains.";
            photo52.FocalLength = "20 mm";
            photo52.Id = 52;
            photo52.ISO = "1500";
            photo52.Name = "MountainsTwo";
            photo52.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/mountains-2.jpg";
            photo52.ShutterSpeed = "1/340";
            photo52.UserId = 4;

            var photo53 = new Photo();
            photo53.Aperture = "f/4.2";
            photo53.Camera = "Xiomi Redmi";
            photo53.Description = "A pretty green meadow with mountain.";
            photo53.FocalLength = "20 mm";
            photo53.Id = 53;
            photo53.ISO = "1500";
            photo53.Name = "MountainsThree";
            photo53.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/mountains-3.jpg";
            photo53.ShutterSpeed = "1/340";
            photo53.UserId = 4;

            var photo54 = new Photo();
            photo54.Aperture = "f/4.2";
            photo54.Camera = "Xiomi Redmi";
            photo54.Description = "Mountain with a river.";
            photo54.FocalLength = "20 mm";
            photo54.Id = 54;
            photo54.ISO = "1500";
            photo54.Name = "MountainsFour";
            photo54.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/mountains-4.jpg";
            photo54.ShutterSpeed = "1/340";
            photo54.UserId = 4;

            var photo55 = new Photo();
            photo55.Aperture = "f/3.2";
            photo55.Camera = "Xiomi Redmi";
            photo55.Description = "Mountain with a sky.";
            photo55.FocalLength = "15 mm";
            photo55.Id = 55;
            photo55.ISO = "1500";
            photo55.Name = "MountainsFive";
            photo55.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/mountains-5.jpg";
            photo55.ShutterSpeed = "1/340";
            photo55.UserId = 7;

            var photo56 = new Photo();
            photo56.Aperture = "f/3.23";
            photo56.Camera = "Xiomi Redmi Note";
            photo56.Description = "A beautiful shot near Austria's capital.";
            photo56.FocalLength = "15 mm";
            photo56.Id = 56;
            photo56.ISO = "1500";
            photo56.Name = "MountainsSix";
            photo56.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/mountains-6.jpg";
            photo56.ShutterSpeed = "1/340";
            photo56.UserId = 7;

            var photo57 = new Photo();
            photo57.Aperture = "f/3.23";
            photo57.Camera = "Xiomi Redmi Note";
            photo57.Description = "A snowy mountain in Bulgaria.";
            photo57.FocalLength = "35 mm";
            photo57.Id = 57;
            photo57.ISO = "1500";
            photo57.Name = "MountainsSeven";
            photo57.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/mountains-7.jpg";
            photo57.ShutterSpeed = "1/340";
            photo57.UserId = 7;

            var photo58 = new Photo();
            photo58.Aperture = "f/3.23";
            photo58.Camera = "Huawei P20";
            photo58.Description = "A breath-taking view.";
            photo58.FocalLength = "45 mm";
            photo58.Id = 58;
            photo58.ISO = "1500";
            photo58.Name = "MountainsEight";
            photo58.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/mountains-8.jpg";
            photo58.ShutterSpeed = "1/340";
            photo58.UserId = 7;

            var photo59 = new Photo();
            photo59.Aperture = "f/3.23";
            photo59.Camera = "Huawei P20";
            photo59.Description = "Really nice lake with mountain.";
            photo59.FocalLength = "45 mm";
            photo59.Id = 59;
            photo59.ISO = "2500";
            photo59.Name = "MountainsNine";
            photo59.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/mountains-9.jpg";
            photo59.ShutterSpeed = "1/240";
            photo59.UserId = 8;

            var photo60 = new Photo();
            photo60.Aperture = "f/3.23";
            photo60.Camera = "Huawei P20";
            photo60.Description = "A town with lights and mountain.";
            photo60.FocalLength = "45 mm";
            photo60.Id = 60;
            photo60.ISO = "2500";
            photo60.Name = "MountainsTen";
            photo60.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/mountains-10.jpg";
            photo60.ShutterSpeed = "1/240";
            photo60.UserId = 8;

            var photo61 = new Photo();
            photo61.Aperture = "f/3.23";
            photo61.Camera = "Huawei P20 Pro";
            photo61.Description = "A man holding a laser sword.";
            photo61.FocalLength = "45 mm";
            photo61.Id = 61;
            photo61.ISO = "1500";
            photo61.Name = "SciOne";
            photo61.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sci-1.jpg";
            photo61.ShutterSpeed = "1/240";
            photo61.UserId = 8;

            var photo62 = new Photo();
            photo62.Aperture = "f/3.23";
            photo62.Camera = "Huawei P20 Pro";
            photo62.Description = "A outworld hand.";
            photo62.FocalLength = "25 mm";
            photo62.Id = 62;
            photo62.ISO = "1500";
            photo62.Name = "SciTwo";
            photo62.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sci-2.jpg";
            photo62.ShutterSpeed = "1/240";
            photo62.UserId = 8;

            var photo63 = new Photo();
            photo63.Aperture = "f/5.23";
            photo63.Camera = "Huawei P20 Pro";
            photo63.Description = "A UFO near mountains.";
            photo63.FocalLength = "25 mm";
            photo63.Id = 63;
            photo63.ISO = "1000";
            photo63.Name = "SciThree";
            photo63.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sci-3.jpg";
            photo63.ShutterSpeed = "1/240";
            photo63.UserId = 8;

            var photo64 = new Photo();
            photo64.Aperture = "f/5.23";
            photo64.Camera = "Huawei P10";
            photo64.Description = "Abstract image of a mind.";
            photo64.FocalLength = "25 mm";
            photo64.Id = 64;
            photo64.ISO = "1000";
            photo64.Name = "SciFour";
            photo64.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sci-4.jpg";
            photo64.ShutterSpeed = "1/240";
            photo64.UserId = 6;

            var photo65 = new Photo();
            photo65.Aperture = "f/5.23";
            photo65.Camera = "Huawei P10";
            photo65.Description = "Some other planet beings and Saturn in the back.";
            photo65.FocalLength = "25 mm";
            photo65.Id = 65;
            photo65.ISO = "1500";
            photo65.Name = "SciFive";
            photo65.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sci-5.jpg";
            photo65.ShutterSpeed = "1/250";
            photo65.UserId = 6;

            var photo66 = new Photo();
            photo66.Aperture = "f/5.23";
            photo66.Camera = "Huawei P10";
            photo66.Description = "Milky way in the background.";
            photo66.FocalLength = "15 mm";
            photo66.Id = 66;
            photo66.ISO = "1500";
            photo66.Name = "SciSix";
            photo66.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sci-6.jpg";
            photo66.ShutterSpeed = "1/250";
            photo66.UserId = 6;

            var photo67 = new Photo();
            photo67.Aperture = "f/5.23";
            photo67.Camera = "Huawei P10";
            photo67.Description = "Formation of galaxies are presented on this photo.";
            photo67.FocalLength = "15 mm";
            photo67.Id = 67;
            photo67.ISO = "1500";
            photo67.Name = "SciSeven";
            photo67.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sci-7.jpg";
            photo67.ShutterSpeed = "1/250";
            photo67.UserId = 6;

            var photo68 = new Photo();
            photo68.Aperture = "f/5.23";
            photo68.Camera = "Huawei P10";
            photo68.Description = "Otherwordly.";
            photo68.FocalLength = "25 mm";
            photo68.Id = 68;
            photo68.ISO = "1500";
            photo68.Name = "SciEight";
            photo68.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sci-8.jpg";
            photo68.ShutterSpeed = "1/250";
            photo68.UserId = 2;

            var photo69 = new Photo();
            photo69.Aperture = "f/6.9";
            photo69.Camera = "Huawei P10";
            photo69.Description = "An owl staring at your soul.";
            photo69.FocalLength = "25 mm";
            photo69.Id = 69;
            photo69.ISO = "1500";
            photo69.Name = "SciNine";
            photo69.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sci-9.jpg";
            photo69.ShutterSpeed = "1/250";
            photo69.UserId = 2;

            var photo70 = new Photo();
            photo70.Aperture = "f/1.9";
            photo70.Camera = "LG S15 Pro";
            photo70.Description = "A stream of light coming from the brightest star in the galaxy.";
            photo70.FocalLength = "15 mm";
            photo70.Id = 70;
            photo70.ISO = "1500";
            photo70.Name = "SciTen";
            photo70.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sci-10.jpg";
            photo70.ShutterSpeed = "1/250";
            photo70.UserId = 2;


            var photo71 = new Photo();
            photo71.Aperture = "f/2.4";
            photo71.Camera = "LG S15 Pro";
            photo71.Description = "This picture is taken near La Paz, Bolivia.";
            photo71.FocalLength = "30 mm";
            photo71.Id = 71;
            photo71.ISO = "1500";
            photo71.Name = "SkyOne";
            photo71.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sky-1.jpg";
            photo71.ShutterSpeed = "1/250";
            photo71.UserId = 5;


            var photo72 = new Photo();
            photo72.Aperture = "f/2.4";
            photo72.Camera = "LG S15 Pro";
            photo72.Description = "This beautiful sky is located south of Dhaka, Bangladesh.";
            photo72.FocalLength = "30 mm";
            photo72.Id = 72;
            photo72.ISO = "2500";
            photo72.Name = "SkyTwo";
            photo72.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sky-2.jpg";
            photo72.ShutterSpeed = "1/250";
            photo72.UserId = 5;

            var photo73 = new Photo();
            photo73.Aperture = "f/2.4";
            photo73.Camera = "LG S15 Pro";
            photo73.Description = "Location of this picture is Slovenia.";
            photo73.FocalLength = "30 mm";
            photo73.Id = 73;
            photo73.ISO = "2500";
            photo73.Name = "SkyThree";
            photo73.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sky-3.jpg";
            photo73.ShutterSpeed = "1/250";
            photo73.UserId = 5;

            var photo74 = new Photo();
            photo74.Aperture = "f/3.1";
            photo74.Camera = "LG S15 Max";
            photo74.Description = "Located in Argentina, in particular north of Buenos Aires.";
            photo74.FocalLength = "30 mm";
            photo74.Id = 74;
            photo74.ISO = "2500";
            photo74.Name = "SkyFour";
            photo74.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sky-4.jpg";
            photo74.ShutterSpeed = "1/220";
            photo74.UserId = 8;

            var photo75 = new Photo();
            photo75.Aperture = "f/3.1";
            photo75.Camera = "LG S15 Max";
            photo75.Description = "This photo is taken 50 kilometers west of Sarajevo.";
            photo75.FocalLength = "15 mm";
            photo75.Id = 75;
            photo75.ISO = "2500";
            photo75.Name = "SkyFive";
            photo75.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sky-5.jpg";
            photo75.ShutterSpeed = "1/220";
            photo75.UserId = 8;

            var photo76 = new Photo();
            photo76.Aperture = "f/3.1";
            photo76.Camera = "LG S15 Max";
            photo76.Description = "15 kilometers south of Milan.";
            photo76.FocalLength = "15 mm";
            photo76.Id = 76;
            photo76.ISO = "1500";
            photo76.Name = "SkySix";
            photo76.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sky-6.jpg";
            photo76.ShutterSpeed = "1/220";
            photo76.UserId = 8;

            var photo77 = new Photo();
            photo77.Aperture = "f/3.1";
            photo77.Camera = "LG S15 Max";
            photo77.Description = "Around 30 kilometers from Cape Town.";
            photo77.FocalLength = "15 mm";
            photo77.Id = 77;
            photo77.ISO = "1500";
            photo77.Name = "SkySeven";
            photo77.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sky-7.jpg";
            photo77.ShutterSpeed = "1/220";
            photo77.UserId = 8;

            var photo78 = new Photo();
            photo78.Aperture = "f/3.1";
            photo78.Camera = "LG S15 Max Pro";
            photo78.Description = "A beautiful pink sky.";
            photo78.FocalLength = "15 mm";
            photo78.Id = 78;
            photo78.ISO = "1500";
            photo78.Name = "SkyEight";
            photo78.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sky-8.jpg";
            photo78.ShutterSpeed = "1/220";
            photo78.UserId = 9;

            var photo79 = new Photo();
            photo79.Aperture = "f/3.8";
            photo79.Camera = "LG S15 Max Pro";
            photo79.Description = "50 kilometers north of Varna";
            photo79.FocalLength = "15 mm";
            photo79.Id = 79;
            photo79.ISO = "1500";
            photo79.Name = "SkyNine";
            photo79.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sky-9.jpg";
            photo79.ShutterSpeed = "1/260";
            photo79.UserId = 9;

            var photo80 = new Photo();
            photo80.Aperture = "f/3.8";
            photo80.Camera = "LG S15 Max Pro";
            photo80.Description = "50 kilometers north of Burgas";
            photo80.FocalLength = "15 mm";
            photo80.Id = 80;
            photo80.ISO = "1500";
            photo80.Name = "SkyTen";
            photo80.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/sky-10.jpg";
            photo80.ShutterSpeed = "1/260";
            photo80.UserId = 9;

            var photo81 = new Photo();
            photo81.Aperture = "f/3.8";
            photo81.Camera = "Lenovo P15";
            photo81.Description = "A computer and my favorite thing in the world: coffee.";
            photo81.FocalLength = "15 mm";
            photo81.Id = 81;
            photo81.ISO = "1500";
            photo81.Name = "TechOne";
            photo81.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/tech-1.jpg";
            photo81.ShutterSpeed = "1/260";
            photo81.UserId = 4;

            var photo82 = new Photo();
            photo82.Aperture = "f/3.8";
            photo82.Camera = "Lenovo P15";
            photo82.Description = "Windmills located near the border with Mexico.";
            photo82.FocalLength = "15 mm";
            photo82.Id = 82;
            photo82.ISO = "1500";
            photo82.Name = "TechTwo";
            photo82.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/tech-2.jpg";
            photo82.ShutterSpeed = "1/260";
            photo82.UserId = 4;

            var photo83 = new Photo();
            photo83.Aperture = "f/3.8";
            photo83.Camera = "Lenovo P15";
            photo83.Description = "A nice setup for gaming.";
            photo83.FocalLength = "25 mm";
            photo83.Id = 83;
            photo83.ISO = "2000";
            photo83.Name = "TechThree";
            photo83.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/tech-3.jpg";
            photo83.ShutterSpeed = "1/260";
            photo83.UserId = 4;

            var photo84 = new Photo();
            photo84.Aperture = "f/3.8";
            photo84.Camera = "Lenovo P15";
            photo84.Description = "Minimalistis setup for programming.";
            photo84.FocalLength = "35 mm";
            photo84.Id = 84;
            photo84.ISO = "2000";
            photo84.Name = "TechFour";
            photo84.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/tech-4.jpg";
            photo84.ShutterSpeed = "1/260";
            photo84.UserId = 4;

            var photo85 = new Photo();
            photo85.Aperture = "f/3.8";
            photo85.Camera = "Lenovo P15";
            photo85.Description = "Minimalistis setup.";
            photo85.FocalLength = "35 mm";
            photo85.Id = 85;
            photo85.ISO = "2000";
            photo85.Name = "TechFive";
            photo85.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/tech-5.jpg";
            photo85.ShutterSpeed = "1/210";
            photo85.UserId = 4;

            var photo86 = new Photo();
            photo86.Aperture = "f/3.8";
            photo86.Camera = "Lenovo P15";
            photo86.Description = "It all starts with a keyboard.";
            photo86.FocalLength = "40 mm";
            photo86.Id = 86;
            photo86.ISO = "2000";
            photo86.Name = "TechSix";
            photo86.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/tech-6.jpg";
            photo86.ShutterSpeed = "1/210";
            photo86.UserId = 3;

            var photo87 = new Photo();
            photo87.Aperture = "f/3.8";
            photo87.Camera = "Lenovo P15";
            photo87.Description = "Couple of cameras used by god knows who.";
            photo87.FocalLength = "40 mm";
            photo87.Id = 87;
            photo87.ISO = "2000";
            photo87.Name = "TechSeven";
            photo87.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/tech-7.jpg";
            photo87.ShutterSpeed = "1/210";
            photo87.UserId = 3;

            var photo88 = new Photo();
            photo88.Aperture = "f/4.1";
            photo88.Camera = "Lenovo P15 Pro";
            photo88.Description = "A software engineer doing her job.";
            photo88.FocalLength = "40 mm";
            photo88.Id = 88;
            photo88.ISO = "2000";
            photo88.Name = "TechEight";
            photo88.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/tech-8.jpg";
            photo88.ShutterSpeed = "1/210";
            photo88.UserId = 3;

            var photo89 = new Photo();
            photo89.Aperture = "f/4.1";
            photo89.Camera = "Lenovo P15 Pro";
            photo89.Description = "A code representation of chaos.";
            photo89.FocalLength = "40 mm";
            photo89.Id = 89;
            photo89.ISO = "2000";
            photo89.Name = "TechNine";
            photo89.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/tech-9.jpg";
            photo89.ShutterSpeed = "1/210";
            photo89.UserId = 3;

            var photo90 = new Photo();
            photo90.Aperture = "f/4.1";
            photo90.Camera = "Lenovo P15 Pro";
            photo90.Description = "Nice setup.";
            photo90.FocalLength = "40 mm";
            photo90.Id = 90;
            photo90.ISO = "1200";
            photo90.Name = "TechTen";
            photo90.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/tech-10.jpg";
            photo90.ShutterSpeed = "1/210";
            photo90.UserId = 3;


            var photo91 = new Photo();
            photo91.Aperture = "f/4.1";
            photo91.Camera = "Lenovo P15 Pro";
            photo91.Description = "A beautiful looking lane road.";
            photo91.FocalLength = "40 mm";
            photo91.Id = 91;
            photo91.ISO = "1200";
            photo91.Name = "UrbanOne";
            photo91.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/urban-1.jpg";
            photo91.ShutterSpeed = "1/210";
            photo91.UserId = 3;

            var photo92 = new Photo();
            photo92.Aperture = "f/4.1";
            photo92.Camera = "Iphone 10 Pro";
            photo92.Description = "Beautiful urban shot.";
            photo92.FocalLength = "40 mm";
            photo92.Id = 92;
            photo92.ISO = "1200";
            photo92.Name = "UrbanTwo";
            photo92.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/urban-2.jpg";
            photo92.ShutterSpeed = "1/210";
            photo92.UserId = 3;

            var photo93 = new Photo();
            photo93.Aperture = "f/4.1";
            photo93.Camera = "Iphone 10 Pro";
            photo93.Description = "Graffitti shot in New York.";
            photo93.FocalLength = "30 mm";
            photo93.Id = 93;
            photo93.ISO = "1200";
            photo93.Name = "UrbanThree";
            photo93.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/urban-3.jpg";
            photo93.ShutterSpeed = "1/210";
            photo93.UserId = 3;

            var photo94 = new Photo();
            photo94.Aperture = "f/4.1";
            photo94.Camera = "Iphone 10 Pro";
            photo94.Description = "City view.";
            photo94.FocalLength = "30 mm";
            photo94.Id = 94;
            photo94.ISO = "1200";
            photo94.Name = "UrbanFour";
            photo94.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/urban-4.jpg";
            photo94.ShutterSpeed = "1/210";
            photo94.UserId = 3;

            var photo95 = new Photo();
            photo95.Aperture = "f/4.1";
            photo95.Camera = "Iphone 10 Pro Max";
            photo95.Description = "Japan's city view.";
            photo95.FocalLength = "30 mm";
            photo95.Id = 95;
            photo95.ISO = "1200";
            photo95.Name = "UrbanFive";
            photo95.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/urban-5.jpg";
            photo95.ShutterSpeed = "1/210";
            photo95.UserId = 3;

            var photo96 = new Photo();
            photo96.Aperture = "f/4.1";
            photo96.Camera = "Iphone 10 Pro";
            photo96.Description = "Underground.";
            photo96.FocalLength = "30 mm";
            photo96.Id = 96;
            photo96.ISO = "1400";
            photo96.Name = "UrbanSix";
            photo96.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/urban-6.jpg";
            photo96.ShutterSpeed = "1/210";
            photo96.UserId = 3;

            var photo97 = new Photo();
            photo97.Aperture = "f/4.1";
            photo97.Camera = "Iphone 10 Pro";
            photo97.Description = "Couple of doors.";
            photo97.FocalLength = "30 mm";
            photo97.Id = 97;
            photo97.ISO = "1400";
            photo97.Name = "UrbanSeven";
            photo97.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/urban-7.jpg";
            photo97.ShutterSpeed = "1/210";
            photo97.UserId = 3;

            var photo98 = new Photo();
            photo98.Aperture = "f/4.1";
            photo98.Camera = "Iphone 10 Pro";
            photo98.Description = "Graffitti in Seattle.";
            photo98.FocalLength = "30 mm";
            photo98.Id = 98;
            photo98.ISO = "1400";
            photo98.Name = "UrbanEight";
            photo98.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/urban-8.jpg";
            photo98.ShutterSpeed = "1/210";
            photo98.UserId = 3;

            var photo99 = new Photo();
            photo99.Aperture = "f/4.1";
            photo99.Camera = "Iphone 10 Pro Max";
            photo99.Description = "Skyscreapers in the clouds.";
            photo99.FocalLength = "30 mm";
            photo99.Id = 99;
            photo99.ISO = "1400";
            photo99.Name = "UrbanNine";
            photo99.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/urban-9.jpg";
            photo99.ShutterSpeed = "1/210";
            photo99.UserId = 3;

            var photo100 = new Photo();
            photo100.Aperture = "f/4.1";
            photo100.Camera = "Iphone 10 Pro Max";
            photo100.Description = "Two trains in New York.";
            photo100.FocalLength = "30 mm";
            photo100.Id = 100;
            photo100.ISO = "1400";
            photo100.Name = "UrbanTen";
            photo100.PhotoLink = "https://photocontest.blob.core.windows.net/youtubedemo/urban-10.jpg";
            photo100.ShutterSpeed = "1/210";
            photo100.UserId = 3;



            var photos = new List<Photo>()
            {
                photo,photo2,photo3,photo4,photo5,photo6,
                photo7,photo8,photo9,photo10,photo11,photo12,photo13,photo14,photo15,
                photo16,photo17,photo18,photo19,photo20,photo21,photo22,photo23,photo24,photo25,
                photo26,photo27,photo28,photo29,photo30,photo31,photo32,photo33,
                photo34,photo35,photo36,photo37,photo38,photo39,photo40,photo41,
                photo42,photo43,photo44,photo45,photo46,photo47,photo48,photo49,photo50,
                photo51,photo52,photo53,photo54,photo55,photo56,photo57,photo58,photo59,photo60,
                photo61,photo62,photo63,photo64,photo65,photo66,photo67,photo68,photo69,photo70,
                photo71,photo72,photo73,photo74,photo75,photo76,photo77,photo78,photo79,photo80,
                photo81,photo82,photo83,photo84,photo85,photo86,photo87,photo88,photo89,photo90,
                photo91,photo92,photo93,photo94,photo95,photo96,photo97,photo98,photo99,photo100

            };

            modelBuilder.Entity<Photo>().HasData(photos);


            var contestPhoto = new PhotoScoreContest() { PhotoId = 1, ContestId = 1, Score = 60 };
            var contestPhoto2 = new PhotoScoreContest() { PhotoId = 2, ContestId = 1, Score = 50 };
            var contestPhoto3 = new PhotoScoreContest() { PhotoId = 3, ContestId = 1, Score = 50 };
            var contestPhoto4 = new PhotoScoreContest() { PhotoId = 4, ContestId = 1, Score = 50 };
            var contestPhoto5 = new PhotoScoreContest() { PhotoId = 5, ContestId = 1, Score = 50 };
            var contestPhoto6 = new PhotoScoreContest() { PhotoId = 6, ContestId = 1, Score = 50 };
            var contestPhoto7 = new PhotoScoreContest() { PhotoId = 7, ContestId = 1, Score = 51 };
            var contestPhoto8 = new PhotoScoreContest() { PhotoId = 8, ContestId = 1, Score = 26 };
            var contestPhotoa = new PhotoScoreContest() { PhotoId = 9, ContestId = 1, Score = 15 };
            var contestPhoto9 = new PhotoScoreContest() { PhotoId = 10, ContestId = 1, Score = 50 };

            var contestPhoto10 = new PhotoScoreContest() { PhotoId = 11, ContestId = 2 };
            var contestPhoto11 = new PhotoScoreContest() { PhotoId = 12, ContestId = 2 };
            var contestPhoto12 = new PhotoScoreContest() { PhotoId = 13, ContestId = 2 };
            var contestPhoto13 = new PhotoScoreContest() { PhotoId = 14, ContestId = 2 };
            var contestPhoto14 = new PhotoScoreContest() { PhotoId = 15, ContestId = 2 };
            var contestPhoto15 = new PhotoScoreContest() { PhotoId = 16, ContestId = 2 };
            var contestPhoto16 = new PhotoScoreContest() { PhotoId = 17, ContestId = 2 };
            var contestPhoto17 = new PhotoScoreContest() { PhotoId = 18, ContestId = 2 };
            var contestPhoto18 = new PhotoScoreContest() { PhotoId = 19, ContestId = 2 };
            var contestPhoto19 = new PhotoScoreContest() { PhotoId = 20, ContestId = 2 };

            var contestPhoto20 = new PhotoScoreContest() { PhotoId = 21, ContestId = 3 };
            var contestPhoto21 = new PhotoScoreContest() { PhotoId = 22, ContestId = 3 };
            var contestPhoto22 = new PhotoScoreContest() { PhotoId = 23, ContestId = 3 };
            var contestPhoto23 = new PhotoScoreContest() { PhotoId = 24, ContestId = 3 };
            var contestPhoto24 = new PhotoScoreContest() { PhotoId = 25, ContestId = 3 };
            var contestPhoto25 = new PhotoScoreContest() { PhotoId = 26, ContestId = 3 };
            var contestPhoto26 = new PhotoScoreContest() { PhotoId = 27, ContestId = 3 };
            var contestPhoto27 = new PhotoScoreContest() { PhotoId = 28, ContestId = 3 };
            var contestPhoto28 = new PhotoScoreContest() { PhotoId = 29, ContestId = 3 };
            var contestPhoto29 = new PhotoScoreContest() { PhotoId = 30, ContestId = 3 };

            var contestPhoto30 = new PhotoScoreContest() { PhotoId = 31, ContestId = 4 };
            var contestPhoto31 = new PhotoScoreContest() { PhotoId = 32, ContestId = 4 };
            var contestPhoto32 = new PhotoScoreContest() { PhotoId = 33, ContestId = 4 };
            var contestPhoto33 = new PhotoScoreContest() { PhotoId = 34, ContestId = 4 };
            var contestPhoto34 = new PhotoScoreContest() { PhotoId = 35, ContestId = 4 };
            var contestPhoto35 = new PhotoScoreContest() { PhotoId = 36, ContestId = 4 };
            var contestPhoto36 = new PhotoScoreContest() { PhotoId = 37, ContestId = 4 };
            var contestPhoto37 = new PhotoScoreContest() { PhotoId = 38, ContestId = 4 };
            var contestPhoto38 = new PhotoScoreContest() { PhotoId = 39, ContestId = 4 };
            var contestPhoto39 = new PhotoScoreContest() { PhotoId = 40, ContestId = 4 };

            var contestPhoto40 = new PhotoScoreContest() { PhotoId = 41, ContestId = 5 };
            var contestPhoto41 = new PhotoScoreContest() { PhotoId = 42, ContestId = 5 };
            var contestPhoto42 = new PhotoScoreContest() { PhotoId = 43, ContestId = 5 };
            var contestPhoto43 = new PhotoScoreContest() { PhotoId = 44, ContestId = 5 };
            var contestPhoto44 = new PhotoScoreContest() { PhotoId = 45, ContestId = 5 };
            var contestPhoto45 = new PhotoScoreContest() { PhotoId = 46, ContestId = 5 };
            var contestPhoto46 = new PhotoScoreContest() { PhotoId = 47, ContestId = 5 };
            var contestPhoto47 = new PhotoScoreContest() { PhotoId = 48, ContestId = 5 };
            var contestPhoto48 = new PhotoScoreContest() { PhotoId = 49, ContestId = 5 };
            var contestPhoto49 = new PhotoScoreContest() { PhotoId = 50, ContestId = 5 };

            var contestPhoto50 = new PhotoScoreContest() { PhotoId = 51, ContestId = 6 };
            var contestPhoto51 = new PhotoScoreContest() { PhotoId = 52, ContestId = 6 };
            var contestPhoto52 = new PhotoScoreContest() { PhotoId = 53, ContestId = 6 };
            var contestPhoto53 = new PhotoScoreContest() { PhotoId = 54, ContestId = 6 };
            var contestPhoto54 = new PhotoScoreContest() { PhotoId = 55, ContestId = 6 };
            var contestPhoto55 = new PhotoScoreContest() { PhotoId = 56, ContestId = 6 };
            var contestPhoto56 = new PhotoScoreContest() { PhotoId = 57, ContestId = 6 };
            var contestPhoto57 = new PhotoScoreContest() { PhotoId = 58, ContestId = 6 };
            var contestPhoto58 = new PhotoScoreContest() { PhotoId = 59, ContestId = 6 };
            var contestPhoto59 = new PhotoScoreContest() { PhotoId = 60, ContestId = 6 };


            var contestPhoto60 = new PhotoScoreContest() { PhotoId = 61, ContestId = 7 };
            var contestPhoto61 = new PhotoScoreContest() { PhotoId = 62, ContestId = 7 };
            var contestPhoto62 = new PhotoScoreContest() { PhotoId = 63, ContestId = 7 };
            var contestPhoto63 = new PhotoScoreContest() { PhotoId = 64, ContestId = 7 };
            var contestPhoto64 = new PhotoScoreContest() { PhotoId = 65, ContestId = 7 };
            var contestPhoto65 = new PhotoScoreContest() { PhotoId = 66, ContestId = 7 };
            var contestPhoto66 = new PhotoScoreContest() { PhotoId = 67, ContestId = 7 };
            var contestPhoto67 = new PhotoScoreContest() { PhotoId = 68, ContestId = 7 };
            var contestPhoto68 = new PhotoScoreContest() { PhotoId = 69, ContestId = 7 };
            var contestPhoto69 = new PhotoScoreContest() { PhotoId = 70, ContestId = 7 };

            var contestPhoto70 = new PhotoScoreContest() { PhotoId = 71, ContestId = 8 };
            var contestPhoto71 = new PhotoScoreContest() { PhotoId = 72, ContestId = 8 };
            var contestPhoto72 = new PhotoScoreContest() { PhotoId = 73, ContestId = 8 };
            var contestPhoto73 = new PhotoScoreContest() { PhotoId = 74, ContestId = 8 };
            var contestPhoto74 = new PhotoScoreContest() { PhotoId = 75, ContestId = 8 };
            var contestPhoto75 = new PhotoScoreContest() { PhotoId = 76, ContestId = 8 };
            var contestPhoto76 = new PhotoScoreContest() { PhotoId = 77, ContestId = 8 };
            var contestPhoto77 = new PhotoScoreContest() { PhotoId = 78, ContestId = 8 };
            var contestPhoto78 = new PhotoScoreContest() { PhotoId = 79, ContestId = 8 };
            var contestPhoto79 = new PhotoScoreContest() { PhotoId = 80, ContestId = 8 };

            var contestPhoto80 = new PhotoScoreContest() { PhotoId = 81, ContestId = 9 };
            var contestPhoto81 = new PhotoScoreContest() { PhotoId = 82, ContestId = 9 };
            var contestPhoto82 = new PhotoScoreContest() { PhotoId = 83, ContestId = 9 };
            var contestPhoto83 = new PhotoScoreContest() { PhotoId = 84, ContestId = 9 };
            var contestPhoto84 = new PhotoScoreContest() { PhotoId = 85, ContestId = 9 };
            var contestPhoto85 = new PhotoScoreContest() { PhotoId = 86, ContestId = 9 };
            var contestPhoto86 = new PhotoScoreContest() { PhotoId = 87, ContestId = 9 };
            var contestPhoto87 = new PhotoScoreContest() { PhotoId = 88, ContestId = 9 };
            var contestPhoto88 = new PhotoScoreContest() { PhotoId = 89, ContestId = 9 };
            var contestPhoto89 = new PhotoScoreContest() { PhotoId = 90, ContestId = 9 };

            var contestPhoto90 = new PhotoScoreContest() { PhotoId = 91, ContestId = 10 };
            var contestPhoto91 = new PhotoScoreContest() { PhotoId = 92, ContestId = 10 };
            var contestPhoto92 = new PhotoScoreContest() { PhotoId = 93, ContestId = 10 };
            var contestPhoto93 = new PhotoScoreContest() { PhotoId = 94, ContestId = 10 };
            var contestPhoto94 = new PhotoScoreContest() { PhotoId = 95, ContestId = 10 };
            var contestPhoto95 = new PhotoScoreContest() { PhotoId = 96, ContestId = 10 };
            var contestPhoto96 = new PhotoScoreContest() { PhotoId = 97, ContestId = 10 };
            var contestPhoto97 = new PhotoScoreContest() { PhotoId = 98, ContestId = 10 };
            var contestPhoto98 = new PhotoScoreContest() { PhotoId = 99, ContestId = 10 };
            var contestPhoto99 = new PhotoScoreContest() { PhotoId = 100, ContestId = 10 };










            var contestPhotoList = new List<PhotoScoreContest>()
            {
                contestPhoto,contestPhoto2,contestPhoto3,contestPhoto4,contestPhoto5,contestPhoto6
                                ,contestPhoto7,contestPhoto8,contestPhoto9,contestPhoto10,contestPhoto11,
                contestPhoto12,contestPhoto13,contestPhoto14,contestPhoto15,contestPhoto16,contestPhoto17,
                contestPhoto18,contestPhoto19,contestPhoto20,contestPhoto21,contestPhoto22,contestPhoto23,
                contestPhoto24,contestPhoto25,contestPhoto26,contestPhoto27,contestPhoto28,contestPhoto29,
                contestPhoto30,contestPhoto31,contestPhoto32,contestPhoto33,contestPhoto34,contestPhoto35,
                contestPhoto36,contestPhoto37,contestPhoto38,contestPhoto39,contestPhoto40,contestPhoto41,
                contestPhoto42,contestPhoto43,contestPhoto44,contestPhoto45,contestPhoto46,contestPhoto47,
                contestPhoto48,contestPhoto49,contestPhoto50,contestPhoto51,contestPhotoa,contestPhoto52,
                contestPhoto53,contestPhoto54,contestPhoto55,contestPhoto56,contestPhoto57,contestPhoto58,
                contestPhoto59,contestPhoto60,contestPhoto61,contestPhoto62,contestPhoto63,
                contestPhoto64,contestPhoto65,contestPhoto66,contestPhoto67,contestPhoto68,contestPhoto69,
                contestPhoto70,contestPhoto71,contestPhoto72,contestPhoto73,contestPhoto74,contestPhoto75,
                contestPhoto76,contestPhoto77,contestPhoto78,contestPhoto79,contestPhoto80,contestPhoto81,
                contestPhoto82,contestPhoto83,contestPhoto84,contestPhoto85,contestPhoto86,contestPhoto87,
                contestPhoto88,contestPhoto89,contestPhoto90,contestPhoto91,contestPhoto92,contestPhoto93,
                contestPhoto94,contestPhoto95,contestPhoto96,contestPhoto97,contestPhoto98,contestPhoto99
            };

            modelBuilder.Entity<PhotoScoreContest>().HasData(contestPhotoList);



            var contestUser = new ContestUser() { ContestId = 1, UserId = 2 };
            var contestUser2 = new ContestUser() { ContestId = 2, UserId = 2 };
            var contestUser3 = new ContestUser() { ContestId = 3, UserId = 2 };
            var contestUser4 = new ContestUser() { ContestId = 4, UserId = 2 };
            var contestUser5 = new ContestUser() { ContestId = 5, UserId = 2 };
            var contestUser6 = new ContestUser() { ContestId = 6, UserId = 2 };
            var contestUser7 = new ContestUser() { ContestId = 1, UserId = 4, Rated = true };
            var contestUser8 = new ContestUser() { ContestId = 2, UserId = 6, Rated = true };
            var contestUser9 = new ContestUser() { ContestId = 7, UserId = 7 };
            var contestUser10 = new ContestUser() { ContestId = 8, UserId = 8 };
            var contestUser11 = new ContestUser() { ContestId = 9, UserId = 9 };
            var contestUser12 = new ContestUser() { ContestId = 2, UserId = 7 };





            var contestUserList = new List<ContestUser>()
            {
                contestUser,contestUser2,contestUser3,contestUser4,contestUser5,contestUser6,contestUser7,contestUser8,
                contestUser9,contestUser10,contestUser11,contestUser12
            };

            modelBuilder.Entity<ContestUser>().HasData(contestUserList);



            var rank = new Rank() { Id = 1, RankName = "Junkie" };
            var rank2 = new Rank() { Id = 2, RankName = "Enthusiast" };
            var rank3 = new Rank() { Id = 3, RankName = "Master" };
            var rank4 = new Rank() { Id = 4, RankName = "Dictator" };

            var rankList = new List<Rank>()
            {
                rank,rank2,rank3,rank4
            };

            modelBuilder.Entity<Rank>().HasData(rankList);







            var comment = new Comment();
            comment.Id = 1;
            comment.CommentTime = DateTime.Now.AddDays(1);
            comment.Description = "Perfect contrast achieved through finding the best direction of the light!";
            comment.PhotoId = 1;
            comment.UserId = 4;

            var comment2 = new Comment();
            comment2.Id = 2;
            comment2.CommentTime = DateTime.Now.AddDays(5);
            comment2.Description = "Perfectly framed architecture!";
            comment2.PhotoId = 2;
            comment2.UserId = 2;

            var comment3 = new Comment();
            comment3.Id = 3;
            comment3.CommentTime = DateTime.Now.AddDays(2);
            comment3.Description = "Beautiful architecture silhouette – perfectly timed photo resulting to the perfect colors and perfect effects!";
            comment3.PhotoId = 3;
            comment3.UserId = 2;

            var comment4 = new Comment();
            comment4.Id = 4;
            comment4.CommentTime = DateTime.Now.AddDays(2);
            comment4.Description = "The perfect foreground created a totally enigmatic image!";
            comment4.PhotoId = 3;
            comment4.UserId = 5;

            var comment5 = new Comment();
            comment5.Id = 5;
            comment5.CommentTime = DateTime.Now.AddDays(2);
            comment5.Description = "The reflections added a whole new eye-catching dimension to the photo!";
            comment5.PhotoId = 4;
            comment5.UserId = 7;

            var comment6 = new Comment();
            comment6.Id = 6;
            comment6.CommentTime = DateTime.Now.AddDays(2);
            comment6.Description = "Great capture of the architecture’s intricate details!";
            comment6.PhotoId = 4;
            comment6.UserId = 5;

            var comment7 = new Comment();
            comment7.Id = 7;
            comment7.CommentTime = DateTime.Now.AddDays(12);
            comment7.Description = "An interesting play of perspective!";
            comment7.PhotoId = 5;
            comment7.UserId = 3;

            var comment8 = new Comment();
            comment8.Id = 8;
            comment8.CommentTime = DateTime.Now.AddDays(2);
            comment8.Description = "A brilliant mix of exceptional architecture and everyday objects – the people, the trees, and the busy streets!";
            comment8.PhotoId = 6;
            comment8.UserId = 2;

            var comment9 = new Comment();
            comment9.Id = 9;
            comment9.CommentTime = DateTime.Now.AddDays(2);
            comment9.Description = "I'd like this photo to be on my screen";
            comment9.PhotoId = 7;
            comment9.UserId = 2;

            var comment10 = new Comment();
            comment10.Id = 10;
            comment10.CommentTime = DateTime.Now.AddDays(2);
            comment10.Description = "It looks like I'm there";
            comment10.PhotoId = 8;
            comment10.UserId = 2;

            var comment11 = new Comment();
            comment11.Id = 11;
            comment11.CommentTime = DateTime.Now.AddDays(8);
            comment11.Description = "I can feel the photo";
            comment11.PhotoId = 9;
            comment11.UserId = 2;

            var comment12 = new Comment();
            comment12.Id = 12;
            comment12.CommentTime = DateTime.Now.AddDays(8);
            comment12.Description = "Smart incorporation of motions into the picture!";
            comment12.PhotoId = 10;
            comment12.UserId = 2;

            var comment13 = new Comment();
            comment13.Id = 13;
            comment13.CommentTime = DateTime.Now.AddDays(82);
            comment13.Description = "Amazing!";
            comment13.PhotoId = 11;
            comment13.UserId = 5;

            var comment14 = new Comment();
            comment14.Id = 14;
            comment14.CommentTime = DateTime.Now.AddDays(2);
            comment14.Description = "Gorgeous!";
            comment14.PhotoId = 12;
            comment14.UserId = 6;

            var comment15 = new Comment();
            comment15.Id = 15;
            comment15.CommentTime = DateTime.Now.AddDays(8);
            comment15.Description = "Such a pretty photo.";
            comment15.PhotoId = 13;
            comment15.UserId = 2;

            var comment16 = new Comment();
            comment16.Id = 16;
            comment16.CommentTime = DateTime.Now.AddDays(82);
            comment16.Description = "I really like the angle.";
            comment16.PhotoId = 14;
            comment16.UserId = 4;

            var comment17 = new Comment();
            comment17.Id = 17;
            comment17.CommentTime = DateTime.Now.AddDays(2);
            comment17.Description = "Really good.";
            comment17.PhotoId = 15;
            comment17.UserId = 2;

            var comment18 = new Comment();
            comment18.Id = 18;
            comment18.CommentTime = DateTime.Now.AddDays(8);
            comment18.Description = "I have the same camera but cannot shoot a photo like this.";
            comment18.PhotoId = 16;
            comment18.UserId = 2;

            var comment19 = new Comment();
            comment19.Id = 19;
            comment19.CommentTime = DateTime.Now.AddDays(2);
            comment19.Description = "The contrast is amazing.";
            comment19.PhotoId = 17;
            comment19.UserId = 2;

            var comment20 = new Comment();
            comment20.Id = 20;
            comment20.CommentTime = DateTime.Now.AddDays(2);
            comment20.Description = "I really like the light in the photo";
            comment20.PhotoId = 18;
            comment20.UserId = 2;

            var comment21 = new Comment();
            comment21.Id = 21;
            comment21.CommentTime = DateTime.Now.AddDays(8);
            comment21.Description = "I want it to a wallpaper";
            comment21.PhotoId = 19;
            comment21.UserId = 3;

            var comment22 = new Comment();
            comment22.Id = 22;
            comment22.CommentTime = DateTime.Now.AddDays(82);
            comment22.Description = "Really good.";
            comment22.PhotoId = 20;
            comment22.UserId = 2;

            var comment23 = new Comment();
            comment23.Id = 23;
            comment23.CommentTime = DateTime.Now.AddDays(2);
            comment23.Description = "The photo makes the viewer wish that he/she was there.";
            comment23.PhotoId = 21;
            comment23.UserId = 2;

            var comment24 = new Comment();
            comment24.Id = 24;
            comment24.CommentTime = DateTime.Now.AddDays(8);
            comment24.Description = "The colourful contrast between the dark sky and city lights is definitely captivating!";
            comment24.PhotoId = 22;
            comment24.UserId = 2;

            var comment25 = new Comment();
            comment25.Id = 25;
            comment25.CommentTime = DateTime.Now.AddDays(82);
            comment25.Description = "Smart capture of the skyline – no part was wasted.";
            comment25.PhotoId = 23;
            comment25.UserId = 2;

            var comment26 = new Comment();
            comment26.Id = 26;
            comment26.CommentTime = DateTime.Now.AddDays(2);
            comment26.Description = "Crisp and sharp image of the city!";
            comment26.PhotoId = 24;
            comment26.UserId = 3;

            var comment27 = new Comment();
            comment27.Id = 27;
            comment27.CommentTime = DateTime.Now.AddDays(8);
            comment27.Description = "The leading lines made for an intriguing feel!";
            comment27.PhotoId = 25;
            comment27.UserId = 2;

            var comment28 = new Comment();
            comment28.Id = 28;
            comment28.CommentTime = DateTime.Now.AddDays(88);
            comment28.Description = "The clean lines saved the photo from being a chaotic jumble of elements!";
            comment28.PhotoId = 26;
            comment28.UserId = 2;

            var comment29 = new Comment();
            comment29.Id = 29;
            comment29.CommentTime = DateTime.Now.AddDays(2);
            comment29.Description = "The clean lines saved the photo from being a chaotic jumble of elements!";
            comment29.PhotoId = 27;
            comment29.UserId = 2;

            var comment30 = new Comment();
            comment30.Id = 30;
            comment30.CommentTime = DateTime.Now.AddDays(8);
            comment30.Description = "The patterns in the photo created a lovely harmony!";
            comment30.PhotoId = 28;
            comment30.UserId = 2;

            var comment31 = new Comment();
            comment31.Id = 31;
            comment31.CommentTime = DateTime.Now.AddDays(8);
            comment31.Description = "Compelling photo of the city! ";
            comment31.PhotoId = 28;
            comment31.UserId = 2;

            var comment32 = new Comment();
            comment32.Id = 32;
            comment32.CommentTime = DateTime.Now.AddDays(8);
            comment32.Description = "Amazing, I have never seen a photo like this.";
            comment32.PhotoId = 28;
            comment32.UserId = 5;

            var comment33 = new Comment();
            comment33.Id = 33;
            comment33.CommentTime = DateTime.Now.AddDays(8);
            comment33.Description = "Amazing.";
            comment33.PhotoId = 29;
            comment33.UserId = 5;

            var comment34 = new Comment();
            comment34.Id = 34;
            comment34.CommentTime = DateTime.Now.AddDays(8);
            comment34.Description = "Oh! Very beautiful, I'm falling in love with this image.";
            comment34.PhotoId = 31;
            comment34.UserId = 5;

            var comment35 = new Comment();
            comment35.Id = 35;
            comment35.CommentTime = DateTime.Now.AddDays(8);
            comment35.Description = "Oh! Very beautiful.";
            comment35.PhotoId = 33;
            comment35.UserId = 5;

            var comment36 = new Comment();
            comment36.Id = 36;
            comment36.CommentTime = DateTime.Now.AddDays(8);
            comment36.Description = "Oh! Very beautiful.";
            comment36.PhotoId = 33;
            comment36.UserId = 7;

            var comment37 = new Comment();
            comment37.Id = 37;
            comment37.CommentTime = DateTime.Now.AddDays(8);
            comment37.Description = "Impressive picture.";
            comment37.PhotoId = 35;
            comment37.UserId = 7;

            var comment38 = new Comment();
            comment38.Id = 38;
            comment38.CommentTime = DateTime.Now.AddDays(8);
            comment38.Description = "Impressive picture.";
            comment38.PhotoId = 37;
            comment38.UserId = 5;

            var comment39 = new Comment();
            comment39.Id = 39;
            comment39.CommentTime = DateTime.Now.AddDays(8);
            comment39.Description = "Lighting is incredible.";
            comment39.PhotoId = 39;
            comment39.UserId = 5;

            var comment40 = new Comment();
            comment40.Id = 40;
            comment40.CommentTime = DateTime.Now.AddDays(8);
            comment40.Description = "Looking gorgeous.";
            comment40.PhotoId = 41;
            comment40.UserId = 5;

            var comment41 = new Comment();
            comment41.Id = 41;
            comment41.CommentTime = DateTime.Now.AddDays(8);
            comment41.Description = "Beautiful colors.";
            comment41.PhotoId = 41;
            comment41.UserId = 5;

            var comment42 = new Comment();
            comment42.Id = 42;
            comment42.CommentTime = DateTime.Now.AddDays(8);
            comment42.Description = "Beautiful colors.";
            comment42.PhotoId = 41;
            comment42.UserId = 6;

            var comment43 = new Comment();
            comment43.Id = 43;
            comment43.CommentTime = DateTime.Now.AddDays(8);
            comment43.Description = "This picture is eloquent like words.";
            comment43.PhotoId = 45;
            comment43.UserId = 6;

            var comment44 = new Comment();
            comment44.Id = 44;
            comment44.CommentTime = DateTime.Now.AddDays(8);
            comment44.Description = "Impressive.";
            comment44.PhotoId = 48;
            comment44.UserId = 6;

            var comment45 = new Comment();
            comment45.Id = 45;
            comment45.CommentTime = DateTime.Now.AddDays(8);
            comment45.Description = "Such a scenic view.";
            comment45.PhotoId = 48;
            comment45.UserId = 3;

            var comment46 = new Comment();
            comment46.Id = 46;
            comment46.CommentTime = DateTime.Now.AddDays(8);
            comment46.Description = "Vibrant colors.";
            comment46.PhotoId = 50;
            comment46.UserId = 3;

            var comment47 = new Comment();
            comment47.Id = 47;
            comment47.CommentTime = DateTime.Now.AddDays(8);
            comment47.Description = "Vibrant colors.";
            comment47.PhotoId = 52;
            comment47.UserId = 3;

            var comment48 = new Comment();
            comment48.Id = 48;
            comment48.CommentTime = DateTime.Now.AddDays(8);
            comment48.Description = "Nice colors.";
            comment48.PhotoId = 53;
            comment48.UserId = 3;

            var comment49 = new Comment();
            comment49.Id = 49;
            comment49.CommentTime = DateTime.Now.AddDays(8);
            comment49.Description = "Impressive.";
            comment49.PhotoId = 53;
            comment49.UserId = 3;

            var comment50 = new Comment();
            comment50.Id = 50;
            comment50.CommentTime = DateTime.Now.AddDays(8);
            comment50.Description = "Impressive.";
            comment50.PhotoId = 55;
            comment50.UserId = 3;



            var comments = new List<Comment>() { comment, comment2, comment3, comment4,
                comment5, comment6 , comment7, comment8, comment9, comment10, comment11,
                comment12, comment13, comment14, comment15, comment16, comment17, comment18,
                comment19, comment20, comment21, comment22, comment23, comment24, comment25,
                comment26, comment27, comment28, comment29, comment30, comment31, comment32,
                comment33, comment34, comment35, comment36, comment37, comment38, comment39,
                comment40, comment41, comment42, comment43, comment44, comment45, comment46,
                comment47, comment48, comment49, comment50 };

            modelBuilder.Entity<Comment>().HasData(comments);

        }
    }
}
