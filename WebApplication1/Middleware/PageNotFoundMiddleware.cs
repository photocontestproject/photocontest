﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Middleware
{
    public class PageNotFoundMiddleware
    {
        private readonly RequestDelegate _next;

        public PageNotFoundMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        //public async Task Invoke(HttpContext context)
        //{
        //    await this._next.Invoke(context);
        //    if (context.Response.StatusCode == 404 || context.Response.StatusCode == 403 || context.Response.StatusCode == 402 || context.Response.StatusCode == 401 || context.Response.StatusCode == 400)
        //    {
        //        context.Response.Redirect("Home/PageNotFound");
        //    }
        //}

        public async Task Invoke(HttpContext httpContext)
        {
            await this._next.Invoke(httpContext);

            switch (httpContext.Response.StatusCode)
            {
                case 404:
                    httpContext.Response.Redirect("/Home/Privacy");
                    break;

                case 500:
                    httpContext.Response.Redirect("/Home/Privacy");
                    break;
            }
        }



    }
}
