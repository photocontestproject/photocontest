﻿using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class PhotoModel
    {
        [Required (ErrorMessage = "The Photo Name is Required!")]
        [StringLength(maximumLength: 20, MinimumLength = 3, ErrorMessage = "The Photo Name should be between 3 and 220 characters long")]
        public string Name { get; set; }

        [Required(ErrorMessage = "The Description is Required!")]
        [StringLength(maximumLength: 40, MinimumLength = 3, ErrorMessage = "Description should be between 3 and 40 characters long")]
        public string Description { get; set; }

        public string PhotoLink { get; set; }

        public string UserName { get; set; }

        public string UserPhoto { get; set; }

        public string Camera { get; set; }

        public string ShutterSpeed { get; set; }

        public string Aperture { get; set; }

        public string FocalLength { get; set; }

        public string ISO { get; set; }

        public int PhotoScore { get; set; }

        public string UserRank { get; set; }

        public List<CommentsModel> Comments { get; set; }
    }
}
