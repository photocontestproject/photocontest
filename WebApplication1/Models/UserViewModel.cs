﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class UserViewModel
    {
        public string ProfileLink { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public string Rank { get; set; }

        public string Description { get; set; }

        public List<ContestCardModel> Contests { get; set; }
        public List<PhotoModel> Photos { get; set; }


    }
}
