﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models.CardsModels
{
    public class UserCardModel
    {
        public string ProfileLink { get; set; }
       
        public string UserName { get; set; }

        public string Rank { get; set; }

        public string Description { get; set; }
    }
}
