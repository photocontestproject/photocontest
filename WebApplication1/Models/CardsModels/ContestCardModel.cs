﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class ContestCardModel
    {
        public string Name { get; set; }

        public string AdminName { get; set; }

        public string Profile { get; set; }

        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int ParticipantLimit { get; set; }

        public int  Phase  { get; set; } 

        public string Joinned { get; set; }

        public bool Rated { get; set; }

        public int PhotosCount { get; set; }

        public string Category { get; set; }


    }
}
