﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class CommentsModel
    {
        public string Description { get; set; }

        public DateTime CommentTime { get; set; }
      
        public string UserName { get; set; }

        public string Photo { get; set; }

    }
}
