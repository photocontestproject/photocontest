﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using WebApplication1.Models.CardsModels;

namespace WebApplication1.Models
{
    public class IndexModel
    {
        [Required]
        [EmailAddress]
        // [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public List<ContestCardModel> ContestCards { get; set; }
        public List<UserCardModel> UserCardModels { get; set; }


    }
}
