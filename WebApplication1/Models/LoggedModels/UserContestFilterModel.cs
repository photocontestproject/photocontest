﻿using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace WebApplication1.Models.LoggedModels
{
    public class UserContestFilterModel
    {

        public IPagedList<ContestCardModel> CardsModels{ get; set; }

    }
}
