﻿using Microsoft.AspNetCore.Http;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models.LoggedModels
{
    public class CreateContestModel
    {
        [Required]
		[Display(Name = "Contest Name")]
        public string Name { get; set; }

        public string Profile { get; set; }

        [Required]
        [Display(Name = "Description")]
        [StringLength(maximumLength: 220, MinimumLength = 1, ErrorMessage = "The Description must be between 1 and 220 characters long")]
        public string Description { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Start Date")]
        //[StringLength(maximumLength: 20, MinimumLength = 3, ErrorMessage = "The Name must be between 3 and 20 characters long")]
        public DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Participants Limit")]
        public int ParticipantLimit { get; set; }

        [Required]
        [Display(Name = "Phase")]
        public int Phase { get; set; }

        [Required]
        public Category Category { get; set; }

        [Required]
        [DataType(DataType.Upload)]
        public IFormFile File { get; set; }

    }
}
