﻿using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models.CardsModels;

namespace WebApplication1.Models
{
    public class IndexModelLogged
    {
        public List<ContestCardModel> ContestCardModels { get; set; }
        public List<PhotoModel> PhotoCardModels { get; set; }

    }
}
