﻿using AutoMapper;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;
using WebApplication1.Models.CardsModels;
using WebApplication1.Models.LoggedModels;

namespace WebApplication1.MapperConfig
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Contest, ContestCardModel>()
              .ForMember(dest => dest.AdminName, opt => opt.MapFrom(src => src.Admin.UserName))
              .ForMember(dest => dest.Profile, opt => opt.MapFrom(src => src.Profile))
              .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
              .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.EndDate))
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
              .ForMember(dest => dest.ParticipantLimit, opt => opt.MapFrom(src => src.ParticipantLimit))
              .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
              //.ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category))
              .ForMember(dest => dest.Phase, opt => opt.MapFrom(src => src.Phase));

            CreateMap<CreateContestModel, Contest>()
               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
               .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
               .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
               .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.EndDate))
               .ForMember(dest => dest.ParticipantLimit, opt => opt.MapFrom(src => src.ParticipantLimit));
              
            CreateMap<User, UserCardModel>()
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                 .ForMember(dest => dest.ProfileLink, opt => opt.MapFrom(src => src.ProfileLink))
                 .ForMember(dest => dest.Rank, opt => opt.MapFrom(src => src.Rank.RankName))
                 .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description));

            CreateMap<UserCardModel, UserViewModel>();

            CreateMap<PhotoScoreContest, PhotoModel>()
                .ForMember(dest => dest.Aperture, opt => opt.MapFrom(src => src.Photo.Aperture))
                .ForMember(dest => dest.Camera, opt => opt.MapFrom(src => src.Photo.Camera))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Photo.Description))
                .ForMember(dest => dest.FocalLength, opt => opt.MapFrom(src => src.Photo.FocalLength))
                .ForMember(dest => dest.ISO, opt => opt.MapFrom(src => src.Photo.ISO))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Photo.Name))
                .ForMember(dest => dest.PhotoLink, opt => opt.MapFrom(src => src.Photo.PhotoLink))
                .ForMember(dest => dest.ShutterSpeed, opt => opt.MapFrom(src => src.Photo.ShutterSpeed))
                .ForMember(dest => dest.PhotoScore, opt => opt.MapFrom(src => src.Score));


            CreateMap<ContestUser, ContestCardModel>()
                .ForMember(dest => dest.AdminName, opt => opt.MapFrom(src => src.Contest.Admin.UserName))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Contest.Description))
                .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.Contest.EndDate))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Contest.Name))
                .ForMember(dest => dest.ParticipantLimit, opt => opt.MapFrom(src => src.Contest.ParticipantLimit))
                .ForMember(dest => dest.Profile, opt => opt.MapFrom(src => src.Contest.Profile))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.Contest.StartDate))
                .ForMember(dest => dest.Phase, opt => opt.MapFrom(src => src.Contest.Phase));



            CreateMap<Photo, PhotoModel>()
                 .ForMember(dest => dest.Aperture, opt => opt.MapFrom(src => src.Aperture))
                 .ForMember(dest => dest.Camera, opt => opt.MapFrom(src => src.Camera))
                 .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                 .ForMember(dest => dest.FocalLength, opt => opt.MapFrom(src => src.FocalLength))
                 .ForMember(dest => dest.ISO, opt => opt.MapFrom(src => src.ISO))
                 .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                 .ForMember(dest => dest.PhotoLink, opt => opt.MapFrom(src => src.PhotoLink))
                 .ForMember(dest => dest.ShutterSpeed, opt => opt.MapFrom(src => src.ShutterSpeed))
                 .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.UserName))
                 .ForMember(dest => dest.UserPhoto, opt => opt.MapFrom(src => src.User.ProfileLink));

            CreateMap<Comment, CommentsModel>()
               .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.UserName))
               .ForMember(dest => dest.Photo, opt => opt.MapFrom(src => src.User.ProfileLink))
               .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
               .ForMember(dest => dest.CommentTime, opt => opt.MapFrom(src => src.CommentTime));

            CreateMap<PhotoModel, Photo>()
                .ForMember(dest => dest.Aperture, opt => opt.MapFrom(src => src.Aperture))
                .ForMember(dest => dest.Camera, opt => opt.MapFrom(src => src.Camera))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.FocalLength, opt => opt.MapFrom(src => src.FocalLength))
                .ForMember(dest => dest.ISO, opt => opt.MapFrom(src => src.ISO))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.ShutterSpeed, opt => opt.MapFrom(src => src.ShutterSpeed));


            CreateMap<RegisterModel, User>()
              .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email))
              .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
              .ForMember(dest => dest.NormalizedUserName, opt => opt.MapFrom(src => src.Email.Normalize()))
              .ForMember(dest => dest.NormalizedEmail, opt => opt.MapFrom(src => src.Email.Normalize()));

    }
    }
}
