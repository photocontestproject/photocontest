﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContestServicess.ServiceContracts;
using PhotoContestServicess.Services;
using WebApplication1.Models;
using WebApplication1.Models.CardsModels;
using WebApplication1.Models.LoggedModels;
using X.PagedList;

namespace WebApplication1.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        const string address = "https://photocontest.blob.core.windows.net/youtubedemo/";

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IPhotoService _photoService;
        private readonly IContestService _contestService;
        private readonly IMapper _mapper;
        private readonly ICategoryService _categoryService;
        private readonly PhotoContestContext _context;
        private readonly IUsersServices _usersServices;

        public AdminController(UserManager<User> userManager,
            SignInManager<User> signInManager,
            IPhotoService service, IContestService service1, IMapper mapper, ICategoryService categoryService, PhotoContestContext context, IUsersServices usersServices)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _photoService = service;
            _contestService = service1;
            _mapper = mapper;
            _categoryService = categoryService;
            _context = context;
            _usersServices = usersServices;
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {

            var getUser = await _userManager.GetUserAsync(User);

            var contests = await _contestService.GetAllContests();

            if (getUser.Contests.Count() != 0)
            {

                contests = await _contestService.GetUserContests(getUser.Id);
            }

            var photoWinners = await _photoService.GetPhotosWinners();

            var cont = _mapper.Map<List<Contest>, List<ContestCardModel>>(contests);
            var photos = _mapper.Map<List<Photo>, List<PhotoModel>>(photoWinners);

            var indexLoggedModel = new IndexModelLogged() { ContestCardModels = cont, PhotoCardModels = photos };

            return View("Index", indexLoggedModel);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UserContestsSearchAsync(string id, IFormCollection col, int? page)
        {
            var getUser = await _userManager.GetUserAsync(User);
            var categories = _categoryService.GettAll();
            ViewData["catname"] = categories;

            if (getUser.RankId == 3 || getUser.RankId == 4)
            {
                ViewData["Rank"] = "ent";
            }

            else
            {
                ViewData["Rank"] = "amt";
            }

            if (id == "usercont")
            {
                var search = col["search"][0].ToString();

                var contusers = await _contestService.ContestCategory(1, getUser.Id);
                var cot = _mapper.Map<List<Contest>, List<ContestCardModel>>(contusers.Where(c => c.Name.Contains(search)).ToList());

                var pageNumber = page ?? 1;
                int pageSize = 3;
                var onePageOfContests = cot.ToPagedList(pageNumber, pageSize);
                var indexLoggedModel = new UserContestFilterModel() { CardsModels = onePageOfContests };
                ViewData["numOfCont"] = "user";

                return View("UserContestsFilter", indexLoggedModel);
            }
            else
            {
                var search = col["search"][0].ToString();

                var contusers = await _contestService.GetAllContests();
                var cot = _mapper.Map<List<Contest>, List<ContestCardModel>>(contusers.Where(c => c.Name.Contains(search)).ToList());

                var pageNumber = page ?? 1;
                int pageSize = 3;
                var onePageOfContests = cot.ToPagedList(pageNumber, pageSize);
                var indexLoggedModel = new UserContestFilterModel() { CardsModels = onePageOfContests };
                ViewData["numofcont"] = "all";

                return View("UserContestsFilter", indexLoggedModel);
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UserContestsAsync(string id, int? page)
        {
            var getUser = await _userManager.GetUserAsync(User);
            var categories = _categoryService.GettAll();
            ViewData["catname"] = categories;

            if (id == "usercont")
            {
                var contusers = await _contestService.GetAdminContests(getUser.Id);
                var cot = _mapper.Map<List<Contest>, List<ContestCardModel>>(contusers);

                var pageNumber = page ?? 1;
                int pageSize = 3;
                var onePageOfContests = cot.ToPagedList(pageNumber, pageSize);

                var indexLoggedModel = new UserContestFilterModel() { CardsModels = onePageOfContests };
                ViewData["numOfCont"] = "user";

                return View("UserContestsFilter", indexLoggedModel);
            }
            else
            {
                var contusers = await _contestService.GetAllContests();
                var cot = _mapper.Map<List<Contest>, List<ContestCardModel>>(contusers);
                ViewData["numOfCont"] = "all";
                var pageNumber = page ?? 1;
                int pageSize = 3;
                var onePageOfContests = cot.ToPagedList(pageNumber, pageSize);
                var indexLoggedModel = new UserContestFilterModel() { CardsModels = onePageOfContests };

                return View("UserContestsFilter", indexLoggedModel);
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UserContFilterAsync(IFormCollection collection, int? page, string id)
        {
            var getUser = await _userManager.GetUserAsync(User);
            var categories = _categoryService.GettAll();
            ViewData["catname"] = categories;
            var catId = 0;

            if (getUser.RankId == 3 || getUser.RankId == 4)
            {
                ViewData["Rank"] = "ent";
            }
            else
            {
                ViewData["Rank"] = "amt";
            }

            string catname = string.Empty;

            if (collection.ContainsKey("radioNoLabel"))
            {
                catname = collection["radioNoLabel"];
                catId = await _categoryService.GetCategoryid(catname);
            }

            var phaselist = new List<int>();

            for (int i = 1; i <= 3; i++)
            {
                if (collection.ContainsKey(i.ToString()))
                {
                    phaselist.Add(i);

                }
            }

            var contcard = new List<Contest>();

            if (id == "all")
            {
                contcard = await _contestService.GetAllContestsFilter(catId, phaselist);
                ViewData["numofcont"] = id;
            }

            if (id == "user")
            {
                contcard = await _contestService.ContestCatUserPhaseAdmin(catId, getUser.Id, phaselist);
                ViewData["numofcont"] = id;
            }

            var contescards = _mapper.Map<List<Contest>, List<ContestCardModel>>(contcard);

            var pageNumber = page ?? 1;
            int pageSize = 3;
            var onePageOfContests = contescards.ToPagedList(pageNumber, pageSize);
            var indexLoggedModel = new UserContestFilterModel() { CardsModels = onePageOfContests };

            return View("UserContestsFilter", indexLoggedModel);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> PhotoDetails(string id)
        {
            var photo = await _photoService.GetPhotosByName(id);
            var getUser = await _usersServices.GetUserFromPhoto(photo.Id);
            var userRank = await _usersServices.GetUserRank(getUser.RankId);
            var photomodel = _mapper.Map<Photo, PhotoModel>(photo);
            photomodel.UserRank = userRank;

            var commentsmodel = _mapper.Map<List<Comment>, List<CommentsModel>>(photo.Comments.ToList());
            photomodel.Comments = commentsmodel;

            return View("PhotoDetails", photomodel);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ContestsInsideAsync(string id, int? page)
        {
            var getUser = await _userManager.GetUserAsync(User);
            var cont = await _contestService.GetContest(id);

            var checkJoined = await _usersServices.CheckIfUserJoinedContest(getUser.Id, cont.Id);
            var category = await _categoryService.GetCategoryname(cont.CategoryId);

            var photomodels = _mapper.Map<List<PhotoScoreContest>, List<PhotoModel>>(_contestService.GetContestPhotos(id));

            var contescard = _mapper.Map<Contest, ContestCardModel>(cont);
            contescard.Category = category;
            contescard.PhotosCount = photomodels.Count();

            if (getUser.RankId == 3 || getUser.RankId == 4)
            {
                ViewData["Rank"] = "ent";
                bool rated = await _usersServices.CheckIfUserRatedContest(getUser.Id, cont.Id);
                contescard.Rated = rated;
            }
            else
            {
                ViewData["Rank"] = "amt";
                contescard.Rated = false;
            }

            if (checkJoined == true)
            {
                if (contescard.Rated == false)
                {
                    contescard.Joinned = "yes";
                }
                else
                {
                    contescard.Joinned = "no";
                }
            }
            else
            {
                contescard.Joinned = "no";
            }

            var pageNumber = page ?? 1;
            int pageSize = 3;
            var onePageOfContests = photomodels.ToPagedList(pageNumber, pageSize);

            ViewData["listphotos"] = onePageOfContests;

            return View("ContestInside", contescard);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ManageContests(int? page, string id)
        {
            var getUser = await _userManager.GetUserAsync(User);
            var categories = _categoryService.GettAll();
            ViewData["catname"] = categories;


            var admcontests = await _contestService.GetAdminContests(getUser.Id);

            var contescards = _mapper.Map<List<Contest>, List<ContestCardModel>>(admcontests);

            var pageNumber = page ?? 1;
            int pageSize = 3;
            var onePageOfContests = contescards.ToPagedList(pageNumber, pageSize);

            return View("ContestsAdmin", onePageOfContests);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ManageContest(IFormCollection collection, int? page)
        {
            var getUser = await _userManager.GetUserAsync(User);
            var categories = _categoryService.GettAll();
            ViewData["catname"] = categories;
            var catId = 0;

            if (getUser.RankId == 3 || getUser.RankId == 4)
            {
                ViewData["Rank"] = "ent";
            }
            else
            {
                ViewData["Rank"] = "amt";
            }

            string catname = string.Empty;

            if (collection.ContainsKey("radioNoLabel"))
            {
                catname = collection["radioNoLabel"];
                catId = await _categoryService.GetCategoryid(catname);
            }

            var phaselist = new List<int>();

            for (int i = 1; i <= 3; i++)
            {
                if (collection.ContainsKey(i.ToString()))
                {
                    phaselist.Add(i);

                }
            }

            var contcard = new List<Contest>();

            contcard = await _contestService.ContestCatUserPhaseAdmin(catId, getUser.Id, phaselist);

            var contescards = _mapper.Map<List<Contest>, List<ContestCardModel>>(contcard);

            var pageNumber = page ?? 1;
            int pageSize = 3;
            var onePageOfContests = contescards.ToPagedList(pageNumber, pageSize);
            var indexLoggedModel = new UserContestFilterModel() { CardsModels = onePageOfContests };

            return View("ContestsAdmin", onePageOfContests);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> CreateContest()
        {
            var getUser = await _userManager.GetUserAsync(User);

            var createmodel = new CreateContestModel();

            return View("CreateContest", createmodel);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> CreateContestPost(CreateContestModel model)
        {
            var getUser = await _userManager.GetUserAsync(User);

            if (model.StartDate < DateTime.Now && model.EndDate < DateTime.Now)
            {
                if (ModelState.IsValid)
                {
                    var cat = await _categoryService.GetCategoryid(model.Category.Name);

                    await _photoService.Upload(model.File);
                    var name = model.File.FileName.Trim('"');
                    var profile = address + name;

                    var photo = new Photo()
                    {
                        UserId = getUser.Id,
                        PhotoLink = address + name
                    };

                    var contest = _mapper.Map<CreateContestModel, Contest>(model);

                    _context.Photos.Add(photo);
                    _context.SaveChanges();

                    contest.Phase = 1;
                    contest.AdminId = getUser.Id;
                    contest.CategoryId = cat;
                    contest.Profile = profile;

                    //var endDate = contest.EndDate;
                    //var startDate = contest.StartDate;
                    //var contestName = contest.Name;
                    //var timeSpan = TimeSpan.FromHours(1);

                    //_contestService.SetTimer(endDate, startDate, contestName, timeSpan);

                    _context.Contests.Add(contest);
                    _context.SaveChanges();

                    return RedirectToAction("SetTimer", contest);
                    //return View("SuccesCreate");
                }
            }

            return RedirectToAction("Privacy", "Home");
        }

        public IActionResult SetTimer(CreateContestModel contest)
        {
            var endDate = contest.EndDate;
            var startDate = contest.StartDate;
            var contestName = contest.Name;
            var timeSpan = TimeSpan.FromMinutes(3);

            _contestService.SetTimer(endDate, startDate, contestName, timeSpan);
            return View("SuccesCreate");
        }


        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetUsers(int? page)
        {
            var getUser = await _userManager.GetUserAsync(User);

            var userrank = await _usersServices.GetUserAndRoles();

            var usercards = _mapper.Map<List<User>, List<UserCardModel>>(userrank);

            ViewData["rankname"] = _context.Ranks.ToList();

            var pageNumber = page ?? 1;
            int pageSize = 9;
            var onePageOfContests = usercards.ToPagedList(pageNumber, pageSize);

            return View("UsersViewAdmin", onePageOfContests);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetUsers(IFormCollection collection, int? page)
        {
            var getUser = await _userManager.GetUserAsync(User);

            var rank = new List<string>();

            foreach (var item in _context.Ranks.ToList())
            {
                if (collection.ContainsKey(item.RankName))
                    rank.Add(collection[item.RankName]);
            }

            var users = await _usersServices.GetUserRankFilter(rank);

            var usercards = _mapper.Map<List<User>, List<UserCardModel>>(users);

            ViewData["rankname"] = _context.Ranks.ToList();

            var pageNumber = page ?? 1;
            int pageSize = 9;
            var onePageOfContests = usercards.ToPagedList(pageNumber, pageSize);

            return View("UsersViewAdmin", onePageOfContests);

        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DelUsers(string id, int? page)
        {
            var user = await _usersServices.GetUserByName(id);
            user.IsDeleted = true;
            _context.SaveChanges();

            var getUser = await _userManager.GetUserAsync(User);

            var userrank = await _usersServices.GetUserAndRoles();

            var usercards = _mapper.Map<List<User>, List<UserCardModel>>(userrank);

            ViewData["rankname"] = _context.Ranks.ToList();

            var pageNumber = page ?? 1;
            int pageSize = 9;
            var onePageOfContests = usercards.ToPagedList(pageNumber, pageSize);

            return View("UsersViewAdmin", onePageOfContests);
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteContest(string id)
        {
            var contest = await _contestService.GetContest(id);
            _contestService.DeleteContest(contest);
            return RedirectToAction("ManageContests");
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UserDetails(string id)
        {
            var getUser = await _userManager.GetUserAsync(User);
            User user = null;

            var usercontests = new List<Contest>();

            if (id != null)
            {
                user = await _usersServices.GetUserByName(id);
                usercontests = await _contestService.GetUserContests(user.Id);
            }
            else
            {
                user = await _usersServices.GetUser(getUser.Id);
                usercontests = await _contestService.GetAdminContests(user.Id);
            }


            var photos = await _photoService.GetPhotosUser(user.Id);

            var usercard = _mapper.Map<User, UserCardModel>(user);
            var userphotos = _mapper.Map<List<Photo>, List<PhotoModel>>(photos);
            var contestscard = _mapper.Map<List<Contest>, List<ContestCardModel>>(usercontests);
            var userviewmodel = _mapper.Map<UserCardModel, UserViewModel>(usercard);

            userviewmodel.Contests = contestscard;
            userviewmodel.Photos = userphotos;

            return View("UserView", userviewmodel);
            //edit button
            //followers
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ExplorePhotos(int? page)
        {
            var photos = await _photoService.GetAllPhotos();
            var userphotos = _mapper.Map<List<Photo>, List<PhotoModel>>(photos);

            var pageNumber = page ?? 1;
            int pageSize = 3;
            var onePageOfContests = userphotos.ToPagedList(pageNumber, pageSize);

            return View("ExplorePhotos", onePageOfContests);

        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ExplorePhotos(string search)
        {
            var photos = await _photoService.GetAllPhotosSearch(search);
            var userphotos = _mapper.Map<List<PhotoScoreContest>, List<PhotoModel>>(photos);

            var onePageOfContests = userphotos.ToPagedList();

            return View("ExplorePhotos", onePageOfContests);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AboutUs()
        {
            var users = await _usersServices.GetAdmins();
            var usermodel = _mapper.Map<List<User>, List<UserCardModel>>(users);
            return View("About Us", usermodel);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetUserApiKey(string id)
        {
            var apiKey = await _usersServices.GetApiKey(id);
            ViewData["apikey"] = apiKey;
            return View("ApiKey");
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}