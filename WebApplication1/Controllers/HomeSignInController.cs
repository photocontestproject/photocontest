﻿
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContestServicess.ServiceContracts;
using PhotoContestServicess.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Loader;
using System.Threading.Tasks;
using WebApplication1.Models;
using WebApplication1.Models.CardsModels;
using WebApplication1.Models.LoggedModels;
using X.PagedList;

namespace WebApplication1.Controllers
{
    [Authorize(Roles = "User")]
    public class HomeSignInController : Controller
    {
        const string address = "https://photocontest.blob.core.windows.net/youtubedemo/";

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IPhotoService _photoService;
        private readonly IContestService _contestService;
        private readonly IMapper _mapper;
        private readonly ICategoryService _categoryService;
        private readonly PhotoContestContext _context;
        private readonly IUsersServices _usersServices;
        private readonly ILogger _logger;


        public HomeSignInController(UserManager<User> userManager,
            SignInManager<User> signInManager,
            IPhotoService service, IContestService service1,
            IMapper mapper, ICategoryService categoryService,
            PhotoContestContext context, IUsersServices usersServices,
            ILogger<HomeSignInController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _photoService = service;
            _contestService = service1;
            _mapper = mapper;
            _categoryService = categoryService;
            _context = context;
            _usersServices = usersServices;
            _logger = logger;
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> Index()
        {
            var getUser = await _userManager.GetUserAsync(User);

            if (getUser.RankId == 3 || getUser.RankId == 4)
            {
                ViewData["Rank"] = "ent";
            }
            else
            {
                ViewData["Rank"] = "amt";
            }
            var contests = await _contestService.GetAllContests();

            if (getUser.Contests.Count() != 0)
            {
                contests = await _contestService.GetUserContests(getUser.Id);
            }

            var photoWinners = await _photoService.GetPhotosWinners();

            var cont = _mapper.Map<List<Contest>, List<ContestCardModel>>(contests);
            var photos = _mapper.Map<List<Photo>, List<PhotoModel>>(photoWinners);

            var indexLoggedModel = new IndexModelLogged() { ContestCardModels = cont, PhotoCardModels = photos };

            _logger.LogInformation("Home page created.");

            return View("Index", indexLoggedModel);

        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> UserContestsSearchAsync(string id, IFormCollection col, int? page)
        {
            var getUser = await _userManager.GetUserAsync(User);
            var categories = _categoryService.GettAll();
            ViewData["catname"] = categories;

            if (getUser.RankId == 3 || getUser.RankId == 4)
            {
                ViewData["Rank"] = "ent";
            }

            else
            {
                ViewData["Rank"] = "amt";
            }

            if (id == "usercont")
            {
                var search = col["search"][0].ToString();

                var contusers = await _contestService.ContestCategory(1, getUser.Id);
                var cot = _mapper.Map<List<Contest>, List<ContestCardModel>>(contusers.Where(c => c.Name.Contains(search)).ToList());

                var pageNumber = page ?? 1;
                int pageSize = 3;
                var onePageOfContests = cot.ToPagedList(pageNumber, pageSize);
                var indexLoggedModel = new UserContestFilterModel() { CardsModels = onePageOfContests };
                ViewData["numOfCont"] = "user";

                _logger.LogInformation("User contest search created.");

                return View("UserContestsFilter", indexLoggedModel);
            }

            else
            {
                var search = col["search"][0].ToString();

                var contusers = await _contestService.GetAllContests();
                var cot = _mapper.Map<List<Contest>, List<ContestCardModel>>(contusers.Where(c => c.Name.Contains(search)).ToList());

                var pageNumber = page ?? 1;
                int pageSize = 3;
                var onePageOfContests = cot.ToPagedList(pageNumber, pageSize);
                var indexLoggedModel = new UserContestFilterModel() { CardsModels = onePageOfContests };
                ViewData["numofcont"] = "all";

                return View("UserContestsFilter", indexLoggedModel);
            }

        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> UserContestsAsync(string id, int? page)
        {
            var getUser = await _userManager.GetUserAsync(User);
            var categories = _categoryService.GettAll();
            ViewData["catname"] = categories;

            if (getUser.RankId == 3 || getUser.RankId == 4)
            {
                ViewData["Rank"] = "ent";
            }

            else
            {
                ViewData["Rank"] = "amt";
            }

            if (id == "usercont")
            {
                var contusers = await _contestService.GetUserContests(getUser.Id);
                var cot = _mapper.Map<List<Contest>, List<ContestCardModel>>(contusers);

                var pageNumber = page ?? 1;
                int pageSize = 3;
                var onePageOfContests = cot.ToPagedList(pageNumber, pageSize);
                var indexLoggedModel = new UserContestFilterModel() { CardsModels = onePageOfContests };
                ViewData["numOfCont"] = "user";

                return View("UserContestsFilter", indexLoggedModel);
            }

            else
            {
                var contusers = await _contestService.GetAllContests();
                var cot = _mapper.Map<List<Contest>, List<ContestCardModel>>(contusers);

                var pageNumber = page ?? 1;
                int pageSize = 3;
                var onePageOfContests = cot.ToPagedList(pageNumber, pageSize);
                var indexLoggedModel = new UserContestFilterModel() { CardsModels = onePageOfContests };
                ViewData["numofcont"] = "all";

                return View("UserContestsFilter", indexLoggedModel);
            }
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> UserContFilterAsync(IFormCollection collection, int? page, string id)
        {
            var getUser = await _userManager.GetUserAsync(User);
            var categories = _categoryService.GettAll();
            ViewData["catname"] = categories;
            var catId = 0;

            if (getUser.RankId == 3 || getUser.RankId == 4)
            {
                ViewData["Rank"] = "ent";
            }
            else
            {
                ViewData["Rank"] = "amt";
            }

            string catname = string.Empty;

            if (collection.ContainsKey("radioNoLabel"))
            {
                catname = collection["radioNoLabel"];
                catId = await _categoryService.GetCategoryid(catname);
            }

            var phaselist = new List<int>();

            for (int i = 1; i <= 3; i++)
            {
                if (collection.ContainsKey(i.ToString()))
                {
                    phaselist.Add(i);

                }
            }

            var contcard = new List<Contest>();

            if (id == "all")
            {
                contcard = await _contestService.GetAllContestsFilter(catId, phaselist);
                ViewData["numofcont"] = id;
            }

            if (id == "user")
            {
                contcard = await _contestService.ContestCatUserPhase(catId, getUser.Id, phaselist);
                ViewData["numofcont"] = id;
            }

            var contescards = _mapper.Map<List<Contest>, List<ContestCardModel>>(contcard);

            var pageNumber = page ?? 1;
            int pageSize = 3;
            var onePageOfContests = contescards.ToPagedList(pageNumber, pageSize);
            var indexLoggedModel = new UserContestFilterModel() { CardsModels = onePageOfContests };

            return View("UserContestsFilter", indexLoggedModel);
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> ContestsInsideAsync(string id, int? page)
        {
            var getUser = await _userManager.GetUserAsync(User);
            var cont = await _contestService.GetContest(id);

            var checkJoined = await _usersServices.CheckIfUserJoinedContest(getUser.Id, cont.Id);
            var category = await _categoryService.GetCategoryname(cont.CategoryId);

            var photomodels = _mapper.Map<List<PhotoScoreContest>, List<PhotoModel>>(_contestService.GetContestPhotos(id));
            var contescard = _mapper.Map<Contest, ContestCardModel>(cont);

            contescard.Category = category;
            contescard.PhotosCount = photomodels.Count();

            if (getUser.RankId == 3 || getUser.RankId == 4)
            {
                ViewData["Rank"] = "ent";
                bool rated = await _usersServices.CheckIfUserRatedContest(getUser.Id, cont.Id);
                contescard.Rated = rated;
            }
            else
            {
                ViewData["Rank"] = "amt";
                contescard.Rated = false;
            }

            if (checkJoined == true)
            {
                if (contescard.Rated == false)
                {
                    contescard.Joinned = "yes";
                }
                else
                {
                    contescard.Joinned = "no";
                }
            }
            else
            {
                contescard.Joinned = "no";
            }

            var pageNumber = page ?? 1;
            int pageSize = 3;
            var onePageOfContests = photomodels.ToPagedList(pageNumber, pageSize);

            ViewData["listphotos"] = onePageOfContests;

            return View("ContestInside", contescard);
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> JoinContest(string id)
        {
            var getUser = await _userManager.GetUserAsync(User);

            var photomodel = new PhotoModel();

            ViewData["currContest"] = id;

            return View("JoinContest");
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> JoinContest(PhotoModel model, IFormFile file, string id)
        {
            var getUser = await _userManager.GetUserAsync(User);
            try
            {
                if (ModelState.IsValid)
                {
                    var photo = _mapper.Map<PhotoModel, Photo>(model);
                    photo.UserId = getUser.Id;

                    if (model.PhotoLink != null)
                    {
                        photo.PhotoLink = model.PhotoLink;
                        var name = file.FileName.Trim('"');
                    }
                    else
                    {
                        await _photoService.Upload(file);
                        var name = file.FileName.Trim('"');
                        photo.PhotoLink = address + name;
                    }

                    _context.Photos.Add(photo);
                    _context.SaveChanges();

                    _photoService.ContestPhotoUpdate(photo.Name, id, getUser.Id);
                }

                return RedirectToAction("ContestsInside", "HomeSignIn", new { id });
            }
            catch
            {
                return RedirectToAction("Privacy", "Home");
            }
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> RateContest(int? page, string id)
        {
            var getUser = await _userManager.GetUserAsync(User);
            var cont = await _contestService.GetContest(id);

            var photomodels = _mapper.Map<List<PhotoScoreContest>, List<PhotoModel>>(_contestService.GetContestPhotos(id));

            return View("RateContestPhotosPage", photomodels);
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> RateContest(IFormCollection col, string id)
        {
            object a = 0;
            ViewData.TryGetValue("aaa", out a);

            var getUser = await _userManager.GetUserAsync(User);

            var cont = await _contestService.GetContest(id);

            var contName = cont.Name;

            var contPhotos = _mapper.Map<List<PhotoScoreContest>, List<PhotoModel>>(_contestService.GetContestPhotos(id));

            for (int i = 0; i < contPhotos.Count(); i++)
            {
                if (col.ContainsKey(contPhotos[i].Name))
                {
                    var photo = await _photoService.GetPhotosByName(contPhotos[i].Name);

                    if (col[contPhotos[i].Name].Count() > 1)
                    {
                        photo.ContestScore = 0 + photo.ContestScore;

                        var comment = new Comment()
                        {
                            Description = "The category is wrong",
                            CommentTime = DateTime.Now,
                            UserId = getUser.Id,
                            PhotoId = photo.Id
                        };
                        _context.Comments.Add(comment);
                    }
                    else if (int.Parse(col[contPhotos[i].Name].ToString()) == 0)
                    {
                        photo.ContestScore = 3 + photo.ContestScore;
                    }
                    else
                    {
                        photo.ContestScore = int.Parse(col[contPhotos[i].Name].ToString()) + photo.ContestScore;
                    }

                    var dkey = contPhotos[i].Name + " com";
                    var com = col[dkey].ToString();

                    if (com.Count() > 0)
                    {
                        var comment = new Comment()
                        {
                            Description = com,
                            CommentTime = DateTime.Now,
                            UserId = getUser.Id,
                            PhotoId = photo.Id
                        };
                        _context.Comments.Add(comment);
                    }
                }
                else
                {
                    continue;
                }
            }
            _context.SaveChanges();
            //
            _usersServices.ContestUserUpdate(contName, getUser.Id);

            return RedirectToAction("ContestsInside", "HomeSignIn", new
            {
                id
            });
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> PhotoDetails(string id)
        {
            try
            {
                var photo = await _photoService.GetPhotosByName(id);
                var getUser = await _usersServices.GetUserFromPhoto(photo.Id);
                var userRank = await _usersServices.GetUserRank(getUser.RankId);
                var photomodel = _mapper.Map<Photo, PhotoModel>(photo);
                photomodel.UserRank = userRank;

                var commentsmodel = _mapper.Map<List<Comment>, List<CommentsModel>>(photo.Comments.ToList());
                photomodel.Comments = commentsmodel;

                return View("PhotoDetails", photomodel);
            }
            catch
            {
                return RedirectToAction("Privacy", "Home");
            }
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> UserDetails(string id)
        {
            var getUser = await _userManager.GetUserAsync(User);
            User user = null;

            try
            {

                if (id != null)
                {
                    user = await _usersServices.GetUserByName(id);
                }
                else
                {
                    user = await _usersServices.GetUser(getUser.Id);
                }
                var usercontests = await _contestService.GetUserContests(user.Id);
                var photos = await _photoService.GetPhotosUser(user.Id);
                var rank = await _usersServices.GetUserRank(user.RankId);

                var usercard = _mapper.Map<User, UserCardModel>(user);
                var userphotos = _mapper.Map<List<Photo>, List<PhotoModel>>(photos);
                var contestscard = _mapper.Map<List<Contest>, List<ContestCardModel>>(usercontests);
                var userviewmodel = _mapper.Map<UserCardModel, UserViewModel>(usercard);

                usercard.Rank = rank;
                userviewmodel.Contests = contestscard;
                userviewmodel.Photos = userphotos;

                return View("UserView", userviewmodel);
                //edit button
            }
            catch
            {
                return RedirectToAction("Privacy", "Home");
            }
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> GetUsers(int? page)
        {
            var getUser = await _userManager.GetUserAsync(User);
            var users = await _usersServices.GetUsers(9);
            var usercards = _mapper.Map<List<User>, List<UserCardModel>>(users);

            ViewData["rankname"] = _context.Ranks.ToList();

            var pageNumber = page ?? 1;
            int pageSize = 9;
            var onePageOfContests = usercards.ToPagedList(pageNumber, pageSize);

            return View("UsersViewUser", onePageOfContests);

        }

        [HttpPost]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> GetUsers(IFormCollection collection, int? page)
        {
            var getUser = await _userManager.GetUserAsync(User);

            var rank = new List<string>();

            foreach (var item in _context.Ranks.ToList())
            {
                if (collection.ContainsKey(item.RankName))
                    rank.Add(collection[item.RankName]);
            }

            var users = await _usersServices.GetUserRankFilter(rank);

            var usercards = _mapper.Map<List<User>, List<UserCardModel>>(users);

            ViewData["rankname"] = _context.Ranks.ToList();

            var pageNumber = page ?? 1;
            int pageSize = 9;
            var onePageOfContests = usercards.ToPagedList(pageNumber, pageSize);

            return View("UsersViewUser", onePageOfContests);

        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> EditUserProfile()
        {
            var getUser = await _userManager.GetUserAsync(User);

            var user = await _usersServices.GetUser(getUser.Id);

            var usercard = _mapper.Map<User, UserCardModel>(user);
            var userviewmodel = _mapper.Map<UserCardModel, UserViewModel>(usercard);

            return View("UpdateUser", userviewmodel);
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> EditUserProfile(UserViewModel model, IFormFile file)
        {
            var getUser = await _userManager.GetUserAsync(User);

            if (ModelState.IsValid)
            {
                var user = await _usersServices.GetUser(getUser.Id);
                if (file != null)
                {
                    await _photoService.Upload(file);
                    var name = file.FileName.Trim('"');
                    var profile = address + name;

                    user.ProfileLink = profile;
                }
                if (model.Description != null)
                {
                    user.Description = model.Description;
                }
                _context.SaveChanges();

                return RedirectToAction("UserDetails", "HomeSignIn", new { id = getUser.UserName });
            }
            else
            {
                return RedirectToAction("Privacy", "Home");
            }
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> ExplorePhotos(int? page)
        {
            var photos = await _photoService.GetAllPhotos();
            var userphotos = _mapper.Map<List<Photo>, List<PhotoModel>>(photos);

            var pageNumber = page ?? 1;
            int pageSize = 3;
            var onePageOfContests = userphotos.ToPagedList(pageNumber, pageSize);

            return View("ExplorePhotos", onePageOfContests);
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> ExplorePhotos(string search)
        {
            var photos = await _photoService.GetAllPhotosSearch(search);
            var userphotos = _mapper.Map<List<PhotoScoreContest>, List<PhotoModel>>(photos);

            var onePageOfContests = userphotos.ToPagedList();
            return View("ExplorePhotos", onePageOfContests);
        }

        [HttpGet]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> GetUserApiKey(string id)
        {
            var apiKey = await _usersServices.GetApiKey(id);
            ViewData["apikey"] = apiKey;
            return View("ApiKey");
        }

        [Authorize]
        public async Task<IActionResult> AboutUs()
        {
            var users = await _usersServices.GetAdmins();
            var usermodel = _mapper.Map<List<User>, List<UserCardModel>>(users);
            return View("About Us", usermodel);
        }

    }
}
