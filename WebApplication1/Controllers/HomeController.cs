﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContestServicess.ServiceContracts;
using PhotoContestServicess.Services;
using WebApplication1.Models;
using WebApplication1.Models.CardsModels;
using WebApplication1.Models.LoggedModels;
using X.PagedList;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IContestService _contestService;
        private readonly IUsersServices _usersServices;
        private readonly IMapper _mapper;
        private readonly PhotoContestContext _context;
        private readonly ICategoryService _categoryService;
        private readonly IPhotoService _photoService;

        public HomeController(UserManager<User> userManager, SignInManager<User> signInManager,
            IContestService contestService, IUsersServices usersServices, IMapper mapper, PhotoContestContext context,
            ICategoryService categoryService, IPhotoService photoService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _contestService = contestService;
            _usersServices = usersServices;
            _mapper = mapper;
            _categoryService = categoryService;
            _photoService = photoService;
        }

        public async Task<IActionResult> Index()
        {
            ViewData["Isindex"] = "yes";
            var md = new IndexModel();
            var cont = await _contestService.GetContests(6);
            var users = await _usersServices.GetUsers(6);
            md.UserCardModels = _mapper.Map<List<User>, List<UserCardModel>>(users);
            md.ContestCards = _mapper.Map<List<Contest>, List<ContestCardModel>>(cont);

            return View(md);
        }

        [HttpPost]
        public async Task<IActionResult> Index(IndexModel model)
        {
            if (ModelState.IsValid)
            {
                var username = await _usersServices.GetUserName(model.Email);

                var result = await _signInManager.PasswordSignInAsync(username, model.Password, isPersistent: false, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    var userget = await  _usersServices.GetUserByName(username);
                    await _usersServices.CheckUserRole(userget.Id);


                    if (await _usersServices.CheckUserRole(userget.Id) == "admin")
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                    if (await _usersServices.CheckUserRole(userget.Id) == "user")
                    {
                        return RedirectToAction("Index", "HomeSignIn");
                    }
                }
            }
            return RedirectToAction("Error", "Home");
        }

        [HttpGet]
        public IActionResult Register()
        {
            var model = new RegisterModel();
            return View("Register", model);
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            var passHasher = new Microsoft.AspNetCore.Identity.PasswordHasher<User>();
            if (ModelState.IsValid)
            {
                var user = _mapper.Map<RegisterModel, User>(model);
                user.PasswordHash = passHasher.HashPassword(user, model.Password);
                user.SecurityStamp = Guid.NewGuid().ToString();
                user.RankId = 1;

                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    var regularUserRole = new IdentityUserRole<int>();
                    regularUserRole.RoleId = 2;
                    regularUserRole.UserId = user.Id;
                    _context.Add(regularUserRole);
                    _context.SaveChanges();
                    return RedirectToAction("Index", "Home");
                }
            }
            return RedirectToAction("Error", "Home");
        }

        public async Task<IActionResult> UserContests(string id, int? page)
        {
            var categories = _categoryService.GettAll();
            ViewData["catname"] = categories;

            var contusers = await _contestService.GetAllContests();
            var cot = _mapper.Map<List<Contest>, List<ContestCardModel>>(contusers);

            var pageNumber = page ?? 1;
            int pageSize = 3;
            var onePageOfContests = cot.ToPagedList(pageNumber, pageSize);
            var indexLoggedModel = new UserContestFilterModel() { CardsModels = onePageOfContests };

            return View("UserContestsFilter", indexLoggedModel);
        }

        public async Task<IActionResult> UserContFilterAsync(IFormCollection collection, int? page)
        {
            var categories = _categoryService.GettAll();
            ViewData["catname"] = categories;
            var catId = 0;

            string catname = string.Empty;

            if (collection.ContainsKey("radioNoLabel"))
            {
                catname = collection["radioNoLabel"];
                catId = await _categoryService.GetCategoryid(catname);
            }

            var phaselist = new List<int>();

            for (int i = 1; i <= 3; i++)
            {
                if (collection.ContainsKey(i.ToString()))
                {
                    phaselist.Add(i);
                }
            }

            var contcard = new List<Contest>();

            contcard = await _contestService.GetAllContestsFilter(catId, phaselist);

            var contescards = _mapper.Map<List<Contest>, List<ContestCardModel>>(contcard);

            var pageNumber = page ?? 1;
            int pageSize = 3;
            var onePageOfContests = contescards.ToPagedList(pageNumber, pageSize);
            var indexLoggedModel = new UserContestFilterModel() { CardsModels = onePageOfContests };

            return View("UserContestsFilter", indexLoggedModel);
        }

        public async Task<ActionResult> ContestsInside(string id, int? page)
        {
            try
            {
                var cont = await _contestService.GetContest(id);
                var category = await _categoryService.GetCategoryname(cont.CategoryId);

                var photomodels = _mapper.Map<List<PhotoScoreContest>, List<PhotoModel>>(_contestService.GetContestPhotos(id));
                var contescard = _mapper.Map<Contest, ContestCardModel>(cont);

                contescard.Category = category;
                contescard.PhotosCount = photomodels.Count();

                var pageNumber = page ?? 1;
                int pageSize = 3;
                var onePageOfContests = photomodels.ToPagedList(pageNumber, pageSize);

                ViewData["listphotos"] = onePageOfContests;

                return View("ContestInside", contescard);
            }
            catch
            {
                return RedirectToAction("Privacy", "Home");
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public async Task<IActionResult> UserDetails(string id)
        {
            var getUser = await _userManager.GetUserAsync(User);
            User user = null;

            user = await _usersServices.GetUserByName(id);

            var usercontests = await _contestService.GetUserContests(user.Id);
            var photos = await _photoService.GetPhotosUser(user.Id);

            var usercard = _mapper.Map<User, UserCardModel>(user);
            var userphotos = _mapper.Map<List<Photo>, List<PhotoModel>>(photos);
            var contestscard = _mapper.Map<List<Contest>, List<ContestCardModel>>(usercontests);
            var userviewmodel = _mapper.Map<UserCardModel, UserViewModel>(usercard);

            userviewmodel.Contests = contestscard;
            userviewmodel.Photos = userphotos;

            return View("UserView", userviewmodel);
            //edit button
            //followers
        }

        public async Task<IActionResult> AboutUs()
        {
            var users = await _usersServices.GetAdmins();
            var usermodel = _mapper.Map<List<User>, List<UserCardModel>>(users);
            return View("About Us", usermodel);
        }
    }
}
