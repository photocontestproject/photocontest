﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContestServicess.Models
{
    public class UserModel
    {
        public string ProfileLink { get; set; }
        public int? RankId { get; set; }
        public string UserName { get; set; }

    }
}
