﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoContestServicess.ServiceContracts
{
    public interface IPhotoViewModelService
    {
        List<string> GetBlobsUri();
    }
}
