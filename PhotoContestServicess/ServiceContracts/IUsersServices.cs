﻿using PhotoContest.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContestServicess.Services
{
    public interface IUsersServices
    {
        Task<bool> CheckIfUserJoinedContest(int userId, int contesId);
        Task<bool> CheckIfUserRatedContest(int userId, int contesId);
        Task<string> CheckUserRole(int id);
        void ContestUserUpdate(string contname, int userid);
        Task<List<User>> GetAdmins();
        Task<List<User>> GetAllUsers();
        Task<string> GetApiKey(string userName);
        Task<User> GetUser(int id);
        Task<List<User>> GetUserAndRoles();
        Task<User> GetUserByEmail(string email);
        User GetUserById(int id);
        Task<User> GetUserByName(string name);
        Task<User> GetUserFromPhoto(int photoId);
        Task<string> GetUserName(string email);
        Task<string> GetUserRank(int? userRankId);
        Task<List<User>> GetUserRankFilter(List<string> rank);
        Task<List<User>> GetUsers(int num);
        void UpdateUser(int userId);
    }
}