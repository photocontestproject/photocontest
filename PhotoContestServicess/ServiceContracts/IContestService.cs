﻿using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContestServicess.Services
{
    public interface IContestService
    {
        void ChangeContestToExpired(string contestName);
        void ChangeContestToPhaseTwo(string contestName);
        Task<List<Contest>> ContestCategory(int categoryId, int userId);
        Task<List<Contest>> ContestCategoryAdmin(int categoryId, int adminId);
        Task<List<Contest>> ContestCatPhase(int categoryId, int phase);
        Task<List<Contest>> ContestCatUserPhase(int categoryId, int userId, int phase);
        Task<List<Contest>> ContestCatUserPhase(int categoryId, int userId, List<int> phases);
        Task<List<Contest>> ContestCatUserPhaseAdmin(int categoryId, int userId, List<int> phases);
        void DeleteContest(Contest contest);
        Task<List<Contest>> GetAdminContests(int adminId);
        Task<List<Contest>> GetAllContests();
        Task<List<Contest>> GetAllContestsFilter(int categoryId, List<int> phases);
        Task<Category> GetCategory(int? id);
        Task<Contest> GetContest(string name);
        List<PhotoScoreContest> GetContestPhotos(string id);
        Task<List<Contest>> GetContests(int num);
        Task<List<Contest>> GetUserContests(int userId);
        void TakeScore(string contestname);
        void SetTimer(DateTime endDate, DateTime startDate, string contestName, TimeSpan span);

        PhotoScoreContest GetPhotoScoreContest(int contestId, int photoId);

    }
}