﻿using PhotoContest.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContestServicess.Services
{
    public interface ICategoryService
    {
        Task<int> GetCategoryid(string name);
        Task<string> GetCategoryname(int? id);
        List<Category>GettAll();
    }
}