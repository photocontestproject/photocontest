﻿using Microsoft.AspNetCore.Http;
using PhotoContest.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContestServicess.Services
{
    public interface IPhotoService
    {
        void ContestPhotoUpdate(string name, string contestName, int userId);
        Task<List<Photo>> GetAllPhotos();
        Task<List<PhotoScoreContest>> GetAllPhotosSearch(string search);
        Photo GetPhotoById(int id);
        Task<List<PhotoScoreContest>> GetPhotoContest(int contestId);
        Task<Photo> GetPhotosByName(string photoName);
        Task<List<Photo>> GetPhotosUser(int userId);
        Task<List<Photo>> GetPhotosWinners();
        Task<int> NumberOfPhotos();
        Task Upload(IFormFile file);

        PhotoScoreContest GetPhotoScoreContest(int contestId, int photoId);
    }
}