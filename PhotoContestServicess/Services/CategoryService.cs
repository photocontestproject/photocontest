﻿using Microsoft.EntityFrameworkCore;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using PhotoContestServicess.ServiceContracts;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;

namespace PhotoContestServicess.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly PhotoContestContext _context;

        public CategoryService(PhotoContestContext context)
        {
            _context = context;

        }

        public async Task<int> GetCategoryid(string name)
        {
            var catid = await _context.Categories.FirstOrDefaultAsync(c => c.Name == name);
            return catid.Id;
        }

        public async Task<string> GetCategoryname(int? id)
        {
            var catname = await _context.Categories.FirstOrDefaultAsync(c => c.Id == id);
            var str = catname.Name;
            return str;
        }

        public List<Category> GettAll()
        {
            return  _context.Categories.ToList();
        }

    }
}
