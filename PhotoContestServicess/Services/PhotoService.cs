﻿using Azure.Storage.Blobs;
using Microsoft.AspNetCore.Http;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace PhotoContestServicess.Services
{
    public class PhotoService : IPhotoService
    {
        private readonly PhotoContestContext _context;
        private readonly BlobServiceClient _blobServiceClient;

        public PhotoService(PhotoContestContext context, BlobServiceClient blobServiceClient)
        {
            _context = context;
            _blobServiceClient = blobServiceClient;

        }

        public async Task<Photo> GetPhotosByName(string photoName)
        {
            return await _context.Photos.Include(p => p.User).
                Include(p => p.Comments)
                .ThenInclude(u => u.User)
                .FirstOrDefaultAsync(p => p.Name == photoName);
        }

        public async Task<List<Photo>> GetPhotosUser(int userId)
        {
            return await _context.Photos.Where(x => x.UserId == userId).ToListAsync();
        }

        public async Task<List<PhotoScoreContest>> GetPhotoContest(int contestId)
        {
            return await _context.PhotoScoreContests.Where(x => x.ContestId == contestId)
                .Include(x => x.Photo).ToListAsync();
        }

        public async Task<List<Photo>> GetPhotosWinners()
        {
            _context.Contests.Where(c => c.EndDate < DateTime.Now).ToList();

            var photo = (from c in _context.Contests
                         join p in  _context.Photos on c.WinnerPhoto equals p.Id

                         select new Photo
                         {

                             Id = p.Id,
                             ISO = p.ISO,
                             Aperture = p.Aperture,
                             Camera = p.Camera,
                             Description = p.Description,
                             FocalLength = p.FocalLength,
                             Name = p.Name,
                             UserId = p.UserId,
                             ShutterSpeed = p.ShutterSpeed,
                             PhotoLink = p.PhotoLink,

                         }).ToListAsync();
            return await photo;
        }

        public async Task Upload(IFormFile file)
        {
            var blobContainer = _blobServiceClient.GetBlobContainerClient("youtubedemo");

            var content = ContentDispositionHeaderValue.Parse(file.ContentDisposition);

            var fileName = content.FileName.Trim('"');

            var blobClient = blobContainer.GetBlobClient(fileName);

            await blobClient.UploadAsync(file.OpenReadStream());
        }

        public Photo GetPhotoById(int id)
        {
            return _context.Photos.FirstOrDefault(x => x.Id == id);
        }

        public async Task<int> NumberOfPhotos()
        {
            var result = await _context.Photos.ToListAsync();
            return result.Count();
        }

        public void ContestPhotoUpdate(string name, string contestName, int userId)
        {
            var cont = _context.Contests.FirstOrDefault(c => c.Name == contestName);
            var photo = _context.Photos.FirstOrDefault(p => p.Name == name);

            var photoId = photo.Id;
            var contid = cont.Id;

            var contphoto2 = new PhotoScoreContest()
            {
                ContestId = contid,
                PhotoId = photoId
            };

            var contuser = new ContestUser()
            {
                UserId = userId,
                ContestId = contid
            };

            _context.ContestUsers.Add(contuser);
            _context.PhotoScoreContests.Add(contphoto2);
            _context.SaveChanges();
        }

        public async Task<List<Photo>> GetAllPhotos()
        {
            var photo = await _context.Photos.ToListAsync();
            return photo;
        }

        public async Task<List<PhotoScoreContest>> GetAllPhotosSearch(string search)
        {
            return await _context.PhotoScoreContests.Include(c => c.Contest).Include(c => c.Photo).Where(x => x.Contest.Name.Contains(search)).ToListAsync();
        }


        public PhotoScoreContest GetPhotoScoreContest(int contestId, int photoId)
        {
            var result = _context.PhotoScoreContests
                .Where(x => x.ContestId == contestId)
                .Where(a => a.PhotoId == photoId)
                .ToList();
            if(result.Count == 0)
            {
                return null;
            }
            return result.First();
        }
    }
}
