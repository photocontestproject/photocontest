﻿using AutoMapper;
using PhotoContest.Data;
using PhotoContest.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using System;

namespace PhotoContestServicess.Services
{
    public class ContestService : IContestService
    {
        private readonly PhotoContestContext _context;
        private readonly IUsersServices _userService;

        public ContestService(PhotoContestContext context, IUsersServices userService)
        {
            _context = context;
            _userService = userService;
        }

        public async Task<Contest> GetContest(string name)
        {
            var cont = await _context.Contests.FirstOrDefaultAsync(c => c.Name == name);
            return cont;

        }


        public List<PhotoScoreContest> GetContestPhotos(string id)
        {
            var cont = _context.Contests.FirstOrDefault(c => c.Name == id);

            var contphotos = _context.PhotoScoreContests.Include(c => c.Photo).Where(c => c.ContestId == cont.Id).ToList();

            return contphotos;
        }

        public async Task<List<Contest>> GetContests(int num)
        {
            var cont = await _context.Contests.Include(x => x.Admin).Take(num).ToListAsync();

            return cont;
        }


        public async Task<List<Contest>> GetAllContests()
        {
            var cont = await _context.Contests.Include(x => x.Admin).ToListAsync();

            return cont;
        }

        public async Task<List<Contest>> GetUserContests(int userId)
        {
            var contests = (from d in _context.Contests
                            join b in _context.ContestUsers on d.Id equals b.ContestId
                            join e in _context.Users on b.UserId equals e.Id
                            where b.UserId == userId
                            select new Contest
                            {
                                Id = d.Id,
                                AdminId = d.AdminId,
                                Description = d.Description,
                                EndDate = d.EndDate,
                                IsDeleted = d.IsDeleted,
                                Name = d.Name,
                                ParticipantLimit = d.ParticipantLimit,
                                Profile = d.Profile,
                                StartDate = d.StartDate,
                                CategoryId = d.CategoryId,
                                Phase = d.Phase

                            }).ToListAsync();
            return await contests;
        }

        public async Task<List<Contest>> GetAdminContests(int adminId)
        {
            return await _context.Contests.Include(c => c.Category)
                .Include(c => c.Admin).Where(c => c.AdminId == adminId)
                .Where(x => x.IsDeleted == false).ToListAsync();
        }


        public async Task<List<Contest>> ContestCategory(int categoryId, int userId)
        {
            var cont = await GetUserContests(userId);
            return cont.Where(c => c.CategoryId == categoryId).ToList();
        }

        public async Task<List<Contest>> ContestCategoryAdmin(int categoryId, int adminId)
        {
            var cont = await GetAdminContests(adminId);
            return cont.Where(c => c.CategoryId == categoryId).ToList();
        }


        public async Task<List<Contest>> ContestCatUserPhase(int categoryId, int userId, int phase)
        {
            var cont = await ContestCategory(categoryId, userId);
            if (phase == 0)
            {
                return cont;
            }

            return cont.Where(c => c.Phase == phase).ToList();
        }

        public async Task<List<Contest>> ContestCatUserPhase(int categoryId, int userId, List<int> phases)
        {
            var cont = new List<Contest>();

            if (categoryId == 0)
            {
                cont = await GetUserContests(userId);
            }
            else
            {
                cont = await ContestCategory(categoryId, userId);
            }

            var contupdate = new List<Contest>();

            if (phases.Count == 0)
            {
                contupdate = cont;
            }

            if (phases.Count == 1)
            {
                contupdate = cont.Where(c => c.Phase == phases[0]).ToList();
            }

            if (phases.Count == 2)
            {
                contupdate = cont.Where(c => c.Phase == phases[0] || c.Phase == phases[1]).ToList();
            }

            if (phases.Count == 3)
            {
                contupdate = cont;
            }

            return contupdate;
        }

        public async Task<List<Contest>> GetAllContestsFilter(int categoryId, List<int> phases)
        {

            var cont = new List<Contest>();

            if (categoryId == 0)
            {
                cont = await GetAllContests();
            }
            else
            {
                cont = await _context.Contests.Where(c => c.CategoryId == categoryId).ToListAsync();
            }

            var contupdate = new List<Contest>();

            if (phases.Count == 0)
            {
                contupdate = cont;
            }

            if (phases.Count == 1)
            {
                contupdate = cont.Where(c => c.Phase == phases[0]).ToList();
            }

            if (phases.Count == 2)
            {
                contupdate = cont.Where(c => c.Phase == phases[0] || c.Phase == phases[1]).ToList();
            }

            if (phases.Count == 3)
            {
                contupdate = cont;
            }

            return contupdate;

        }

        public async Task<List<Contest>> ContestCatPhase(int categoryId, int phase)
        {
            return await _context.Contests
                .Where(c => c.CategoryId == categoryId && c.Phase == phase)
                .ToListAsync();
        }

        public async Task<List<Contest>> ContestCatUserPhaseAdmin(int categoryId, int userId, List<int> phases)
        {
            var cont = new List<Contest>();

            if (categoryId == 0)
            {
                cont = await GetAdminContests(userId);
            }
            else
            {
                cont = await ContestCategoryAdmin(categoryId, userId);
            }

            var contupdate = new List<Contest>();

            if (phases.Count == 0)
            {
                contupdate = cont;
            }

            if (phases.Count == 1)
            {
                contupdate = cont.Where(c => c.Phase == phases[0]).ToList();
            }

            if (phases.Count == 2)
            {
                contupdate = cont.Where(c => c.Phase == phases[0] || c.Phase == phases[1]).ToList();
            }

            if (phases.Count == 3)
            {
                contupdate = cont;
            }

            return contupdate;
        }

        public void ChangeContestToExpired(string contestName)
        {
            var contest = _context.Contests.FirstOrDefault(x => x.Name == contestName);
            contest.Phase = 3;
            _context.SaveChanges();
        }

        public void ChangeContestToPhaseTwo(string contestName)
        {
            var contest = _context.Contests.FirstOrDefault(x => x.Name == contestName);
            contest.Phase = 2;
            _context.SaveChanges();
        }

        public async void DeleteContest(Contest contest)
        {
            contest.IsDeleted = true;
            await _context.SaveChangesAsync();
        }

        public async Task<Category> GetCategory(int? id)
        {
            return await _context.Categories.FirstOrDefaultAsync(x => x.Id == id);
        }


        public void TakeScore(string contestname)
        {
            var contests = _context.Contests.Include(c => c.PhotoScoreContests)
                .ThenInclude(c => c.Photo)
                .FirstOrDefault(x => x.Name == contestname);

            var listPhotos = new List<Photo>();

            foreach (var item in contests.PhotoScoreContests)
            {
                listPhotos.Add(item.Photo);
            }


            var orderListPhotos = listPhotos.OrderByDescending(x => x.ContestScore).ToList();


            for (int i = 0; i < orderListPhotos.Count(); i++)
            {
                if (i == 0)
                {
                    if (orderListPhotos.Count > 1)
                    {
                        if (orderListPhotos[0].ContestScore == orderListPhotos[1].ContestScore)
                        {
                            var photoScoreContest = GetPhotoScoreContest(contests.Id, orderListPhotos[0].Id);
                            photoScoreContest.Score = 40;
                            _context.SaveChanges();
                            _userService.UpdateUser(orderListPhotos[0].UserId);

                        }
                        else
                        {
                            var photoScoreContest = GetPhotoScoreContest(contests.Id, orderListPhotos[0].Id);
                            photoScoreContest.Score = 50;
                            _context.SaveChanges();
                            _userService.UpdateUser(orderListPhotos[0].UserId);

                        }
                    }
                    if (orderListPhotos.Count == 1)
                    {
                        var photoScoreContest = GetPhotoScoreContest(contests.Id, orderListPhotos[0].Id);
                        photoScoreContest.Score = 50;
                        _context.SaveChanges();
                        _userService.UpdateUser(orderListPhotos[0].UserId);
                    }
                }
                if (i == 1)
                {
                    if (orderListPhotos.Count > 2)
                    {
                        if (orderListPhotos[1].ContestScore == orderListPhotos[2].ContestScore)
                       
                        {
                            var photoScoreContest = GetPhotoScoreContest(contests.Id, orderListPhotos[0].Id);
                            photoScoreContest.Score = 25;
                            _context.SaveChanges();
                            _userService.UpdateUser(orderListPhotos[0].UserId);
                        }
                        else
                        {
                            var photoScoreContest = GetPhotoScoreContest(contests.Id, orderListPhotos[0].Id);
                            photoScoreContest.Score = 35;
                            _context.SaveChanges();
                            _userService.UpdateUser(orderListPhotos[0].UserId);
                        }
                    }
                    if (orderListPhotos.Count == 2)
                    {
                        var photoScoreContest = GetPhotoScoreContest(contests.Id, orderListPhotos[0].Id);
                        photoScoreContest.Score = 35;
                        _context.SaveChanges();
                        _userService.UpdateUser(orderListPhotos[0].UserId);
                    }
                }
                if (i == 2)
                {
                    if (orderListPhotos.Count > 3)
                    {
                        if (orderListPhotos[2].ContestScore == orderListPhotos[3].ContestScore)
                       
                        {
                            var photoScoreContest = GetPhotoScoreContest(contests.Id, orderListPhotos[0].Id);
                            photoScoreContest.Score = 10;
                            _context.SaveChanges();
                            _userService.UpdateUser(orderListPhotos[0].UserId);
                        }
                        else
                        {
                            var photoScoreContest = GetPhotoScoreContest(contests.Id, orderListPhotos[0].Id);
                            photoScoreContest.Score = 20;
                            _context.SaveChanges();
                            _userService.UpdateUser(orderListPhotos[0].UserId);
                        }
                    }
                    if (orderListPhotos.Count == 3)
                    {
                        var photoScoreContest = GetPhotoScoreContest(contests.Id, orderListPhotos[0].Id);
                        photoScoreContest.Score = 20;
                        _context.SaveChanges();
                        _userService.UpdateUser(orderListPhotos[0].UserId);
                    }

                }
                orderListPhotos[i].ContestScore = 0;

            }
            _context.SaveChanges();
        }

        public void SetTimer(DateTime endDate, DateTime startDate, string contestName, TimeSpan span)
        {
            Set(endDate, startDate, contestName, span);
        }

        public void Set(DateTime endDate, DateTime startDate, string contestName, TimeSpan span)
        {
            BackgroundJob.Schedule(() => ChangeContestToPhaseTwo(contestName), endDate.Subtract(span) - startDate);
            BackgroundJob.Schedule(() => ChangeContestToExpired(contestName), endDate - startDate);
            BackgroundJob.Schedule(() => TakeScore(contestName), endDate - startDate);
        }
        
        public PhotoScoreContest GetPhotoScoreContest(int contestId, int photoId)
        {
            var result = _context.PhotoScoreContests
                .Where(x => x.ContestId == contestId)
                .Where(a => a.PhotoId == photoId)
                .ToList();
            if(result.Count == 0)
            {
                return null;
            }
            return result.First();
        }
    }
}
