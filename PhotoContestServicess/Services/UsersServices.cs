﻿using PhotoContest.Data;
using PhotoContest.Data.Models;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;


namespace PhotoContestServicess.Services
{
    public class UsersServices : IUsersServices
    {
        private readonly PhotoContestContext _context;

        public UsersServices(PhotoContestContext context)
        {
            _context = context;

        }

        public async Task<List<User>> GetUsers(int num)
        {
            var a = (from d in _context.Users
                     join b in _context.UserRoles on d.Id equals b.UserId

                     select new User
                     {
                         Id = d.Id,
                         UserName = d.UserName,
                         NormalizedUserName = d.NormalizedUserName,
                         Email = d.Email,
                         NormalizedEmail = d.NormalizedEmail,
                         PasswordHash = d.PasswordHash,
                         SecurityStamp = d.SecurityStamp,
                         ProfileLink = d.ProfileLink,
                         RankId = d.RankId

                     }).Take(num).ToListAsync();
            return await a;
        }

        public async Task<string> CheckUserRole(int id)
        {
            var u = await _context.UserRoles.FirstOrDefaultAsync(u => u.UserId == id);
            if (u.RoleId == 1)
                return "admin";

            else
                return "user";
        }

        public async Task<bool> CheckIfUserJoinedContest(int userId, int contesId)
        {
            var uc = await _context.ContestUsers.FirstOrDefaultAsync(cu => cu.ContestId == contesId && cu.UserId == userId);
            if (uc != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> CheckIfUserRatedContest(int userId, int contesId)
        {
            var uc = await _context.ContestUsers.FirstOrDefaultAsync(cu => cu.ContestId == contesId && cu.UserId == userId);

            if (uc == null)
            {
                return false;
            }

            if (uc.Rated == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public async Task<string> GetUserName(string email)
        {
            var username = await _context.Users.FirstOrDefaultAsync(u => u.Email == email);

            return username.UserName;
        }

        public async Task<User> GetUser(int id)
        {
            return await _context.Users.Include(u => u.Rank).FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<List<User>> GetUserRankFilter(List<string> rank)
        {
            if (rank.Count == 0)
            {
                return await GetAllUsers();
            }

            if (rank.Count == 1)
            {
                var rankId = _context.Ranks.FirstOrDefault(r => r.RankName == rank[0]).Id;
                return await _context.Users.Where(u => u.RankId == rankId).ToListAsync();
            }

            if (rank.Count == 2)
            {
                var rankId = await _context.Ranks.Where(r => r.RankName == rank[0] || r.RankName == rank[1]).ToListAsync();
                var users = await _context.Users.Where(u => u.RankId.Equals(rankId[0].Id) || u.RankId.Equals(rankId[1].Id)).ToListAsync();
                return users;
            }

            if (rank.Count == 3)
            {
                var rankId = await _context.Ranks.Where(r => r.RankName == rank[0] || r.RankName == rank[1] || r.RankName == rank[2]).ToListAsync();
                var users = await _context.Users.Where(u => u.RankId.Equals(rankId[0].Id) || u.RankId.Equals(rankId[1].Id) || u.RankId.Equals(rankId[2].Id)).ToListAsync();
                return users;
            }

            return await GetAllUsers();
        }

        public async Task<List<User>> GetAllUsers()
        {
            return await _context.Users.ToListAsync();
        }

        public  User GetUserById(int id)
        {
            return  _context.Users.FirstOrDefault(x => x.Id == id);
        }

        public async Task<User> GetUserByName(string name)
        {
            return await _context.Users.FirstOrDefaultAsync(x => x.UserName == name);
        }

        public async Task<User> GetUserByEmail(string email)
        {
            return await _context.Users.FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<List<User>> GetUserAndRoles()
        {
            return await _context.Users.Include(u => u.Rank).Where(u => u.RankId == 2).ToListAsync();
        }

        public void ContestUserUpdate(string contname, int userid)
        {
            var cont = _context.Contests.FirstOrDefault(c => c.Name == contname);
            var contid = cont.Id;

            var contuser = new ContestUser()
            {
                ContestId = contid,
                UserId = userid,
                Rated = true
            };

            _context.ContestUsers.Add(contuser);
            _context.SaveChanges();
        }

        public void UpdateUser(int userId)
        {
            var photoScore =_context.PhotoScoreContests.
                Include(x => x.Photo).
                Where(a => a.Photo.UserId == userId).ToList();

            int sum = 0;

            foreach (var item in photoScore)
            {
                sum += item.Score;
            }

            var user = this.GetUserById(userId);


            if (sum > 50 && sum < 151)
            {
                if (user.RankId != 4)
                {
                    user.RankId = user.RankId + 1;
                }
            }

            if (sum > 150 && sum < 1001)
            {
                if (user.RankId != 4)
                {
                    user.RankId = user.RankId + 1;
                }
            }

            _context.SaveChanges();
        }

        public async Task<string> GetUserRank(int? userRankId)
        {
            var rank = await _context.Ranks.FirstOrDefaultAsync(u => u.Id == userRankId);
            return rank.RankName;
        }

        public async Task<string> GetApiKey(string userName)
        {
            var user = await _context.Users.Where(x => x.UserName == userName).ToListAsync();
            var apiKey = "";
            if (user.Count() != 0)
            {
                var guid = Guid.NewGuid().ToString();
                user.First().ApiKey = guid;
                apiKey = guid;
            }
            _context.SaveChanges();
            return apiKey;
        }

        public async Task<List<User>> GetAdmins()
        {
            var adm = await _context.UserRoles.Where(u => u.RoleId == 1).ToListAsync();
            var users = new List<User>();

            foreach (var item in adm)
            {
                users.Add(await _context.Users.FirstOrDefaultAsync(u => u.Id == item.UserId));
            }
            return users;
        }

        public async Task<User> GetUserFromPhoto(int photoId)
        {
            var photo = await _context.Photos.FirstOrDefaultAsync(p => p.Id == photoId);
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == photo.UserId);
            return user;
        }

    }
}

