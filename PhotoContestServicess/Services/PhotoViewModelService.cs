﻿using Azure.Storage.Blobs;
using Microsoft.WindowsAzure.Storage;
using PhotoContestServicess.ServiceContracts;
using System;
using System.Collections.Generic;


namespace PhotoContestServicess.Services
{
    public class PhotoViewModelService : IPhotoViewModelService
    {
        private readonly BlobServiceClient _client;
        public PhotoViewModelService(BlobServiceClient client)
        {
            _client = client;
        }

        public List<string> GetBlobsUri()
        {
            CloudStorageAccount backupStorageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=photocontest;AccountKey=l69XRYx6ZbrjJdpEciIsq4ebymLOcV3sb/2TBuhk2vmpVb02ZIMyjb0JjaShNRv2fosicf87m4u+MgI4MIoVEg==;EndpointSuffix=core.windows.net");

            var backupBlobClient = backupStorageAccount.CreateCloudBlobClient();

            var listUri = new List<string>();

            var blobContainer = _client.GetBlobContainerClient("youtubedemo");

            
            foreach (var item in blobContainer.GetBlobs())
            {
                var blobClient = blobContainer.GetBlobClient(item.Name);

                listUri.Add(blobClient.Uri.OriginalString);
            }
            return listUri; 
        }
    }
}
