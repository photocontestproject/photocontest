﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Data.Models;
using PhotoContestServicess.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;
using WebApplication1.Models.CardsModels;

namespace PhotoContest.Controllers
{
    [Route("[controller]")]
    [ApiController]

    public class ContestController : Controller
    {
        private readonly IContestService _service;
        private readonly IMapper _mapper;
        private readonly IPhotoService _photoService;

        public ContestController(IContestService service, IMapper mapper, IPhotoService photoService)
        {
            _service = service;
            _mapper = mapper;
            _photoService = photoService;
        }

        [HttpGet("get_contest/contestname={contestname}")]
        public async Task<IActionResult> GetContestByName(string contestname)
        {
            try
            {
                var resultContest = await _service.GetContest(contestname);
                var mappedContest = _mapper.Map<Contest, ContestCardModel>(resultContest);
                return Ok(mappedContest);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_contest_photos/contestname={contestname}")]
        public IActionResult GetContestPhotos(string contestname)
        {
            try
            {
                var resultPhotos = _service.GetContestPhotos(contestname);
                var listPhotos = new List<Photo>();

                foreach (var item in resultPhotos)
                {
                    listPhotos.Add(_photoService.GetPhotoById(item.PhotoId));
                }

                return Ok(listPhotos);

            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_num_contests/num={num}")]
        public async Task<IActionResult> GetNumContests(int num)
        {
            try
            {
                var resultContests = await _service.GetContests(num);
                var mappedContests = _mapper.Map<List<Contest>, List<ContestCardModel>>(resultContests);
                return Ok(mappedContests);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_all_contests")]
        public async Task<IActionResult> GetAllContests()
        {
            try
            {
                var resultContests = await _service.GetAllContests();
                return Ok(resultContests);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }


        [HttpGet("get_user_contests/userid={userid}")]
        public async Task<IActionResult> GetAllContests(int userid)
        {
            try
            {
                var resultContests = await _service.GetUserContests(userid);
                var mappedContests = _mapper.Map<List<Contest>, List<ContestCardModel>>(resultContests);
                return Ok(mappedContests);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }


        [HttpGet("get_contests/admin/userid={userid}")]
        public async Task<IActionResult> GetAdminContests(int userid)
        {
            try
            {
                var resultContests = await _service.GetAdminContests(userid);
                var mappedContests = _mapper.Map<List<Contest>, List<ContestCardModel>>(resultContests);
                return Ok(mappedContests);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_contests/user/categoryid={categoryid}&userid={userid}")]
        public async Task<IActionResult> GetContestsByCategory(int categoryid, int userid)
        {
            try
            {
                var resultContests = await _service.ContestCategory(categoryid, userid);
                var mappedContests = _mapper.Map<List<Contest>, List<ContestCardModel>>(resultContests);
                return Ok();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_contests/admin/categoryid={categoryid}&userid={userid}")]
        public async Task<IActionResult> GetContestsByCategoryAdmin(int categoryid, int userid)
        {
            try
            {
                var resultContests = await _service.ContestCategory(categoryid, userid);
                var mappedContests = _mapper.Map<List<Contest>, List<ContestCardModel>>(resultContests);
                return Ok();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_contests/user/categoryid={categoryid}&userid={userid}&phaseid={phaseid}")]
        public async Task<IActionResult> GetContestsByCategoryUser(int categoryid, int userid, int phaseid)
        {
            try
            {
                var resultContests = await _service.ContestCatUserPhase(categoryid, userid, phaseid);
                var mappedContests = _mapper.Map<List<Contest>, List<ContestCardModel>>(resultContests);
                return Ok();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_contests/admin/categoryid={categoryid}&userid={userid}&phaseid={phaseid}")]
        public async Task<IActionResult> GetContestsByCategoryAdmin(int categoryid, int userid, int phaseid)
        {
            try
            {
                var resultContests = await _service.ContestCatUserPhase(categoryid, userid, phaseid);
                var mappedContests = _mapper.Map<List<Contest>, List<ContestCardModel>>(resultContests);
                return Ok();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }


        [HttpGet("get_contests/categoryid={categoryid}&phaseid={phaseid}")]
        public async Task<IActionResult> GetContestsByPhase(int categoryid, int phaseid)
        {
            try
            {
                var resultContests = await _service.ContestCatPhase(categoryid, phaseid);
                var mappedContests = _mapper.Map<List<Contest>, List<ContestCardModel>>(resultContests);
                return Ok(resultContests);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}
