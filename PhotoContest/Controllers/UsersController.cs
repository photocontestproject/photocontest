﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Data.Models;
using PhotoContestServicess.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace PhotoContest.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        private readonly IUsersServices _service;
        private readonly IMapper _mapper;

        public UsersController(IUsersServices service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;

        }

        [HttpGet("")]
        public async Task<IActionResult> GetUsers()
        {
            try
            {
                var resultUsers = await _service.GetAllUsers();

                var mappedUsers = _mapper.Map<List<User>, List<UserViewModel>>(resultUsers);

                return Ok(mappedUsers);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_by/id={id}")]
        public async Task<IActionResult> GetUserById(int id)
        {
            try
            {
                var user = _service.GetUserById(id);
                var mappedUser = _mapper.Map<User, UserViewModel>(user);
                return Ok(mappedUser);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_by/username={username}")]
        public async Task<IActionResult> GetUserByName(string username)
        {
            try
            {
                var user = await _service.GetUserByName(username);
                var mappedUser = _mapper.Map<User, UserViewModel>(user);
                return Ok(mappedUser);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_by/email={email}")]
        public async Task<IActionResult> GetUserByEmail(string email)
        {
            try
            {
                var user = await _service.GetUserByEmail(email);
                var mappedUser = _mapper.Map<User, UserViewModel>(user);
                return Ok(mappedUser);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_username/email={email}")]
        public async Task<IActionResult> GetUserUsername(string email)
        {
            try
            {
                var username = await _service.GetUserName(email);
                return Ok(username);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("if_joined/userid={id}&contestid={contestid}")]
        public async Task<IActionResult> CheckIfJoined(int userid, int contestid)
        {
            try
            {
                var username = await _service.CheckIfUserJoinedContest(userid, contestid);
                return Ok(username);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("if_rated/userid={id}&contestid={contestid}")]
        public async Task<IActionResult> CheckIfRated(int userid, int contestid)
        {
            try
            {
                var username = await _service.CheckIfUserRatedContest(userid, contestid);
                return Ok(username);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_user_role/userid={id}")]
        public async Task<IActionResult> GetUserRole(int id)
        {
            try
            {
                var user = await _service.CheckUserRole(id);
                return Ok(user);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}
