﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Data.Models;
using PhotoContestServicess.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models.CardsModels;

namespace PhotoContest.Controllers
{
    [Route("[controller]")]
    [ApiController]

    public class PhotoController : Controller
    {
        private readonly IPhotoService _service;
        private readonly IMapper _mapper;

        public PhotoController(IPhotoService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet("get_by/name={name}")]
        public async Task<IActionResult> GetPhotoByName(string name)
        {
            try
            {
                var resultPhoto = await _service.GetPhotosByName(name);

                return Ok(resultPhoto);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_user_photos/userid={userid}")]
        public async Task<IActionResult> GetUserPhotos(int userid)
        {
            try
            {
                var resultPhotos = await _service.GetPhotosUser(userid);
                return Ok(resultPhotos);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_contest_photos/contestid={contestid}")]
        public async Task<IActionResult> GetContestPhotos(int contestid)
        {
            try
            {
                var resultPhotos = await _service.GetPhotoContest(contestid);

                var listPhotos = new List<Photo>();

                foreach (var item in resultPhotos)
                {
                    listPhotos.Add( _service.GetPhotoById(item.PhotoId));
                }

                return Ok(listPhotos);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_photo_count")]
        public async Task<IActionResult> GetPhotosCount()
        {
            try
            {
                var result = await _service.NumberOfPhotos();
                return Ok(result);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }


        [HttpGet("get_photo_by_id/photoid={photoid}")]
        public async Task<IActionResult> GetPhotoById(int photoid)
        {
            try
            {
                var photo =  _service.GetPhotoById(photoid);
                return Ok(photo);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet("get_photo_winners")]
        public async Task<IActionResult> GetPhotoWinners()
        {
            try
            {
                var photos = await _service.GetPhotosWinners();
                return Ok(photos);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

    }
}
