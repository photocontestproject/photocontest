﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.UserServiceAPI
{
    public interface IUserServiceAPI
    {
        bool CheckUser(string apiKey);
    }
}
