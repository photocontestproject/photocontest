﻿using PhotoContest.Data;
using PhotoContestServicess.Services;
using System.Linq;

namespace PhotoContest.UserServiceAPI
{
    public class UserServicesAPI : IUserServiceAPI
    {
        private readonly PhotoContestContext _context;

        public UserServicesAPI(PhotoContestContext context)
        {
            _context = context;
        }
        public bool CheckUser(string apiKey)
        {
            var users = _context.Users;
            var moreUsers = users.ToList();

            foreach (var item in moreUsers)
            {
                if (item.ApiKey == apiKey)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
