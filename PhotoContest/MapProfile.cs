﻿using PhotoContest.Data.Models;
using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;
using WebApplication1.Models.CardsModels;

namespace PhotoContest
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<User, UserViewModel>();
            CreateMap<Contest, ContestCardModel>();
        }
    }
}
